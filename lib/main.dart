import 'dart:io';
import 'package:cashbook_app/src/core/di/app_bindings.dart';
import 'package:cashbook_app/src/core/localization/app_localization.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/app_constants.dart';
import 'package:cashbook_app/src/core/theme/app_theme.dart';
import 'package:cashbook_app/src/core/widgets/k_scroll_behavior.dart';
import 'package:cashbook_app/src/features/localization/controller/locale_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  await GetStorage.init();

  Get.put(ThemeController());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final localController = Get.put(LocaleController());
  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return GetMaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: KScrollBehavior(),
            child: child!,
          );
        },
        debugShowCheckedModeBanner: false,
        title: AppConstants.appName,
        theme: themeController.isDarkMode
            ? AppThemeData.darkTheme
            : AppThemeData.lightTheme,
        initialBinding: AppBindings(),
        initialRoute: RouteGenerator.splash,
        getPages: RouteGenerator.routes,

        /// localization
        translations: AppLocalization(),
        locale: localController.selectedLocaleData.value.locale,
        fallbackLocale: AppLocalization.locals.first.locale,
      );
    });
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
