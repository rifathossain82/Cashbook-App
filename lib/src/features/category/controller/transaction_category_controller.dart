import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/category/model/transaction_category_model.dart';
import 'package:get/get.dart';

class TransactionCategoryController extends GetxController {
  var isLoading = false.obs;
  var transactionCategoryList = <TransactionCategoryModel>[].obs;

  @override
  void onInit() {
    getTransactionCategoryList();
    super.onInit();
  }

  Future getTransactionCategoryList() async {
    try {
      isLoading(true);
      transactionCategoryList.value = [];

      String businessId = '${LocalStorage.getData(key: LocalStorageKey.selectedBusinessId)}';

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.transactionCategoryList(businessId),
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            transactionCategoryList.add(TransactionCategoryModel.fromJson(i));
            transactionCategoryList.refresh();
          }
        }
      } else {
        throw 'Unable to load transaction category!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addTransactionCategory({
    required String categoryName,
  }) async {
    try {

      String businessId = '${LocalStorage.getData(key: LocalStorageKey.selectedBusinessId)}';

      var map = <String, dynamic>{};
      map['category_name'] = categoryName;
      map['business_id'] = businessId;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addTransactionCategory,
          body: map,
        ),
      );

      if (responseBody != null) {
        getTransactionCategoryList();
      } else {
        throw 'Failed to add category!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> updateTransactionCategory({
    required String id,
    required String categoryName,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['category_name'] = categoryName;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updateTransactionCategory,
          body: map,
        ),
      );

      if (responseBody != null) {
        // kSnackBar(
        //   message: "Category update successfully!",
        //   bgColor: successColor,
        // );
        getTransactionCategoryList();
      } else {
        throw 'Failed to update Category!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deleteTransactionCategory({
    required String id,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deleteTransactionCategory,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Category Deleted Successfully!",
          bgColor: successColor,
        );
        getTransactionCategoryList();
      } else {
        throw 'Failed to Delete Category!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }
}
