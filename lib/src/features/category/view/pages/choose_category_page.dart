import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/core/widgets/popup_menu_item_builder.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:cashbook_app/src/features/category/controller/transaction_category_controller.dart';
import 'package:cashbook_app/src/features/category/model/transaction_category_model.dart';
import 'package:cashbook_app/src/features/category/view/widgets/add_new_category_bottom_sheet.dart';
import 'package:cashbook_app/src/features/category/view/widgets/update_category_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChooseCategoryPage extends StatefulWidget {
  final TransactionCategoryModel? categoryModel;

  const ChooseCategoryPage({
    Key? key,
    required this.categoryModel,
  }) : super(key: key);

  @override
  State<ChooseCategoryPage> createState() => _ChooseCategoryPageState();
}

class _ChooseCategoryPageState extends State<ChooseCategoryPage> {
  final categoryController = Get.find<TransactionCategoryController>();
  TransactionCategoryModel? selectedCategory;

  // final suggestedCategories = [
  //   'Sale',
  //   'Petrol',
  //   'Salary',
  //   'Income From Rent',
  //   'Profit',
  //   'Deposit',
  // ];

  @override
  void initState() {
    selectedCategory = widget.categoryModel;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose Category'.tr),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings_outlined,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => addNewCategoryBottomSheet(context),
        backgroundColor: kPrimaryLightColor,
        elevation: 0,
        icon: Icon(
          Icons.add,
          color: kWhite,
        ),
        label: Text(
          'ADD NEW'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
      ),
      body: Obx(() {
        return categoryController.isLoading.value
            ? const KCustomLoader()
            : categoryController.transactionCategoryList.isEmpty
                ? const NoDataFound()
                : Padding(
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Categories in this book'.tr,
                            style: MyTextStyles.h5.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kGreyTextColor,
                            ),
                          ),
                          const SizedBox(height: 10),
                          _buildCategoryList(),
                          const SizedBox(height: 60),
                          // Text(
                          //   'Suggested Categories',
                          //   style: MyTextStyles.h5.copyWith(
                          //     fontWeight: FontWeight.bold,
                          //     color: kGreyTextColor,
                          //   ),
                          // ),
                          // const SizedBox(height: 10),
                          // Wrap(
                          //   children: suggestedCategories
                          //       .map((tag) => SuggestedCategoryTagBuilder(
                          //             onTap: () => Navigator.pop(
                          //               context,
                          //               tag,
                          //             ),
                          //             text: tag,
                          //           ))
                          //       .toList(),
                          // ),
                        ],
                      ),
                    ),
                  );
      }),
    );
  }

  Widget _buildCategoryList() {
    List<TransactionCategoryModel> categoryList =
        categoryController.transactionCategoryList;
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: categoryList.length,
      itemBuilder: (context, index) => RadioListTileWidgetBuilder(
        onChanged: (value) {
          setState(() {
            selectedCategory = categoryList[index];
            Get.back(result: selectedCategory);
          });
        },
        value: categoryList[index].id,
        groupValue: selectedCategory?.id,
        isSelected: selectedCategory?.id == categoryList[index].id,
        title: categoryList[index].name ?? "",
        secondary: PopupMenuButton(
          color: Get.find<ThemeController>().isDarkMode
              ? kSecondaryDarkColor
              : kWhite,
          onSelected: (value) {
            if (value == PopupMenuItemOptions.edit) {
              updateCategoryBottomSheet(
                context: context,
                categoryModel: categoryList[index],
              );
            } else {
              deleteCategory(context, categoryList[index].id!);
            }
          },
          icon: Icon(
            Icons.more_vert_outlined,
            color: kGrey,
            size: 25,
          ),
          itemBuilder: (BuildContext context) => [
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.edit,
              icon: Icons.edit_outlined,
              iconColor: kGrey,
              title: 'Edit'.tr,
            ),
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.delete,
              icon: Icons.delete_outline,
              iconColor: kGrey,
              title: 'Delete'.tr,
            ),
          ],
        ),
      ),
      separatorBuilder: (context, index) => const SizedBox(
        height: 8,
      ),
    );
  }

  void deleteCategory(BuildContext context, int categoryID) async {
    /// show alert dialog
    bool? result = await customDeleteDialog(
      context: context,
      dialogPosition: Alignment.center,
      title: 'Delete Category?'.tr,
      contentText: "Are you sure you want to delete this category? This can't be undone.".tr,
      negativeActionText: 'NO'.tr,
      positiveActionText: 'YES, DELETE'.tr,
    );

    if (result ?? false) {
      final categoryController = Get.find<TransactionCategoryController>();
      categoryController.deleteTransactionCategory(
        id: '$categoryID',
      );
    }
  }
}
