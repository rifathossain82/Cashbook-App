import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/category/controller/transaction_category_controller.dart';
import 'package:cashbook_app/src/features/category/model/transaction_category_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future updateCategoryBottomSheet({
  required BuildContext context,
  required TransactionCategoryModel categoryModel,
}) {
  final formKey = GlobalKey<FormState>();
  final categoryNameTextController = TextEditingController();
  final categoryController = Get.find<TransactionCategoryController>();

  categoryNameTextController.text = categoryModel.name ?? "";

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.5, //size of bottom sheet
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Update Category'.tr,
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  const Divider(
                    height: 0,
                  ),

                  /// category name text field
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: TextFormField(
                      controller: categoryNameTextController,
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return Message.emptyName.tr;
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Category Name'.tr,
                        border: const OutlineInputBorder(),
                      ),
                    ),
                  ),

                  /// save button
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: KButton(
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.pop(context);
                          categoryController.updateTransactionCategory(
                            id: '${categoryModel.id}',
                            categoryName: categoryNameTextController.text,
                          );
                        }
                      },
                      child: Text(
                        'Update'.tr,
                        style: MyTextStyles.h4.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
