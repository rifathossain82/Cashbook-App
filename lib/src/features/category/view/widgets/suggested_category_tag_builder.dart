import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class SuggestedCategoryTagBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String text;

  const SuggestedCategoryTagBuilder({
    super.key,
    required this.onTap,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 8,
        ),
        margin: const EdgeInsets.all(4.0),
        decoration: BoxDecoration(
          color: kGreyLight.withOpacity(0.8),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          text,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
