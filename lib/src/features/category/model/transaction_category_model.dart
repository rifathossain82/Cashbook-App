class TransactionCategoryModel {
  int? id;
  String? name;
  int? businessId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;

  TransactionCategoryModel(
      {this.id,
        this.name,
        this.businessId,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  TransactionCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessId = json['business_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_id'] = businessId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}