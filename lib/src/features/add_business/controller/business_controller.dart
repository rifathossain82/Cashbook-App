import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/add_business/model/business_category_model.dart';
import 'package:cashbook_app/src/features/add_business/model/business_model.dart';
import 'package:cashbook_app/src/features/add_business/model/business_type_model.dart';
import 'package:get/get.dart';

class BusinessController extends GetxController {
  var isLoading = false.obs;
  var businessList = <BusinessModel>[].obs;
  var selectedBusiness = BusinessModel().obs;
  var businessCategoryList = <BusinessCategoryModel>[].obs;
  var businessTypeList = <BusinessTypeModel>[].obs;

  @override
  void onInit() {
    getBusinessList();
    super.onInit();
  }

  Future getBusinessList() async {
    try {
      isLoading(true);
      businessList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.businessList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['businesses'];
        if (data != null) {

          /// add business list
          for (Map<String, dynamic> i in data) {
            businessList.add(BusinessModel.fromJson(i));
            businessList.refresh();
          }

          /// selected business will be the last item initially
          selectedBusiness.value = businessList.last;

          /// fetch selected business id from local storage
          var selectedBusinessId = LocalStorage.getData(
            key: LocalStorageKey.selectedBusinessId,
          );

          /// if there is any id then set the business id which matched with business list
          /// otherwise set the last business item as selected
          if (selectedBusinessId != null) {
            for (var element in businessList) {
              if (element.id == selectedBusinessId) {
                selectedBusiness.value = element;
              }
            }
          } else {
            LocalStorage.saveData(
              key: LocalStorageKey.selectedBusinessId,
              data: selectedBusiness.value.id,
            );
          }
        }
      } else {
        throw 'Unable to load business list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future getBusinessCategoryList() async {
    try {
      isLoading(true);
      businessCategoryList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.businessCategoryList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['business_categories'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            businessCategoryList.add(BusinessCategoryModel.fromJson(i));
            businessCategoryList.refresh();
          }
        }
      } else {
        throw 'Unable to load business category list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future getBusinessTypeList() async {
    try {
      isLoading(true);
      businessTypeList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.businessTypeList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['business_types'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            businessTypeList.add(BusinessTypeModel.fromJson(i));
            businessTypeList.refresh();
          }
        }
      } else {
        throw 'Unable to load business type list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addBusiness({
    required String name,
    required String categoryId,
    required String typeId,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['name'] = name;
      map['business_category_id'] = categoryId;
      map['business_type_id'] = typeId;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addBusiness,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Business added successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to add new business!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deleteBusiness({
    required String businessId,
  }) async {
    try {
      isLoading(true);

      var map = <String, dynamic>{};
      map['id'] = businessId;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deleteBusiness,
          body: map,
        ),
      );

      if (responseBody != null) {

        LocalStorage.removeData(key: LocalStorageKey.token);
        LocalStorage.removeData(key: LocalStorageKey.selectedBusinessId);
        Get.offAllNamed(RouteGenerator.login);

        kSnackBar(
          message: "Business Deleted Successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed To Delete Business!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
