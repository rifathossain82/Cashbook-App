import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddBusinessSkipButton extends StatelessWidget {
  final VoidCallback onTap;

  const AddBusinessSkipButton({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextButton(
        onPressed: onTap,
        child: Text(
          'SKIP'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kGreyTextColor,
          ),
        ),
      ),
    );
  }
}
