import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StepTextBuilder extends StatelessWidget {
  final int currentStep;

  const StepTextBuilder({
    Key? key,
    required this.currentStep,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: context.screenWidth,
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode
            ? kSecondaryDarkColor
            : kGreyLight.withOpacity(0.5),
      ),
      alignment: Alignment.centerLeft,
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Business Setup: ',
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kGreyTextColor,
              ),
            ),
            TextSpan(
              text: ' ${'Step'.tr} $currentStep/3',
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
