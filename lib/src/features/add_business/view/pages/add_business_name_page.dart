import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/features/add_business/view/widgets/step_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddBusinessNamePage extends StatefulWidget {
  const AddBusinessNamePage({Key? key}) : super(key: key);

  @override
  State<AddBusinessNamePage> createState() => _AddBusinessNamePageState();
}

class _AddBusinessNamePageState extends State<AddBusinessNamePage> {
  final businessNameTextController = TextEditingController();
  bool isFormValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      body: Column(
        children: [
          Text(
            'Add Business Name'.tr,
            style: MyTextStyles.h2.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(15),
            child: TextFormField(
              controller: businessNameTextController,
              onChanged: (value) {
                setState(() {
                  if (value.isEmpty) {
                    isFormValid = false;
                  } else {
                    isFormValid = true;
                  }
                });
              },
              decoration: InputDecoration(
                isDense: true,
                labelText: 'Business Name'.tr,
                border: const OutlineInputBorder(),
              ),
            ),
          ),
          const Spacer(),
          const StepTextBuilder(currentStep: 1),
          const SizedBox(height: 20),
          KElevatedButton(
            onTap: () => Get.toNamed(
              RouteGenerator.selectBusinessCategory,
              arguments: {
                'name' : businessNameTextController.text,
              },
            ),
            isFormValid: isFormValid,
            text: 'NEXT',
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
