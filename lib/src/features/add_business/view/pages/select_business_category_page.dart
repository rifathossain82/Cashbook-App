import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/add_business/model/business_category_model.dart';
import 'package:cashbook_app/src/features/add_business/view/widgets/add_business_skip_button.dart';
import 'package:cashbook_app/src/core/widgets/business_category_item_builder.dart';
import 'package:cashbook_app/src/features/add_business/view/widgets/step_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelectBusinessCategoryPage extends StatefulWidget {
  const SelectBusinessCategoryPage({Key? key}) : super(key: key);

  @override
  State<SelectBusinessCategoryPage> createState() =>
      _SelectBusinessCategoryPageState();
}

class _SelectBusinessCategoryPageState
    extends State<SelectBusinessCategoryPage> {
  BusinessCategoryModel? selectedBusinessCategory;
  final businessController = Get.find<BusinessController>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      businessController.getBusinessCategoryList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          AddBusinessSkipButton(
            onTap: () {},
          ),
        ],
      ),
      body: Column(
        children: [
          /// title and category grid list
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Select Business Category',
                    style: MyTextStyles.h2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'This will help us personalise your app experience',
                      style: MyTextStyles.h4.copyWith(
                        color: kGreyTextColor,
                      ),
                    ),
                  ),

                  /// business category grid view
                  _buildBusinessCategoryGridView(),
                ],
              ),
            ),
          ),

          /// step text and next button
          Column(
            children: [
              const StepTextBuilder(currentStep: 2),
              const SizedBox(height: 20),
              KElevatedButton(
                onTap: () => Get.toNamed(
                  RouteGenerator.selectBusinessType,
                  arguments: {
                    'name': Get.arguments['name'],
                    'business_category_id': '${selectedBusinessCategory!.id}',
                  },
                ),
                isFormValid: selectedBusinessCategory != null,
                text: 'NEXT'.tr,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBusinessCategoryGridView() {
    return Obx(() {
      return businessController.isLoading.value
          ? const KCustomLoader()
          : businessController.businessCategoryList.isEmpty
              ? const NoDataFound()
              : GridView.builder(
                  padding: const EdgeInsets.all(15),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 15,
                    childAspectRatio: 2 / 1,
                  ),
                  itemCount: businessController.businessCategoryList.length,
                  itemBuilder: (context, index) => BusinessCategoryItemBuilder(
                    onTap: () {
                      setState(() {
                        selectedBusinessCategory =
                            businessController.businessCategoryList[index];
                      });
                    },
                    isSelected: selectedBusinessCategory ==
                        businessController.businessCategoryList[index],
                    categoryModel:
                        businessController.businessCategoryList[index],
                  ),
                );
    });
  }
}
