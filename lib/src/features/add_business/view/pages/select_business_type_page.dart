import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/add_business/model/business_type_model.dart';
import 'package:cashbook_app/src/features/add_business/view/widgets/add_business_skip_button.dart';
import 'package:cashbook_app/src/core/widgets/business_type_item_builder.dart';
import 'package:cashbook_app/src/features/add_business/view/widgets/step_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelectBusinessTypePage extends StatefulWidget {
  const SelectBusinessTypePage({Key? key}) : super(key: key);

  @override
  State<SelectBusinessTypePage> createState() => _SelectBusinessTypePageState();
}

class _SelectBusinessTypePageState extends State<SelectBusinessTypePage> {
  BusinessTypeModel? selectedBusinessType;
  final businessController = Get.find<BusinessController>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      businessController.getBusinessTypeList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          AddBusinessSkipButton(
            onTap: () {},
          ),
        ],
      ),
      body: Column(
        children: [
          /// title and business type list
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Select Business Type'.tr,
                    style: MyTextStyles.h2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'This will help us personalise your app experience'.tr,
                      style: MyTextStyles.h4.copyWith(
                        color: kGreyTextColor,
                      ),
                    ),
                  ),

                  /// business type list view
                  _buildBusinessTypeListView(),
                ],
              ),
            ),
          ),

          /// step text and done button
          Column(
            children: [
              const StepTextBuilder(currentStep: 3),
              const SizedBox(height: 20),
              KElevatedButton(
                onTap: doneMethod,
                isFormValid: selectedBusinessType != null,
                text: 'Done'.tr,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBusinessTypeListView() {
    return Obx(() {
      return businessController.isLoading.value
          ? const KCustomLoader()
          : businessController.businessTypeList.isEmpty
              ? const NoDataFound()
              : ListView.separated(
                  padding: const EdgeInsets.all(15),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: businessController.businessTypeList.length,
                  itemBuilder: (context, index) => BusinessTypeItemBuilder(
                    onTap: () {
                      setState(() {
                        selectedBusinessType =
                            businessController.businessTypeList[index];
                      });
                    },
                    isSelected: selectedBusinessType ==
                        businessController.businessTypeList[index],
                    typeModel: businessController.businessTypeList[index],
                  ),
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 15),
                );
    });
  }

  void doneMethod() async {
    /// clear selected business id
    LocalStorage.removeData(key: LocalStorageKey.selectedBusinessId);

    /// call add business api
    await businessController.addBusiness(
      name: Get.arguments['name'],
      categoryId: Get.arguments['business_category_id'],
      typeId: '${selectedBusinessType!.id}',
    );

    /// pop all screen
    Get.offAllNamed(RouteGenerator.dashboard);
  }
}
