class BusinessModel {
  int? id;
  String? name;
  int? businessCategoryId;
  int? businessTypeId;
  int? userId;
  dynamic deletedAt;
  List<Books>? books;

  BusinessModel(
      {this.id,
        this.name,
        this.businessCategoryId,
        this.businessTypeId,
        this.userId,
        this.deletedAt,
        this.books});

  BusinessModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessCategoryId = json['business_category_id'];
    businessTypeId = json['business_type_id'];
    userId = json['user_id'];
    deletedAt = json['deleted_at'];
    if (json['books'] != null) {
      books = <Books>[];
      json['books'].forEach((v) {
        books!.add(Books.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_category_id'] = businessCategoryId;
    data['business_type_id'] = businessTypeId;
    data['user_id'] = userId;
    data['deleted_at'] = deletedAt;
    if (books != null) {
      data['books'] = books!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Books {
  int? id;
  String? name;
  int? businessId;
  int? userId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;

  Books(
      {this.id,
        this.name,
        this.businessId,
        this.userId,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Books.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessId = json['business_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_id'] = businessId;
    data['user_id'] = userId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}