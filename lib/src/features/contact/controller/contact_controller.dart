import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/contact/model/contact_model.dart';
import 'package:cashbook_app/src/features/contact/model/contact_type_model.dart';
import 'package:get/get.dart';

class ContactController extends GetxController {
  var isLoading = false.obs;
  var contactTypeList = <ContactTypeModel>[].obs;
  var contactList = <ContactModel>[].obs;

  @override
  void onInit() {
    getContactList();
    super.onInit();
  }

  Future getContactTypeList() async {
    try {
      isLoading(true);
      contactTypeList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.contactTypeList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            contactTypeList.add(ContactTypeModel.fromJson(i));
            contactTypeList.refresh();
          }
        }
      } else {
        throw 'Unable to load contact type!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future getContactList() async {
    try {
      isLoading(true);
      contactList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.contactList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            contactList.add(ContactModel.fromJson(i));
            contactList.refresh();
          }
        }
      } else {
        throw 'Unable to load contact!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addContact({
    required Map<String, dynamic> body,
  }) async {
    try {
      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addContact,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Contact Added successfully!",
          bgColor: successColor,
        );
        getContactList();
      } else {
        throw 'Failed to update Contact!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> updateContact({
    required String id,
    required String name,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['contact_name'] = name;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updateContact,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Contact update successfully!",
          bgColor: successColor,
        );
        getContactList();
      } else {
        throw 'Failed to update Contact!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deleteContact({
    required String id,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deleteContact,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Contact Deleted  Successfully!",
          bgColor: successColor,
        );
        getContactList();
      } else {
        throw 'Failed to delete Contact!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }
}
