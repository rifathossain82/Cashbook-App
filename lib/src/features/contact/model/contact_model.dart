class ContactModel {
  int? id;
  String? name;
  int? bookId;
  int? businessId;
  int? contactTypeId;
  String? primaryNumber;
  String? secondaryNumber;
  String? remarks;
  String? images;
  int? status;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;

  ContactModel(
      {this.id,
        this.name,
        this.bookId,
        this.businessId,
        this.contactTypeId,
        this.primaryNumber,
        this.secondaryNumber,
        this.remarks,
        this.images,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  ContactModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    bookId = json['book_id'];
    businessId = json['business_id'];
    contactTypeId = json['contact_type_id'];
    primaryNumber = json['primary_number'];
    secondaryNumber = json['secondary_number'];
    remarks = json['remarks'];
    images = json['images'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['book_id'] = bookId;
    data['business_id'] = businessId;
    data['contact_type_id'] = contactTypeId;
    data['primary_number'] = primaryNumber;
    data['secondary_number'] = secondaryNumber;
    data['remarks'] = remarks;
    data['images'] = images;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}