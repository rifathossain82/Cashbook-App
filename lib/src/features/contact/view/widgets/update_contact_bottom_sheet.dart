import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/contact/model/contact_model.dart';
import 'package:cashbook_app/src/features/contact/model/contact_type_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future updateContactBottomSheet({
  required BuildContext context,
  required ContactModel contactModel,
}) {
  final contactController = Get.find<ContactController>();
  final formKey = GlobalKey<FormState>();
  final contactNameTextController = TextEditingController();
  final countryCodeTextController = TextEditingController();
  final mobileNumberTextController = TextEditingController();
  ContactTypeModel? selectedContactType;

  contactNameTextController.text = contactModel.name!;

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.6, //size of bottom sheet
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// heading and close button
                Row(
                  children: [
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.close,
                        color: kGrey,
                      ),
                    ),
                    Text(
                      '${'Update'.tr} ${contactModel.name}',
                      style: MyTextStyles.h2,
                    ),
                  ],
                ),
                const Divider(
                  height: 0,
                ),

                /// contact name, mobile number and contact type here
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          controller: contactNameTextController,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return Message.emptyName.tr;
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            isDense: true,
                            labelText: 'Contact Name'.tr,
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        // const SizedBox(height: 20),
                        // Row(
                        //   children: [
                        //     Expanded(
                        //       flex: 3,
                        //       child: TextFormField(
                        //         controller: countryCodeTextController,
                        //         readOnly: true,
                        //         onTap: () {},
                        //         decoration: InputDecoration(
                        //           isDense: true,
                        //           suffixIconConstraints: const BoxConstraints(
                        //             maxHeight: 25,
                        //             maxWidth: 40,
                        //             minWidth: 30,
                        //           ),
                        //           suffixIcon: Icon(
                        //             Icons.arrow_drop_down,
                        //             color: kBlackLight,
                        //             size: 20,
                        //           ),
                        //           border: OutlineInputBorder(
                        //             borderSide: BorderSide(color: kGrey),
                        //           ),
                        //           focusedBorder: OutlineInputBorder(
                        //             borderSide: BorderSide(color: kGrey),
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //     const SizedBox(width: 15),
                        //     Expanded(
                        //       flex: 7,
                        //       child: TextFormField(
                        //         controller: mobileNumberTextController,
                        //         textInputAction: TextInputAction.done,
                        //         keyboardType: TextInputType.number,
                        //         decoration: InputDecoration(
                        //           isDense: true,
                        //           labelText: 'Mobile Number(Optional)',
                        //           labelStyle: MyTextStyles.h4.copyWith(color: kGrey),
                        //           border: const OutlineInputBorder(),
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        // const SizedBox(height: 20),
                        // Text(
                        //   'Contact Type',
                        //   style: MyTextStyles.h4.copyWith(
                        //     fontWeight: FontWeight.bold,
                        //   ),
                        // ),
                        // const SizedBox(height: 8),
                        // Wrap(
                        //   children: ContactType.values
                        //       .map(
                        //         (type) => SelectableContainer(
                        //           onTap: () {
                        //             setState(() {
                        //               selectedContactType = type;
                        //             });
                        //           },
                        //           isSelected: selectedContactType == type,
                        //           borderRadius: 30,
                        //           margin: const EdgeInsets.only(right: 8),
                        //           child: Text(
                        //             type.name.capitalizedFirst,
                        //             textAlign: TextAlign.center,
                        //             style: MyTextStyles.h4.copyWith(
                        //               color: selectedContactType == type
                        //                   ? kWhite
                        //                   : kBlackLight,
                        //             ),
                        //           ),
                        //         ),
                        //       )
                        //       .toList(),
                        // ),
                      ],
                    ),
                  ),
                ),

                /// update contact button
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: KButton(
                    onPressed: () {
                      if(formKey.currentState!.validate()){
                        Navigator.pop(context);
                        contactController.updateContact(
                          id: '${contactModel.id}',
                          name: contactNameTextController.text,
                        );
                      }
                    },
                    child: Text(
                      'UPDATE CONTACT'.tr,
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kWhite,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
    },
  );
}
