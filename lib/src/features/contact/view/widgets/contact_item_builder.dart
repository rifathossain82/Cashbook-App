import 'dart:math';

import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/popup_menu_item_builder.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/contact/model/contact_model.dart';
import 'package:cashbook_app/src/features/contact/view/widgets/update_contact_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContactItemBuilder extends StatelessWidget {
  final ContactModel data;

  const ContactItemBuilder({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int index = Random().nextInt(randomColors.length);
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, data);
      },
      child: ListTile(
        dense: true,
        contentPadding: EdgeInsets.zero,
        leading: Container(
          height: 40,
          width: 40,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: kGreyLight,
          ),
          child: Text(
            data.name!.substring(0, 1).toUpperCase(),
            style: MyTextStyles.h2.copyWith(
              color: randomColors[index],
            ),
          ),
        ),
        title: Text(
          '${data.name}',
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          '${data.primaryNumber}',
          style: MyTextStyles.h5.copyWith(
            color: kGreyTextColor,
          ),
        ),
        trailing: PopupMenuButton(
          color: Get.find<ThemeController>().isDarkMode
              ? kSecondaryDarkColor
              : kWhite,
          onSelected: (value) {
            if (value == PopupMenuItemOptions.edit) {
              updateContactBottomSheet(
                context: context,
                contactModel: data,
              );
            } else {
              deleteContact(context);
            }
          },
          icon: Icon(
            Icons.more_vert_outlined,
            color: kGrey,
            size: 25,
          ),
          itemBuilder: (BuildContext context) => [
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.edit,
              icon: Icons.edit_outlined,
              iconColor: kGrey,
              title: 'Edit'.tr,
            ),
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.delete,
              icon: Icons.delete_outline,
              iconColor: kGrey,
              title: 'Delete'.tr,
            ),
          ],
        ),
      ),
    );
  }


  void deleteContact(BuildContext context) async {
    /// show alert dialog
    bool? result = await customDeleteDialog(
      context: context,
      dialogPosition: Alignment.center,
      title: 'Delete Contact?'.tr,
      contentText: "Are you sure you want to delete this contact? This can't be undone.".tr,
      negativeActionText: 'NO'.tr,
      positiveActionText: 'YES, DELETE'.tr,
    );

    if (result ?? false) {
      final contactController = Get.find<ContactController>();
      contactController.deleteContact(
        id: '${data.id}',
      );
    }
  }
}
