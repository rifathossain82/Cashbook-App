import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future addNewContactBottomSheet({
  required BuildContext context,
  required int bookId,
}) {
  final contactController = Get.find<ContactController>();
  final formKey = GlobalKey<FormState>();
  final contactNameTextController = TextEditingController();
  final primaryMobileNumberTextController = TextEditingController();
  final secondaryMobileNumberTextController = TextEditingController();
  final remarkTextController = TextEditingController();

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.8, //size of bottom sheet
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// heading and close button
                Row(
                  children: [
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.close,
                        color: kGrey,
                      ),
                    ),
                    Text(
                      'ADD MANUALLY'.tr,
                      style: MyTextStyles.h2,
                    ),
                  ],
                ),
                const Divider(
                  height: 0,
                ),

                /// contact name, mobile number and contact type here
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          controller: contactNameTextController,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return Message.emptyName.tr;
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            isDense: true,
                            labelText: 'Contact Name'.tr,
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          controller: primaryMobileNumberTextController,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return Message.emptyPhone;
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            isDense: true,
                            labelText: '${'Primary Phone No.'.tr} *',
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          controller: secondaryMobileNumberTextController,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            isDense: true,
                            labelText: 'Secondary Phone No.'.tr,
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          controller: remarkTextController,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            isDense: true,
                            labelText: 'Remark'.tr,
                            border: const OutlineInputBorder(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                /// add contact button
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: KButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        Navigator.pop(context);
                        contactController.addContact(
                          body: {
                            'contact_name': contactNameTextController.text,
                            'book_id': '$bookId',
                            'business_id': '${LocalStorage.getData(key: LocalStorageKey.selectedBusinessId)}',
                            'primary_number':
                                primaryMobileNumberTextController.text,
                            'secondary_number':
                                secondaryMobileNumberTextController.text,
                            'remarks': remarkTextController.text,
                          },
                        );
                        // Navigator.pop(
                        //   context,
                        //   'MD Sazzad Hossain',
                        // );
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person_add_alt_1_outlined,
                          color: kWhite,
                        ),
                        const SizedBox(width: 8),
                        Text(
                          'ADD CONTACT'.tr,
                          style: MyTextStyles.h4.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kWhite,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
    },
  );
}
