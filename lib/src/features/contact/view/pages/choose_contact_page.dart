import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/contact/view/widgets/add_new_contact_bottom_sheet.dart';
import 'package:cashbook_app/src/features/contact/view/widgets/contact_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChooseContactPage extends StatefulWidget {
  const ChooseContactPage({Key? key}) : super(key: key);

  @override
  State<ChooseContactPage> createState() => _ChooseContactPageState();
}

class _ChooseContactPageState extends State<ChooseContactPage> {
  /// received from add entry page
  int? bookId;

  final contactController = Get.find<ContactController>();
  final searchTextController = TextEditingController();

  @override
  void didChangeDependencies() {
    bookId = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose Contact'.tr),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings_outlined,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => addNewContactBottomSheet(
          context: context,
          bookId: bookId!,
        ),
        backgroundColor: kPrimaryLightColor,
        elevation: 0,
        icon: Icon(
          Icons.add,
          color: kWhite,
        ),
        label: Text(
          'ADD MANUALLY'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
      ),
      body: Obx(() {
        return contactController.isLoading.value
            ? const KCustomLoader()
            : contactController.contactList.isEmpty
                ? const NoDataFound()
                : Column(
                    children: [
                      TextFormField(
                        controller: searchTextController,
                        decoration: InputDecoration(
                          hintText: 'Type to search and add'.tr,
                          border: InputBorder.none,
                          prefixIcon: const Icon(Icons.search),
                        ),
                      ),
                      const KDivider(height: 0),
                      Expanded(
                        child: _buildContactList(),
                      ),
                    ],
                  );
      }),
    );
  }

  Widget _buildContactList() {
    return ListView.separated(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 10,
        bottom: 60,
      ),
      itemCount: contactController.contactList.length,
      itemBuilder: (context, index) => ContactItemBuilder(
        data: contactController.contactList[index],
      ),
      separatorBuilder: (context, index) => const SizedBox(
        height: 8,
      ),
    );
  }
}
