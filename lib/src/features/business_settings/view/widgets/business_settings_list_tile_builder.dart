import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessSettingsListTileBuilder extends StatelessWidget {
  final SettingsItemModel itemModel;

  BusinessSettingsListTileBuilder({
    Key? key,
    required this.itemModel,
  }) : super(key: key);

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: itemModel.onTap ??
          () {
            if (itemModel.routeName!.isNotEmpty || itemModel.routeName != '') {
              Navigator.pushNamed(
                context,
                itemModel.routeName!,
              );
            }
          },
      contentPadding: EdgeInsets.zero,
      leading: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: kRed.withOpacity(0.2),
        ),
        child: Icon(
          itemModel.leadingIcon,
          color: kRed,
        ),
      ),
      title: Text(
        itemModel.titleText.tr,
        style: MyTextStyles.h3.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        itemModel.subtitleText.tr,
        style: MyTextStyles.h5.copyWith(
          color: kGreyTextColor,
        ),
      ),
      trailing: Icon(
        itemModel.trailingIcon ?? Icons.arrow_forward_ios_outlined,
        color: kGrey,
        size: 20,
      ),
    );
  }
}
