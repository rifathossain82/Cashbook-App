import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeOwnerPage extends StatelessWidget {
  ChangeOwnerPage({Key? key}) : super(key: key);

  final hintTextList = [
    'Every business can hae only one owner',
    'Once you transfer ownership to new owner, your role will be changed to partner',
    'New owner can remove you from business or delete the business',
    'You will not be able to reverse this.',
  ];

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Owner'.tr),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 30,
                    ),
                    decoration: BoxDecoration(
                      color: themeController.isDarkMode
                          ? kSecondaryDarkColor
                          : kRedShadowColor,
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.warning_rounded,
                          color: kRedDeep,
                          size: 40,
                        ),
                        const SizedBox(height: 15),
                        Text(
                          'This will transfer all your permissions to new owner'
                              .tr,
                          textAlign: TextAlign.center,
                          style: MyTextStyles.h3.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 30,
                    ),
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: hintTextList.length,
                      itemBuilder: (context, index) => _buildHintText(
                        hintTextList[index],
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 15,
                      ),
                    ),
                  ),

                  /// step text and next button
                  Column(
                    children: [
                      Container(
                        height: 50,
                        width: context.screenWidth,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 15,
                        ),
                        decoration: BoxDecoration(
                          color: themeController.isDarkMode
                              ? kSecondaryDarkColor
                              : kGreyLight.withOpacity(0.5),
                        ),
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: '${'Next'.tr}: ',
                                style: MyTextStyles.h4.copyWith(
                                  color: kGreyTextColor,
                                ),
                              ),
                              TextSpan(
                                text: ' ${'Choose New Owner'.tr}',
                                style: MyTextStyles.h4,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildNextButton(context),
          ),
        ],
      ),
    );
  }

  Widget _buildHintText(String hintText) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 8,
          height: 8,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: kGrey,
          ),
        ),
        const SizedBox(width: 15),
        Expanded(
          child: Text(
            hintText,
            style: MyTextStyles.h4,
          ),
        )
      ],
    );
  }

  Widget _buildNextButton(BuildContext context) {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: KButton(
        onPressed: () {},
        child: Text(
          'Next',
          style: MyTextStyles.h3.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
      ),
    );
  }
}
