import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/circular_button_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeleteBusinessConfirmPage extends StatefulWidget {
  final int businessId;
  final String businessName;
  final List<CashbookModel> bookList;

  const DeleteBusinessConfirmPage({
    Key? key,
    required this.businessId,
    required this.businessName,
    required this.bookList,
  }) : super(key: key);

  @override
  State<DeleteBusinessConfirmPage> createState() =>
      _DeleteBusinessConfirmPageState();
}

class _DeleteBusinessConfirmPageState extends State<DeleteBusinessConfirmPage> {
  final businessController = Get.find<BusinessController>();
  final themeController = Get.find<ThemeController>();

  final formKey = GlobalKey<FormState>();
  final businessNameTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${'Delete'.tr} ${widget.businessName}"),
      ),
      body: Stack(
        children: [
          Container(
            margin: const EdgeInsets.all(15),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  /// heading rich text
                  const SizedBox(height: 10),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "${'Are you sure?'.tr} ",
                          style: MyTextStyles.h4.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: " Please type ",
                          style: MyTextStyles.h4,
                        ),
                        TextSpan(
                          text: " '${widget.businessName}' ",
                          style: MyTextStyles.h4.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: "to confirm",
                          style: MyTextStyles.h4,
                        ),
                      ],
                    ),
                  ),

                  /// business name text controller
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: businessNameTextController,
                    validator: (value) {
                      if (value.toString().isEmpty) {
                        return Message.emptyName.tr;
                      } else if (widget.businessName !=
                          businessNameTextController.text) {
                        return Message.incorrectBusinessName;
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      labelText: 'Enter Business Name'.tr,
                      isDense: true,
                      border: const OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 20),

                  /// total books card
                  Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: kGrey,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.warning_rounded,
                              color: kRedDeep,
                            ),
                            const SizedBox(width: 15),
                            Text(
                              '${widget.bookList.length} books will be deleted',
                              style: MyTextStyles.h3.copyWith(
                                fontWeight: FontWeight.bold,
                                color: kRedDeep,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 20),
                        Wrap(
                          children: widget.bookList
                              .map((book) => _buildBookItem(book.name ?? ''))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomButtons(context),
          ),
        ],
      ),
    );
  }

  Widget _buildBookItem(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      margin: const EdgeInsets.all(4.0),
      decoration: BoxDecoration(
        color:
            themeController.isDarkMode ? kSecondaryDarkColor : kRedShadowColor,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        text,
        style: MyTextStyles.h4.copyWith(
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  Widget _buildBottomButtons(BuildContext context) {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          KOutlinedButton(
            onPressed: deleteBusinessMethod,
            borderColor: kRedDeep,
            width: context.screenWidth * 0.4,
            child: Obx(() {
              return businessController.isLoading.value
                  ? CircularButtonLoader(
                      indicatorColor: kRed,
                    )
                  : Row(
                      children: [
                        Icon(
                          Icons.delete_outline,
                          color: kRed,
                        ),
                        const SizedBox(width: 8),
                        Text(
                          'DELETE'.tr,
                          style: MyTextStyles.h3.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kRedDeep,
                          ),
                        ),
                      ],
                    );
            }),
          ),
          KButton(
            onPressed: () => Get.back(),
            width: context.screenWidth * 0.4,
            child: Text(
              'CANCEL'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void deleteBusinessMethod() {
    if (formKey.currentState!.validate()) {
      businessController.deleteBusiness(
        businessId: '${widget.businessId}',
      );
    }
  }
}
