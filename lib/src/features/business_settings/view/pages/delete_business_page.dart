import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/add_business/model/business_model.dart';
import 'package:cashbook_app/src/features/business_settings/view/pages/delete_business_confirm_page.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeleteBusinessPage extends StatefulWidget {
  const DeleteBusinessPage({Key? key}) : super(key: key);

  @override
  State<DeleteBusinessPage> createState() => _DeleteBusinessPageState();
}

class _DeleteBusinessPageState extends State<DeleteBusinessPage> {
  final businessController = Get.find<BusinessController>();
  final cashbookController = Get.find<CashbookController>();
  final themeController = Get.find<ThemeController>();

  final businessId = LocalStorage.getData(
    key: LocalStorageKey.selectedBusinessId,
  );

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor:
            themeController.isDarkMode ? kPrimaryDarkColor : kRedShadowColor,
        appBar: AppBar(
          title: businessController.isLoading.value
              ? Text("Delete".tr)
              : Text(
                  "${'Delete'.tr} ${getSelectedBusinessName(businessController.businessList)}",
                ),
        ),
        body: Stack(
          children: [
            Container(
              margin: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Icon(
                    Icons.warning_rounded,
                    color: kRedDeep,
                    size: 40,
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Are you sure?'.tr,
                    textAlign: TextAlign.center,
                    style: MyTextStyles.h2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'This business will be deleted permanently. All the team members will lose access'
                        .tr,
                    textAlign: TextAlign.center,
                    style: MyTextStyles.h3,
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    height: 4,
                    width: 20,
                    decoration: BoxDecoration(
                      color: kRedLight,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                  Text(
                    'You are deleting'.tr,
                    style: MyTextStyles.h3.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  cashbookController.isLoading.value
                      ? const KCustomLoader()
                      : Container(
                          margin: const EdgeInsets.only(top: 20),
                          padding: const EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            color: themeController.isDarkMode
                                ? kSecondaryDarkColor
                                : kWhite,
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: IntrinsicHeight(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      '${cashbookController.cashbookList.length}',
                                      style: MyTextStyles.h1.copyWith(
                                        fontWeight: FontWeight.bold,
                                        color: kRedDeep,
                                      ),
                                    ),
                                    Text(
                                      'Books'.tr,
                                      style: MyTextStyles.h3.copyWith(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                                const VerticalDivider(
                                  thickness: 1,
                                  width: 10,
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      '0',
                                      style: MyTextStyles.h1.copyWith(
                                        fontWeight: FontWeight.bold,
                                        color: kRedDeep,
                                      ),
                                    ),
                                    Text(
                                      'Entries'.tr,
                                      style: MyTextStyles.h3.copyWith(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: _buildBottomButtons(context),
            ),
          ],
        ),
      );
    });
  }

  String getSelectedBusinessName(List<BusinessModel> businessList) {
    String businessName = '';
    for (var business in businessList) {
      if (business.id == businessId) {
        businessName = business.name ?? '';
      }
    }

    return businessName;
  }

  Widget _buildBottomButtons(BuildContext context) {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          KOutlinedButton(
            onPressed: () {
              Get.to(
                () => DeleteBusinessConfirmPage(
                  businessId: businessId,
                  businessName: getSelectedBusinessName(
                    businessController.businessList,
                  ),
                  bookList: cashbookController.cashbookList,
                ),
              );
            },
            borderColor: kRedDeep,
            width: context.screenWidth * 0.4,
            child: Text(
              'CONTINUE'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kRedDeep,
              ),
            ),
          ),
          KButton(
            onPressed: () => Get.back(),
            width: context.screenWidth * 0.4,
            child: Text(
              'GO BACK'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
