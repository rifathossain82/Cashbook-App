import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/business_settings/view/widgets/business_settings_list_tile_builder.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessSettingsPage extends StatefulWidget {
  const BusinessSettingsPage({Key? key}) : super(key: key);

  @override
  State<BusinessSettingsPage> createState() => _BusinessSettingsPageState();
}

class _BusinessSettingsPageState extends State<BusinessSettingsPage> {
  final themeController = Get.find<ThemeController>();

  final settingsList = [
    SettingsItemModel(
      routeName: RouteGenerator.changeOwner,
      leadingIcon: Icons.swap_horiz_outlined,
      titleText: 'Change Owner',
      subtitleText: 'Current Owner: You',
    ),
    SettingsItemModel(
      routeName: RouteGenerator.deleteBusiness,
      leadingIcon: Icons.delete_outline,
      titleText: 'Delete Business',
      subtitleText: 'Delete all the data of this business permanently',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          themeController.isDarkMode ? kPrimaryDarkColor : kGreyLight,
      appBar: AppBar(
        title: Text('Business Settings'.tr),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 20,
              ),
              child: Text(
                'General'.tr,
                textAlign: TextAlign.start,
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: themeController.isDarkMode
                      ? kWhite.withOpacity(0.7)
                      : kBlackLight.withOpacity(0.7),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color:
                    themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
              ),
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: settingsList.length,
                itemBuilder: (context, index) =>
                    BusinessSettingsListTileBuilder(
                  itemModel: settingsList[index],
                ),
                separatorBuilder: (context, index) => Divider(
                  height: 10,
                  color: kGreyLight,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
