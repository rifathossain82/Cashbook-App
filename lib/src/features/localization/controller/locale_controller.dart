import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/localization/app_localization.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/features/localization/model/locale_model.dart';
import 'package:get/get.dart';

class LocaleController extends GetxController {
  var selectedLocaleData = AppLocalization.locals.first.obs;

  @override
  void onInit() {
    setInitialLocale();
    super.onInit();
  }

  setInitialLocale() {
    var languageCode = LocalStorage.getData(
      key: LocalStorageKey.localeLanguageCode,
    );

    if (languageCode != null) {
      for (var localData in AppLocalization.locals) {
        if(localData.locale.languageCode == languageCode){
          selectedLocaleData.value = localData;
        }
      }
    }
  }

  updateLocale(LocaleData localeData) {
    selectedLocaleData.value = localeData;
    Get.updateLocale(selectedLocaleData.value.locale);

    /// to save in local storage
    LocalStorage.saveData(
      key: LocalStorageKey.localeLanguageCode,
      data: selectedLocaleData.value.locale.languageCode,
    );
  }
}
