import 'package:flutter/material.dart';

class LocaleData {
  Locale locale;
  String languageName;
  String localeLanguageName;
  String countryName;
  String languageCode;
  String? flag;

  LocaleData({
    required this.locale,
    required this.languageName,
    required this.localeLanguageName,
    required this.countryName,
    required this.languageCode,
    this.flag,
  });
}
