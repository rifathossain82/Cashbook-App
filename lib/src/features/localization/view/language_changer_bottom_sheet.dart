import 'package:cashbook_app/src/core/localization/app_localization.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/localization/controller/locale_controller.dart';
import 'package:cashbook_app/src/features/localization/model/locale_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void languageChangerBottomSheet(BuildContext context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(10),
      ),
    ),
    builder: (context) {
      return SizedBox(
        height: MediaQuery.of(context).size.height * 0.7,
        child: Column(
          children: [
            const _BottomSheetTitle(),
            const KDivider(height: 0),
            _BottomSheetContent(),
          ],
        ),
      );
    },
  );
}

class _BottomSheetTitle extends StatelessWidget {
  const _BottomSheetTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.close,
            size: 20,
          ),
        ),
        const SizedBox(width: 15),
        Text(
          'Change Language'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}

class _BottomSheetContent extends StatelessWidget {
  _BottomSheetContent({Key? key}) : super(key: key);

  final localeController = Get.put(LocaleController());

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(builder: (context, setState) {
      return GridView.builder(
        padding: const EdgeInsets.all(15),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 15,
          crossAxisSpacing: 15,
          childAspectRatio: 2.5,
        ),
        itemCount: AppLocalization.locals.length,
        itemBuilder: (context, index) => _LanguageItemBuilder(
          onTap: () {
            localeController.updateLocale(AppLocalization.locals[index]);
            Navigator.pop(context);
          },
          isSelected: localeController.selectedLocaleData.value ==
              AppLocalization.locals[index],
          localeData: AppLocalization.locals[index],
        ),
      );
    });
  }
}

class _LanguageItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final bool isSelected;
  final LocaleData localeData;

  _LanguageItemBuilder({
    Key? key,
    required this.onTap,
    required this.isSelected,
    required this.localeData,
  }) : super(key: key);

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          color: isSelected ? kGreen.withOpacity(0.2) : kGrey.withOpacity(0.2),
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            width: isSelected ? 1 : 0,
            color: isSelected ? kGreen : Colors.transparent,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  localeData.languageName,
                  style: MyTextStyles.h5.copyWith(
                    color: isSelected
                        ? kGreen
                        : themeController.isDarkMode
                            ? kWhite
                            : kBlackLight,
                  ),
                ),
                Text(
                  localeData.localeLanguageName,
                  style: MyTextStyles.h3.copyWith(
                    fontWeight: FontWeight.w500,
                    color: isSelected
                        ? kGreen
                        : themeController.isDarkMode
                            ? kWhite
                            : kBlackLight,
                  ),
                ),
              ],
            ),
            isSelected
                ? Icon(
                    Icons.check_circle_rounded,
                    color: kGreen,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
