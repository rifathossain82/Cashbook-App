class BookTransactionModel {
  List<Transactions>? transactions;
  int? finalBalace;
  int? totalCashIn;
  int? totalCashOut;
  dynamic permission;

  BookTransactionModel(
      {this.transactions,
        this.finalBalace,
        this.totalCashIn,
        this.totalCashOut,
        this.permission});

  BookTransactionModel.fromJson(Map<String, dynamic> json) {
    if (json['Transactions'] != null) {
      transactions = <Transactions>[];
      json['Transactions'].forEach((v) {
        transactions!.add(Transactions.fromJson(v));
      });
    }
    finalBalace = json['finalBalace'];
    totalCashIn = json['totalCashIn'];
    totalCashOut = json['totalCashOut'];
    permission = json['permission'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (transactions != null) {
      data['Transactions'] = transactions!.map((v) => v.toJson()).toList();
    }
    data['finalBalace'] = finalBalace;
    data['totalCashIn'] = totalCashIn;
    data['totalCashOut'] = totalCashOut;
    data['permission'] = permission;
    return data;
  }
}

class Transactions {
  int? id;
  String? date;
  String? time;
  String? type;
  int? bookContactId;
  int? contactTypeId;
  int? amount;
  int? pageNo;
  int? bookId;
  int? transactionCategoryId;
  int? paymentMethodId;
  String? description;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;
  String? methodName;
  String? transactionCategoryName;
  String? documents;
  String? contactName;
  String? contactTypeName;
  String? typeName;
  int? balance;
  List<dynamic>? documentFile;
  dynamic paymentMode;

  Transactions(
      {this.id,
        this.date,
        this.time,
        this.type,
        this.bookContactId,
        this.contactTypeId,
        this.amount,
        this.pageNo,
        this.bookId,
        this.transactionCategoryId,
        this.paymentMethodId,
        this.description,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.methodName,
        this.transactionCategoryName,
        this.documents,
        this.contactName,
        this.contactTypeName,
        this.typeName,
        this.balance,
        this.documentFile,
        this.paymentMode});

  Transactions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    time = json['time'];
    type = json['type'];
    bookContactId = json['book_contact_id'];
    contactTypeId = json['contact_type_id'];
    amount = json['amount'];
    pageNo = json['page_no'];
    bookId = json['book_id'];
    transactionCategoryId = json['transaction_category_id'];
    paymentMethodId = json['payment_method_id'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    methodName = json['methodName'];
    transactionCategoryName = json['transactionCategoryName'];
    documents = json['documents'];
    contactName = json['contactName'];
    contactTypeName = json['contactTypeName'];
    typeName = json['typeName'];
    balance = json['balance'];
    documentFile = json['documentFile'];
    paymentMode = json['payment_mode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['date'] = date;
    data['time'] = time;
    data['type'] = type;
    data['book_contact_id'] = bookContactId;
    data['contact_type_id'] = contactTypeId;
    data['amount'] = amount;
    data['page_no'] = pageNo;
    data['book_id'] = bookId;
    data['transaction_category_id'] = transactionCategoryId;
    data['payment_method_id'] = paymentMethodId;
    data['description'] = description;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    data['methodName'] = methodName;
    data['transactionCategoryName'] = transactionCategoryName;
    data['documents'] = documents;
    data['contactName'] = contactName;
    data['contactTypeName'] = contactTypeName;
    data['typeName'] = typeName;
    data['balance'] = balance;
    data['documentFile'] = documentFile;
    data['payment_mode'] = paymentMode;
    return data;
  }
}