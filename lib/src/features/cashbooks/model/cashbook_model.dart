class CashbookModel {
  int? id;
  String? name;
  int? businessId;
  int? userId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;
  Business? business;

  CashbookModel(
      {this.id,
        this.name,
        this.businessId,
        this.userId,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.business});

  CashbookModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessId = json['business_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    business = json['business'] != null
        ? Business.fromJson(json['business'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_id'] = businessId;
    data['user_id'] = userId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    if (business != null) {
      data['business'] = business!.toJson();
    }
    return data;
  }
}

class Business {
  int? id;
  String? name;
  int? businessCategoryId;
  int? businessTypeId;
  int? userId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;

  Business(
      {this.id,
        this.name,
        this.businessCategoryId,
        this.businessTypeId,
        this.userId,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Business.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessCategoryId = json['business_category_id'];
    businessTypeId = json['business_type_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_category_id'] = businessCategoryId;
    data['business_type_id'] = businessTypeId;
    data['user_id'] = userId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}