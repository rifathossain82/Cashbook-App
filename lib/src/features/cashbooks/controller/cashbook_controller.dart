import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/auth/controller/auth_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:get/get.dart';

class CashbookController extends GetxController {
  var isLoading = false.obs;
  var cashbookList = <CashbookModel>[].obs;
  var bookTransaction = BookTransactionModel(transactions: []).obs;

  @override
  void onInit() {

    /// fetch selected business id from locale storage
    var businessId = LocalStorage.getData(
      key: LocalStorageKey.selectedBusinessId,
    );

    /// if the business id is null then business will be the last items id of login response sidebar list
    businessId ??= AuthController().loginResponse.value.sidebar?.last.id;

    /// load cashbooks based on selected business id
    getCashbookList(
      id: '$businessId',
    );

    super.onInit();
  }

  Future getCashbookList({required String id}) async {
    try {
      isLoading(true);
      cashbookList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.cashbookList(id),
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            cashbookList.add(CashbookModel.fromJson(i));
            cashbookList.refresh();
          }
        }
      } else {
        throw 'Unable to load cashbooks!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addCashbook({
    required String name,
    required String businessID,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['book_name'] = name;
      map['businessId'] = businessID;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addBook,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Book added successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to add new book!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> updateCashbook({
    required String id,
    required String name,
    required String businessID,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['book_name'] = name;
      map['businessId'] = businessID;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updateBook,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Book update successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to update book!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deleteCashbook({
    required String id,
    required String businessID,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['business_id'] = businessID;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deleteBook,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Book Deleted  Successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to Delete Book!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deleteCashbookWithTransactions({
    required String id,
    required String businessID,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['business_id'] = businessID;

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.forceDeleteBook,
          params: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Book Deleted  Successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to Delete Book!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future getBookTransaction({required String id}) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.bookTransaction(id),
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          bookTransaction.value = BookTransactionModel.fromJson(data);
        }
      } else {
        throw 'Unable to load book transaction!';
      }
    } catch (e) {
      kPrint(e);
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addBookTransaction({
    required Map<String, dynamic> body,
  }) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addBookTransaction,
          body: body,
        ),
      );

      if (responseBody != null) {
        Get.back();
        kSnackBar(
          message: "Transaction added successfully!",
          bgColor: successColor,
        );
        getBookTransaction(id: body['book_id']);
      } else {
        throw 'Failed to add new Transaction!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> updateBookTransaction({
    required Map<String, dynamic> body,
  }) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updateBookTransaction,
          body: body,
        ),
      );

      if (responseBody != null) {
        Get
          ..back()
          ..back();
        kSnackBar(
          message: "Transaction update successfully!",
          bgColor: successColor,
        );
        getBookTransaction(id: body['book_id']);
      } else {
        throw 'Failed to update Transaction!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> deleteBookTransaction({
    required String transactionId,
    required String bookId,
  }) async {
    try {
      isLoading(true);

      Map<String, dynamic> body = {
        'id': transactionId,
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deleteBookTransaction,
          body: body,
        ),
      );

      if (responseBody != null) {
        Get.back();
        kSnackBar(
          message: "Transaction deleted successfully!",
          bgColor: successColor,
        );
        getBookTransaction(id: bookId);
      } else {
        throw 'Failed to delete Transaction!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
