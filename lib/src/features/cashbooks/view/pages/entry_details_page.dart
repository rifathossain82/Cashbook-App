import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/core/widgets/status_builder.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/pages/edit_entry_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class EntryDetailsPage extends StatefulWidget {
  final Transactions transactions;
  final CashbookModel cashbookModel;

  const EntryDetailsPage({
    Key? key,
    required this.transactions,
    required this.cashbookModel,
  }) : super(key: key);

  @override
  State<EntryDetailsPage> createState() => _EntryDetailsPageState();
}

class _EntryDetailsPageState extends State<EntryDetailsPage> {
  final cashbookController = Get.find<CashbookController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Entry Details'.tr),
        actions: [
          IconButton(
            onPressed: deleteEntryMethod,
            icon: Icon(
              Icons.delete_outline,
              color: kRed,
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      body: Obx(() {
        return cashbookController.isLoading.value
            ? const KCustomLoader()
            : Stack(
                children: [
                  SizedBox(
                    height: context.screenHeight,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          _buildEntryDetailsCard(),
                          _buildAuthorCard(),
                          const SizedBox(height: 70),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: _buildShareEntryButton(),
                  ),
                ],
              );
      }),
    );
  }

  Widget _buildEntryDetailsCard() {
    return Container(
      margin: const EdgeInsets.all(15),
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, 3),
                  spreadRadius: 0,
                  blurRadius: 4,
                  color: Get.find<ThemeController>().isDarkMode
                      ? kItemBlueDarkShadowColor
                      : kItemBlueShadowColor,
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      widget.transactions.typeName ?? "",
                      style: MyTextStyles.h4,
                    ),
                    Expanded(
                      child: RichText(
                        textAlign: TextAlign.end,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'On ${DateFormat('dd MMM yyyy').format(
                                DateTime.parse(
                                  widget.transactions.date!,
                                ),
                              )}',
                              style: MyTextStyles.h4,
                            ),
                            TextSpan(
                              text: ',  ${widget.transactions.time!}',
                              style: MyTextStyles.h4,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  '${widget.transactions.amount}',
                  style: MyTextStyles.h1.copyWith(
                    color: widget.transactions.typeName == 'Cash Out'
                        ? kRed
                        : kGreen,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const KDivider(height: 20),
                RichText(
                  textAlign: TextAlign.end,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: widget.transactions.contactName ?? '',
                        style: MyTextStyles.h4,
                      ),
                      TextSpan(
                        text: '  (${widget.transactions.contactTypeName!})',
                        style: MyTextStyles.h5.copyWith(
                          color: kGreyTextColor,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
                widget.transactions.description != null
                    ? Text(
                        '${widget.transactions.description}',
                        maxLines: 2,
                        style: MyTextStyles.h5,
                      )
                    : Container(),
                const KDivider(height: 10),
                Row(
                  children: [
                    StatusBuilder(
                      status: widget.transactions.transactionCategoryName ?? '',
                      statusColor: kPrimaryLightColor,
                    ),
                    const SizedBox(width: 8),
                    StatusBuilder(
                      status: widget.transactions.methodName ?? '',
                      statusColor: kBlue,
                    ),
                  ],
                ),
                const KDivider(height: 20),
                TextButton(
                  onPressed: () => Get.to(
                    () => EditEntryPage(
                      transactions: widget.transactions,
                      cashbookModel: widget.cashbookModel,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.edit_outlined,
                        color: kPrimaryLightColor,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'EDIT ENTRY'.tr,
                        style: MyTextStyles.h4.copyWith(
                          color: kPrimaryLightColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            child: Container(
              height: 5,
              width: context.screenWidth,
              decoration: BoxDecoration(
                color:
                    widget.transactions.typeName == 'Cash Out' ? kRed : kGreen,
                borderRadius: const BorderRadius.vertical(
                  top: Radius.circular(8),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAuthorCard() {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 3),
            spreadRadius: 0,
            blurRadius: 4,
            color: Get.find<ThemeController>().isDarkMode
                ? kItemBlueDarkShadowColor
                : kItemBlueShadowColor,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Created By".tr,
                style: MyTextStyles.h4.copyWith(
                  color: kGreyTextColor,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "You",
                    textAlign: TextAlign.end,
                    style: MyTextStyles.h4.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kGreyTextColor,
                    ),
                  ),
                  RichText(
                    textAlign: TextAlign.end,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'On ${DateFormat('dd MMM yyyy').format(
                            DateTime.parse(
                              widget.transactions.date!,
                            ),
                          )}',
                          style: MyTextStyles.h5.copyWith(
                            color: kGreyTextColor,
                          ),
                        ),
                        TextSpan(
                          text: ',  ${widget.transactions.time!}',
                          style: MyTextStyles.h5.copyWith(
                            color: kGreyTextColor,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildShareEntryButton() {
    return Container(
      height: 60,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: KButton(
        onPressed: () {},
        bgColor: kGreen,
        child: Row(
          children: [
            Icon(
              Icons.whatsapp,
              color: kWhite,
            ),
            const SizedBox(width: 8),
            Text(
              'SHARE ENTRY'.tr,
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void deleteEntryMethod() async {
    bool? result = await customDialog(
      context: context,
      dialogPosition: Alignment.center,
      title: 'Delete Entry?'.tr,
      content: Text(
        "Are you sure you want to delete this entry? This can't be undone.".tr,
        style: MyTextStyles.h4,
      ),
      actions: [
        KOutlinedButton(
          onPressed: () => Navigator.pop(context, true),
          width: context.screenWidth * 0.4,
          child: Text(
            'YES, DELETE'.tr,
            style: MyTextStyles.h4.copyWith(
              color: kPrimaryLightColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        KButton(
          onPressed: () => Navigator.pop(context, false),
          width: context.screenWidth * 0.4,
          child: Text(
            'NO'.tr,
            style: MyTextStyles.h4.copyWith(
              color: kWhite,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );

    if (result ?? false) {
      cashbookController.deleteBookTransaction(
        transactionId: '${widget.transactions.id}',
        bookId: '${widget.cashbookModel.id}',
      );
    }
  }
}
