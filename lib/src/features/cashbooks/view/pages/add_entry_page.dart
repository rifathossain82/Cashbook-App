import 'dart:io';

import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/image_services.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/circular_button_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_amount_text_form_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_attach_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_category_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_contact_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_date_or_time_select_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_icon_text_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_payment_method_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_remark_text_field.dart';
import 'package:cashbook_app/src/features/category/model/transaction_category_model.dart';
import 'package:cashbook_app/src/features/category/view/pages/choose_category_page.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/contact/model/contact_model.dart';
import 'package:cashbook_app/src/features/contact/model/contact_type_model.dart';
import 'package:cashbook_app/src/features/payment_modes/model/payment_method_model.dart';
import 'package:cashbook_app/src/features/payment_modes/view/pages/choose_payment_modes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddEntryPage extends StatefulWidget {
  const AddEntryPage({Key? key}) : super(key: key);

  @override
  State<AddEntryPage> createState() => _AddEntryPageState();
}

class _AddEntryPageState extends State<AddEntryPage> {
  /// received from book details page
  TransactionType? transactionType;
  CashbookModel? cashbookModel;

  final cashbookController = Get.find<CashbookController>();
  final contactController = Get.find<ContactController>();
  final themeController = Get.find<ThemeController>();

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  ContactModel? selectedContact;
  ContactTypeModel? selectedContactType;
  TransactionCategoryModel? selectedCategory;
  PaymentMethodModel? selectedPaymentMethod;

  final formKey = GlobalKey<FormState>();
  final amountTextController = TextEditingController();
  final remarkTextController = TextEditingController();
  final contactTextController = TextEditingController();
  final categoryTextController = TextEditingController();
  final paymentMethodTextController = TextEditingController();
  File? selectedAttachImage;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? time = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (time != null && time != selectedTime) {
      setState(() {
        selectedTime = time;
      });
    }
  }

  @override
  void initState() {
    contactController.getContactTypeList();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    transactionType = context.getArguments[0];
    cashbookModel = context.getArguments[1];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          transactionType! == TransactionType.cashIn
              ? 'Add Cash In Entry'.tr
              : 'Add Cash Out Entry'.tr,
          style: MyTextStyles.h3.copyWith(
            color: transactionType! == TransactionType.cashIn ? kGreen : kRed,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings_outlined,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      body: Stack(
        children: [
          _buildAddCashInEntryBody(),
          Positioned(
            bottom: 0,
            child: _buildBottomButtons(),
          ),
        ],
      ),
    );
  }

  Widget _buildAddCashInEntryBody() {
    return Container(
      padding: const EdgeInsets.all(15),
      height: context.screenHeight,
      child: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// date and time
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AddEntryDateOrTimeSelectButton(
                    onTap: () => _selectDate(context),
                    leadingIcon: Icons.calendar_month,
                    title: DateFormat('dd-MM-yyyy').format(selectedDate),
                  ),
                  AddEntryDateOrTimeSelectButton(
                    onTap: () => _selectTime(context),
                    leadingIcon: Icons.access_time_outlined,
                    title: selectedTime.format(context),
                  ),
                ],
              ),
              const SizedBox(height: 15),

              AddEntryAmountTextFormField(
                controller: amountTextController,
              ),
              Obx(() {
                return contactController.isLoading.value
                    ? const Center(child: CircularProgressIndicator())
                    : contactController.contactTypeList.isEmpty
                        ? Text('There is no contact type!'.tr)
                        : DropdownButtonFormField(
                            dropdownColor: themeController.isDarkMode
                                ? kSecondaryDarkColor
                                : kWhite,
                            items: contactController.contactTypeList
                                .map(
                                  (e) => DropdownMenuItem(
                                    value: e,
                                    child: Text(
                                      e.name!.capitalizedFirst,
                                    ),
                                  ),
                                )
                                .toList(),
                            isDense: true,
                            hint: Text(
                              'Select Contact Type'.tr,
                              style: MyTextStyles.h4.copyWith(
                                color: kGreyTextColor,
                              ),
                            ),
                            value: selectedContactType,
                            style: MyTextStyles.h4,
                            onChanged: (value) {
                              setState(() {
                                selectedContactType = value;
                              });
                            },
                            validator: (value) {
                              if (selectedContactType == null) {
                                return Message.emptyContactType.tr;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              labelText: '${'Contact Type'.tr} *',
                              isDense: true,
                              border: const OutlineInputBorder(),
                            ),
                          );
              }),
              const SizedBox(height: 15),
              AddEntryContactTextField(
                onTap: () async {
                  var result = await Navigator.pushNamed(
                    context,
                    RouteGenerator.chooseContact,
                    arguments: cashbookModel!.id,
                  );
                  if (result != null) {
                    selectedContact = result as ContactModel;
                    contactTextController.text = selectedContact!.name!;
                  }
                },
                controller: contactTextController,
              ),
              AddEntryRemarkTextField(
                controller: remarkTextController,
              ),
              selectedAttachImage != null
                  ? IntrinsicHeight(
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: FileImage(selectedAttachImage!),
                              ),
                            ),
                          ),
                          const SizedBox(width: 30),
                          AddEntryIconTextButton(
                            onTap: () => openImageSourceSelectorDialog(context),
                            leadingIcon: Icons.edit,
                            title: 'CHANGE'.tr,
                          ),
                          const VerticalDivider(
                            thickness: 1,
                            indent: 15,
                            endIndent: 15,
                            width: 30,
                          ),
                          AddEntryIconTextButton(
                            onTap: () {
                              setState(() {
                                selectedAttachImage = null;
                              });
                            },
                            leadingIcon: Icons.delete_outline,
                            title: 'DELETE'.tr,
                          ),
                        ],
                      ),
                    )
                  : AddEntryAttachButton(
                      onTap: () => openImageSourceSelectorDialog(context),
                    ),
              const SizedBox(height: 15),
              AddEntryCategoryTextField(
                onTap: () async {
                  var result = await Get.to(
                    () => ChooseCategoryPage(
                      categoryModel: selectedCategory,
                    ),
                  );
                  if (result != null) {
                    setState(() {
                      selectedCategory = result as TransactionCategoryModel;
                      categoryTextController.text =
                          selectedCategory!.name ?? '';
                    });
                  }
                },
                controller: categoryTextController,
              ),
              AddEntryPaymentMethodTextField(
                onTap: () async {
                  var result = await Get.to(
                    () => ChoosePaymentModesPage(
                      paymentMethod: selectedPaymentMethod,
                    ),
                  );
                  if (result != null) {
                    setState(() {
                      selectedPaymentMethod = result as PaymentMethodModel;
                      paymentMethodTextController.text =
                          selectedPaymentMethod!.name ?? '';
                    });
                  }
                },
                controller: paymentMethodTextController,
              ),
              // /// payment mode
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 15),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Payment Method',
              //         style: MyTextStyles.h4.copyWith(
              //           fontWeight: FontWeight.w600,
              //           color: kGreyTextColor,
              //         ),
              //       ),
              //       const SizedBox(height: 5),
              //       Obx(() {
              //         return paymentMethodController.isLoading.value
              //             ? const Center(
              //                 child: CircularProgressIndicator(),
              //               )
              //             : paymentMethodController.paymentMethodList.isEmpty
              //                 ? const Center(
              //                     child: Text('No Payment Methods Found!'),
              //                   )
              //                 : Wrap(
              //                     alignment: WrapAlignment.start,
              //                     children: [
              //                       Wrap(
              //                         children: paymentMethodController
              //                             .paymentMethodList
              //                             .map(
              //                               (method) => SelectableContainer(
              //                                 onTap: () {
              //                                   setState(() {
              //                                     selectedPaymentMethod = method;
              //                                   });
              //                                 },
              //                                 isSelected:
              //                                     selectedPaymentMethod == method,
              //                                 borderRadius: 30,
              //                                 margin: const EdgeInsets.only(
              //                                   right: 8,
              //                                   bottom: 10,
              //                                 ),
              //                                 child: Text(
              //                                   method.name ?? "",
              //                                   textAlign: TextAlign.center,
              //                                   style: MyTextStyles.h4.copyWith(
              //                                     color: selectedPaymentMethod == method
              //                                         ? kWhite
              //                                         : kBlackLight,
              //                                   ),
              //                                 ),
              //                               ),
              //                             )
              //                             .toList(),
              //                       ),
              //                       GestureDetector(
              //                         onTap: () async {
              //                           var result = await Navigator.pushNamed(
              //                             context,
              //                             RouteGenerator.choosePaymentModes,
              //                           );
              //                           if (result != null) {
              //                             setState(() {
              //                               // if (paymentMethodController.paymentMethodList.length == 3) {
              //                               //   paymentMethodController.paymentMethodList.removeLast();
              //                               // }
              //                               // paymentMethodController.paymentMethodList.insert(
              //                               //     2, result);
              //                               // selectedPaymentMethod =
              //                               //     result.toString();
              //                             });
              //                           }
              //                         },
              //                         behavior: HitTestBehavior.opaque,
              //                         child: Padding(
              //                           padding: const EdgeInsets.symmetric(
              //                             horizontal: 15,
              //                             vertical: 8,
              //                           ),
              //                           child: Row(
              //                             crossAxisAlignment:
              //                                 CrossAxisAlignment.center,
              //                             mainAxisSize: MainAxisSize.min,
              //                             children: [
              //                               Text(
              //                                 'Show More',
              //                                 style: MyTextStyles.h4.copyWith(
              //                                   color: kPrimaryLightColor,
              //                                   fontWeight: FontWeight.bold,
              //                                 ),
              //                               ),
              //                               const SizedBox(width: 8),
              //                               Icon(
              //                                 Icons.arrow_drop_down,
              //                                 color: kPrimaryLightColor,
              //                               ),
              //                             ],
              //                           ),
              //                         ),
              //                       ),
              //                     ],
              //                   );
              //       }),
              //     ],
              //   ),
              // ),

              const SizedBox(height: 70),
            ],
          ),
        ),
      ),
    );
  }

  void openImageSourceSelectorDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Select Photo Using".tr,
      dialogPosition: Alignment.bottomCenter,
      actions: [
        /// image from camera
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.cameraImage();
            var imageFile = await ImageServices.getImageFile(img);
            setState(() {
              selectedAttachImage = imageFile;
              Navigator.pop(context);
            });
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimarySwatchColor,
          bgColor: themeController.isDarkMode ? kPrimaryDarkColor : kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                color: kPrimarySwatchColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Camera'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),

        /// image from gallery
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.galleryImage();
            var imageFile = await ImageServices.getImageFile(img);
            setState(() {
              selectedAttachImage = imageFile;
              Navigator.pop(context);
            });
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimarySwatchColor,
          bgColor: themeController.isDarkMode ? kPrimaryDarkColor : kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.image,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Gallery'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildBottomButtons() {
    return Container(
      height: 60,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Obx(() {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            KOutlinedButton(
              onPressed: () {},
              width: context.screenWidth * 0.58,
              borderColor: kPrimaryLightColor,
              bgColor:
                  themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
              child: Text(
                'SAVE & ADD NEW'.tr,
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ),
            KButton(
              onPressed: saveMethod,
              width: context.screenWidth * 0.32,
              child: cashbookController.isLoading.value
                  ? const CircularButtonLoader()
                  : Text(
                      'SAVE'.tr,
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kWhite,
                      ),
                    ),
            ),
          ],
        );
      }),
    );
  }

  void saveMethod() {
    if (formKey.currentState!.validate()) {
      cashbookController.addBookTransaction(
        body: {
          'date': DateFormat('yyyy-MM-dd').format(selectedDate),
          'time': selectedTime.format(context),
          'type': transactionType!.name,
          'book_contact_id': '${selectedContact!.id}',
          'book_contact_name': '${selectedContact!.name}',
          'contact_type_id': '${selectedContactType?.id}',
          'contact_type_name': selectedContactType!.name?.capitalizedFirst,
          'amount': amountTextController.text,
          'book_id': '${cashbookModel!.id}',
          'book_name': '${cashbookModel!.name}',
          'transaction_category_id': '${selectedCategory!.id}',
          'transaction_category_name': '${selectedCategory!.name}',
          'payment_method_id': '${selectedPaymentMethod!.id}',
          'payment_method_name': '${selectedPaymentMethod!.name}',
          'description': remarkTextController.text,
        },
      );
    }
  }
}
