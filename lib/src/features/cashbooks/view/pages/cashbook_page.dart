import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/business_leading_icon.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/auth/controller/auth_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/view/pages/book_details_page.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_new_book_bottom_sheet_widget.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_new_book_card.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/business_item_builder.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/cashbook_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CashbookPage extends StatefulWidget {
  const CashbookPage({Key? key}) : super(key: key);

  @override
  State<CashbookPage> createState() => _CashbookPageState();
}

class _CashbookPageState extends State<CashbookPage> {
  final businessController = Get.find<BusinessController>();
  final cashbookController = Get.find<CashbookController>();

  String selectedSort = 'Last Update';
  List<String> sortList = [
    'Last Update',
    'Name (A to Z)',
    'Net Balance (High to Low)',
    'Net Balance (Low to High)',
    'Last Created',
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: _buildCashbookAppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () => addNewBookBottomSheet(context: context),
          backgroundColor: kPrimaryLightColor,
          elevation: 0,
          child: const Icon(Icons.add),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 15,
                  top: 15,
                  bottom: 15,
                ),
                child: Row(
                  children: [
                    Text(
                      'Your Books'.tr,
                      style: MyTextStyles.h4.copyWith(
                        color: kGrey,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () => booksSortingBottomSheetWidget(context),
                      icon: Icon(
                        Icons.sort,
                        color: kPrimaryLightColor,
                      ),
                    ),
                    IconButton(
                      onPressed: () => Navigator.pushNamed(
                        context,
                        RouteGenerator.searchPage,
                      ),
                      icon: Icon(
                        Icons.search,
                        color: kPrimaryLightColor,
                      ),
                    ),
                  ],
                ),
              ),
              _buildBooks(),
              AddNewBookCard(),
              const SizedBox(height: 80),
            ],
          ),
        ),
      );
    });
  }

  AppBar _buildCashbookAppBar() {
    return AppBar(
      elevation: 1,
      titleSpacing: 8,
      title: businessController.isLoading.value
          ? const CircularProgressIndicator()
          : businessController.businessList.isEmpty
              ? Text('No Business Found!'.tr)
              : _buildCashbookAppBarTitle(),
      actions: [
        IconButton(
          onPressed: () => Navigator.pushNamed(
            context,
            RouteGenerator.businessTeam,
          ),
          icon: Icon(
            Icons.person_add_outlined,
            color: kPrimaryLightColor,
          ),
        ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget _buildCashbookAppBarTitle() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => businessSelectorSheetWidget(context),
      child: Row(
        children: [
          const BusinessLeadingIcon(),
          const SizedBox(width: 5),
          SizedBox(
            width: 120,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  businessController.selectedBusiness.value.name ?? '',
                  style: MyTextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Text(
                  'Tap to switch business',
                  style: MyTextStyles.h6,
                ),
              ],
            ),
          ),
          Icon(
            Icons.keyboard_arrow_down,
            color: kGrey,
          ),
        ],
      ),
    );
  }

  Widget _buildBooks() {
    return cashbookController.isLoading.value
        ? const KCustomLoader()
        : cashbookController.cashbookList.isEmpty
            ? const NoDataFound()
            : ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: cashbookController.cashbookList.length,
                itemBuilder: (context, index) => CashbookItemBuilder(
                  onTap: () => Get.to(
                    () => BookDetailsPage(
                      cashbookModel: cashbookController.cashbookList[index],
                    ),
                  ),
                  cashbookModel: cashbookController.cashbookList[index],
                ),
              );
  }

  businessSelectorSheetWidget(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: SizedBox(
              height: context.screenHeight * 0.7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Select Business'.tr,
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  KDivider(
                    color: kGrey,
                    height: 10,
                  ),
                  Expanded(
                    child: _buildBusinessList(),
                  ),
                  KDivider(
                    color: kGrey,
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: KButton(
                      onPressed: () {
                        Navigator.pushNamed(
                          context,
                          RouteGenerator.addBusinessName,
                        );
                      },
                      bgColor: kPrimaryLightColor,
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.add,
                            color: kWhite,
                          ),
                          const SizedBox(width: 15),
                          Text(
                            'ADD NEW BUSINESS'.tr,
                            style: MyTextStyles.h3.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kWhite,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }

  Widget _buildBusinessList() {
    return businessController.isLoading.value
        ? const KCustomLoader()
        : businessController.businessList.isEmpty
            ? const NoDataFound()
            : ListView.separated(
                padding: const EdgeInsets.all(15),
                itemCount: businessController.businessList.length,
                itemBuilder: (context, index) => BusinessItemBuilder(
                  onTap: () {
                    /// set selected business
                    businessController.selectedBusiness.value =
                        businessController.businessList[index];

                    /// refresh cashbook list
                    cashbookController.getCashbookList(
                      id: businessController.selectedBusiness.value.id
                          .toString(),
                    );

                    /// save business id
                    LocalStorage.saveData(
                      key: LocalStorageKey.selectedBusinessId,
                      data: businessController.selectedBusiness.value.id,
                    );

                    /// pop the bottom sheet
                    Navigator.pop(context);
                  },
                  isSelected: businessController.selectedBusiness.value ==
                      businessController.businessList[index],
                  businessModel: businessController.businessList[index],
                ),
                separatorBuilder: (context, index) => const KDivider(
                  height: 5,
                ),
              );
  }

  booksSortingBottomSheetWidget(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              height: context.screenHeight * 0.6,
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Sort Books By',
                    textAlign: TextAlign.center,
                    style: MyTextStyles.h3.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  KDivider(
                    color: kGrey,
                  ),
                  SizedBox(
                    height: context.screenHeight * 0.4,
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: sortList.length,
                      itemBuilder: (context, index) =>
                          RadioListTileWidgetBuilder(
                        onChanged: (value) {
                          setState(() {
                            selectedSort = sortList[index];
                          });
                        },
                        value: sortList[index],
                        groupValue: selectedSort,
                        isSelected: selectedSort == sortList[index],
                        title: sortList[index],
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 8,
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                  KButton(
                    onPressed: () {
                      /// close the bottom sheet
                      Navigator.pop(context);
                    },
                    bgColor: kPrimaryLightColor,
                    child: Text(
                      'APPLY',
                      style: MyTextStyles.h3.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kWhite,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }
}
