import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/book_entries_builder.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/data_safety_card_widget.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/net_balance_card_widget.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/showing_entries_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BookDetailsPage extends StatefulWidget {
  final CashbookModel cashbookModel;

  const BookDetailsPage({
    Key? key,
    required this.cashbookModel,
  }) : super(key: key);

  @override
  State<BookDetailsPage> createState() => _BookDetailsPageState();
}

class _BookDetailsPageState extends State<BookDetailsPage>
    with SingleTickerProviderStateMixin {

  final cashbookController = Get.find<CashbookController>();

  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      cashbookController.getBookTransaction(id: '${widget.cashbookModel.id}');
    });

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    _animation = Tween<double>(begin: 0.0, end: 20.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.fastOutSlowIn,
      ),
    );

    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Get.find<ThemeController>().isDarkMode ? kPrimaryDarkColor : kGreyLight,
      appBar: _buildBookDetailsAppBar(),
      body: Obx(() {
        return cashbookController.isLoading.value
            ? const KCustomLoader()
            : Stack(
                children: [
                  _buildBookDetailsBody(),
                  Positioned(
                    bottom: 0,
                    child: cashbookController
                            .bookTransaction.value.transactions!.isEmpty
                        ? _buildButtonsForFirstTime()
                        : _buildCashInAndCashOutButtons(),
                  ),
                ],
              );
      }),
    );
  }

  AppBar _buildBookDetailsAppBar() {
    return AppBar(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.cashbookModel.name ?? '',
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Add Member, Book Activity etc'.tr,
            style: MyTextStyles.h6.copyWith(
              color: kGreyTextColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.person_add_alt,
            color: kPrimaryLightColor,
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.picture_as_pdf_outlined,
            color: kPrimaryLightColor,
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.more_vert_outlined,
            color: kPrimaryLightColor,
          ),
        ),
      ],
    );
  }

  Widget _buildBookDetailsBody() {
    List<Transactions> transactionList =
        cashbookController.bookTransaction.value.transactions ?? [];
    return SizedBox(
      height: context.screenHeight,
      child: SingleChildScrollView(
        child: Column(
          children: [
            NetBalanceCardWidget(
              bookTransactionModel: cashbookController.bookTransaction.value,
            ),
            ShowingEntriesTextWidget(
              entries: transactionList.length,
            ),
            ListView.separated(
              padding: const EdgeInsets.symmetric(
                vertical: 15,
              ),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: transactionList.length,
              itemBuilder: (context, index) => BookEntriesBuilder(
                data: transactionList[index],
                cashbookModel: widget.cashbookModel,
              ),
              separatorBuilder: (context, index) => const SizedBox(height: 8),
            ),
            const DataSafetyCardWidget(),
            const SizedBox(height: 70),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonsForFirstTime() {
    return Container(
      height: 200,
      width: context.screenWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Add your first entry'.tr,
            style: MyTextStyles.h2.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Center(
            child: SizedBox(
              height: 60.0, // Set the height of the parent widget
              child: GestureDetector(
                onTap: () {
                  _animationController.reset();
                  _animationController.forward();
                },
                child: AnimatedBuilder(
                  animation: _animationController,
                  builder: (context, child) {
                    return Transform.translate(
                      offset: Offset(0.0, -_animation.value),
                      child: child,
                    );
                  },
                  child: Icon(
                    Icons.arrow_downward_outlined,
                    size: 30,
                    color: kPrimaryLightColor,
                  ),
                ),
              ),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '${'Record'.tr} ',
                      style: MyTextStyles.h4,
                    ),
                    TextSpan(
                      text: ' ${'Income'.tr}',
                      style: MyTextStyles.h4.copyWith(
                        color: kGreen,
                      ),
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '${'Record'.tr} ',
                      style: MyTextStyles.h4,
                    ),
                    TextSpan(
                      text: ' ${'Expense'.tr}',
                      style: MyTextStyles.h4.copyWith(
                        color: kRed,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          _buildCashInAndCashOutButtons(),
        ],
      ),
    );
  }

  Widget _buildCashInAndCashOutButtons() {
    return Container(
      height: 60,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          KButton(
            onPressed: () => Navigator.pushNamed(
              context,
              RouteGenerator.addEntry,
              arguments: [
                TransactionType.cashIn,
                widget.cashbookModel,
              ],
            ),
            width: context.screenWidth * 0.44,
            bgColor: kGreen,
            child: Row(
              children: [
                Icon(
                  Icons.add,
                  color: kWhite,
                ),
                const SizedBox(width: 8),
                Text(
                  'CASH IN',
                  style: MyTextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: kWhite,
                  ),
                ),
              ],
            ),
          ),
          KButton(
            onPressed: () => Navigator.pushNamed(
              context,
              RouteGenerator.addEntry,
              arguments: [
                TransactionType.cashOut,
                widget.cashbookModel,
              ],
            ),
            width: context.screenWidth * 0.44,
            bgColor: kRed,
            child: Row(
              children: [
                Icon(
                  CupertinoIcons.minus,
                  color: kWhite,
                ),
                const SizedBox(width: 8),
                Text(
                  'CASH OUT',
                  style: MyTextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: kWhite,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
