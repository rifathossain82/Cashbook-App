import 'dart:io';

import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/image_services.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/circular_button_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/core/widgets/selectable_container.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_amount_text_form_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_attach_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_category_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_contact_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_date_or_time_select_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_icon_text_button.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_payment_method_text_field.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_entry_remark_text_field.dart';
import 'package:cashbook_app/src/features/category/model/transaction_category_model.dart';
import 'package:cashbook_app/src/features/category/view/pages/choose_category_page.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/contact/model/contact_model.dart';
import 'package:cashbook_app/src/features/contact/model/contact_type_model.dart';
import 'package:cashbook_app/src/features/payment_modes/model/payment_method_model.dart';
import 'package:cashbook_app/src/features/payment_modes/view/pages/choose_payment_modes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class EditEntryPage extends StatefulWidget {
  final Transactions transactions;
  final CashbookModel cashbookModel;

  const EditEntryPage({
    Key? key,
    required this.transactions,
    required this.cashbookModel,
  }) : super(key: key);

  @override
  State<EditEntryPage> createState() => _EditEntryPageState();
}

class _EditEntryPageState extends State<EditEntryPage> {
  final cashbookController = Get.find<CashbookController>();
  final contactController = Get.find<ContactController>();
  final themeController = Get.find<ThemeController>();

  TransactionType? transactionType;
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  ContactModel? selectedContact;
  ContactTypeModel? selectedContactType;
  TransactionCategoryModel? selectedCategory;
  PaymentMethodModel? selectedPaymentMethod;

  final formKey = GlobalKey<FormState>();
  final amountTextController = TextEditingController();
  final remarkTextController = TextEditingController();
  final contactTextController = TextEditingController();
  final categoryTextController = TextEditingController();
  final paymentMethodTextController = TextEditingController();
  File? selectedAttachImage;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? time = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (time != null && time != selectedTime) {
      setState(() {
        selectedTime = time;
      });
    }
  }

  @override
  void initState() {
    /// load contact type list
    contactController.getContactTypeList();

    /// set transaction data
    setInitialData();
    super.initState();
  }

  void setInitialData() {
    transactionType = widget.transactions.type == TransactionType.cashIn.name
        ? TransactionType.cashIn
        : TransactionType.cashOut;
    selectedDate = DateTime.parse(widget.transactions.date!);
    DateTime dateTime = DateFormat("HH:mm").parse(widget.transactions.time!);
    selectedTime = TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
    amountTextController.text = '${widget.transactions.amount}';

    /// set contact
    contactTextController.text = widget.transactions.contactName ?? "";
    selectedContact = ContactModel(
      id: widget.transactions.contactTypeId,
      name: widget.transactions.contactName,
    );

    /// set remark
    remarkTextController.text = widget.transactions.description ?? "";

    /// set transaction category
    categoryTextController.text =
        widget.transactions.transactionCategoryName ?? "";
    selectedCategory = TransactionCategoryModel(
      id: widget.transactions.transactionCategoryId,
      name: widget.transactions.transactionCategoryName,
    );

    /// set payment method
    paymentMethodTextController.text = widget.transactions.methodName ?? '';
    selectedPaymentMethod = PaymentMethodModel(
      id: widget.transactions.paymentMethodId,
      name: widget.transactions.methodName,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Entry'),
      ),
      body: Stack(
        children: [
          _buildEditEntryBody(),
          Positioned(
            bottom: 0,
            child: _buildUpdateButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildEditEntryBody() {
    return Container(
      padding: const EdgeInsets.all(15),
      height: context.screenHeight,
      child: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// transaction Type
              Wrap(
                children: [
                  SelectableContainer(
                    onTap: () {
                      setState(() {
                        transactionType = TransactionType.cashIn;
                      });
                    },
                    isSelected: transactionType == TransactionType.cashIn,
                    borderRadius: 30,
                    selectedBgColor: kGreen,
                    margin: const EdgeInsets.only(
                      right: 8,
                      bottom: 10,
                    ),
                    child: Text(
                      'Cash In',
                      textAlign: TextAlign.center,
                      style: MyTextStyles.h4.copyWith(
                        color: transactionType == TransactionType.cashIn
                            ? kWhite
                            : kBlackLight,
                      ),
                    ),
                  ),
                  SelectableContainer(
                    onTap: () {
                      setState(() {
                        transactionType = TransactionType.cashOut;
                      });
                    },
                    isSelected: transactionType == TransactionType.cashOut,
                    borderRadius: 30,
                    selectedBgColor: kRed,
                    margin: const EdgeInsets.only(
                      right: 8,
                      bottom: 10,
                    ),
                    child: Text(
                      'Cash Out',
                      textAlign: TextAlign.center,
                      style: MyTextStyles.h4.copyWith(
                        color: transactionType == TransactionType.cashOut
                            ? kWhite
                            : kBlackLight,
                      ),
                    ),
                  ),
                ],
              ),

              /// date and time
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AddEntryDateOrTimeSelectButton(
                    onTap: () => _selectDate(context),
                    leadingIcon: Icons.calendar_month,
                    title: DateFormat('dd-MM-yyyy').format(selectedDate),
                  ),
                  AddEntryDateOrTimeSelectButton(
                    onTap: () => _selectTime(context),
                    leadingIcon: Icons.access_time_outlined,
                    title: selectedTime.format(context),
                  ),
                ],
              ),
              const SizedBox(height: 15),

              AddEntryAmountTextFormField(
                controller: amountTextController,
              ),
              Obx(() {
                return contactController.isLoading.value
                    ? const Center(child: CircularProgressIndicator())
                    : contactController.contactTypeList.isEmpty
                        ? const Text('There is no contact type!')
                        : DropdownButtonFormField(
                            items: contactController.contactTypeList.map(
                              (e) {
                                if (e.id == widget.transactions.contactTypeId) {
                                  selectedContactType = e;
                                }
                                return DropdownMenuItem(
                                  value: e,
                                  child: Text(
                                    e.name!.capitalizedFirst,
                                  ),
                                );
                              },
                            ).toList(),
                            isDense: true,
                            hint: const Text('Select Contact Type'),
                            value: selectedContactType,
                            onChanged: (value) {
                              setState(() {
                                selectedContactType = value;
                              });
                            },
                            validator: (value) {
                              if (selectedContactType == null) {
                                return 'Contact Type is required';
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              labelText: 'Contact Type*',
                              isDense: true,
                              border: OutlineInputBorder(),
                            ),
                          );
              }),
              const SizedBox(height: 15),
              AddEntryContactTextField(
                onTap: () async {
                  var result = await Navigator.pushNamed(
                    context,
                    RouteGenerator.chooseContact,
                    arguments: widget.cashbookModel.id,
                  );
                  if (result != null) {
                    selectedContact = result as ContactModel;
                    contactTextController.text = selectedContact!.name!;
                  }
                },
                controller: contactTextController,
              ),
              AddEntryRemarkTextField(
                controller: remarkTextController,
              ),
              selectedAttachImage != null
                  ? IntrinsicHeight(
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: FileImage(selectedAttachImage!),
                              ),
                            ),
                          ),
                          const SizedBox(width: 30),
                          AddEntryIconTextButton(
                            onTap: () => openImageSourceSelectorDialog(context),
                            leadingIcon: Icons.edit,
                            title: 'CHANGE',
                          ),
                          const VerticalDivider(
                            thickness: 1,
                            indent: 15,
                            endIndent: 15,
                            width: 30,
                          ),
                          AddEntryIconTextButton(
                            onTap: () {
                              setState(() {
                                selectedAttachImage = null;
                              });
                            },
                            leadingIcon: Icons.delete_outline,
                            title: 'DELETE',
                          ),
                        ],
                      ),
                    )
                  : AddEntryAttachButton(
                      onTap: () => openImageSourceSelectorDialog(context),
                    ),
              const SizedBox(height: 15),
              AddEntryCategoryTextField(
                onTap: () async {
                  var result = await Get.to(
                    () => ChooseCategoryPage(
                      categoryModel: selectedCategory,
                    ),
                  );
                  if (result != null) {
                    setState(() {
                      selectedCategory = result as TransactionCategoryModel;
                      categoryTextController.text =
                          selectedCategory!.name ?? '';
                    });
                  }
                },
                controller: categoryTextController,
              ),
              AddEntryPaymentMethodTextField(
                onTap: () async {
                  var result = await Get.to(
                    () => ChoosePaymentModesPage(
                      paymentMethod: selectedPaymentMethod,
                    ),
                  );
                  if (result != null) {
                    setState(() {
                      selectedPaymentMethod = result as PaymentMethodModel;
                      paymentMethodTextController.text =
                          selectedPaymentMethod!.name ?? '';
                    });
                  }
                },
                controller: paymentMethodTextController,
              ),
              // /// payment mode
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 15),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Payment Method',
              //         style: MyTextStyles.h4.copyWith(
              //           fontWeight: FontWeight.w600,
              //           color: kGreyTextColor,
              //         ),
              //       ),
              //       const SizedBox(height: 5),
              //       Obx(() {
              //         return paymentMethodController.isLoading.value
              //             ? const Center(
              //                 child: CircularProgressIndicator(),
              //               )
              //             : paymentMethodController.paymentMethodList.isEmpty
              //                 ? const Center(
              //                     child: Text('No Payment Methods Found!'),
              //                   )
              //                 : Wrap(
              //                     alignment: WrapAlignment.start,
              //                     children: [
              //                       Wrap(
              //                         children: paymentMethodController
              //                             .paymentMethodList
              //                             .map(
              //                               (method) => SelectableContainer(
              //                                 onTap: () {
              //                                   setState(() {
              //                                     selectedPaymentMethod = method;
              //                                   });
              //                                 },
              //                                 isSelected:
              //                                     selectedPaymentMethod == method,
              //                                 borderRadius: 30,
              //                                 margin: const EdgeInsets.only(
              //                                   right: 8,
              //                                   bottom: 10,
              //                                 ),
              //                                 child: Text(
              //                                   method.name ?? "",
              //                                   textAlign: TextAlign.center,
              //                                   style: MyTextStyles.h4.copyWith(
              //                                     color: selectedPaymentMethod == method
              //                                         ? kWhite
              //                                         : kBlackLight,
              //                                   ),
              //                                 ),
              //                               ),
              //                             )
              //                             .toList(),
              //                       ),
              //                       GestureDetector(
              //                         onTap: () async {
              //                           var result = await Navigator.pushNamed(
              //                             context,
              //                             RouteGenerator.choosePaymentModes,
              //                           );
              //                           if (result != null) {
              //                             setState(() {
              //                               // if (paymentMethodController.paymentMethodList.length == 3) {
              //                               //   paymentMethodController.paymentMethodList.removeLast();
              //                               // }
              //                               // paymentMethodController.paymentMethodList.insert(
              //                               //     2, result);
              //                               // selectedPaymentMethod =
              //                               //     result.toString();
              //                             });
              //                           }
              //                         },
              //                         behavior: HitTestBehavior.opaque,
              //                         child: Padding(
              //                           padding: const EdgeInsets.symmetric(
              //                             horizontal: 15,
              //                             vertical: 8,
              //                           ),
              //                           child: Row(
              //                             crossAxisAlignment:
              //                                 CrossAxisAlignment.center,
              //                             mainAxisSize: MainAxisSize.min,
              //                             children: [
              //                               Text(
              //                                 'Show More',
              //                                 style: MyTextStyles.h4.copyWith(
              //                                   color: kPrimaryLightColor,
              //                                   fontWeight: FontWeight.bold,
              //                                 ),
              //                               ),
              //                               const SizedBox(width: 8),
              //                               Icon(
              //                                 Icons.arrow_drop_down,
              //                                 color: kPrimaryLightColor,
              //                               ),
              //                             ],
              //                           ),
              //                         ),
              //                       ),
              //                     ],
              //                   );
              //       }),
              //     ],
              //   ),
              // ),

              const SizedBox(height: 70),
            ],
          ),
        ),
      ),
    );
  }

  void openImageSourceSelectorDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Select Photo Using",
      dialogPosition: Alignment.bottomCenter,
      actions: [
        /// image from camera
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.cameraImage();
            var imageFile = await ImageServices.getImageFile(img);
            setState(() {
              selectedAttachImage = imageFile;
              Navigator.pop(context);
            });
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          bgColor: kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Camera',
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),

        /// image from gallery
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.galleryImage();
            var imageFile = await ImageServices.getImageFile(img);
            setState(() {
              selectedAttachImage = imageFile;
              Navigator.pop(context);
            });
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          bgColor: kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.image,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'GALLERY',
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildUpdateButton() {
    return Container(
      height: 60,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
      ),
      child: Obx(() {
        return KButton(
          onPressed: updateMethod,
          child: cashbookController.isLoading.value
              ? const CircularButtonLoader()
              : Text(
                  'UPDATE',
                  style: MyTextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: kWhite,
                  ),
                ),
        );
      }),
    );
  }

  void updateMethod() {
    if (formKey.currentState!.validate()) {
      cashbookController.updateBookTransaction(
        body: {
          'id': '${widget.transactions.id}',
          'date': DateFormat('yyyy-MM-dd').format(selectedDate),
          'time': selectedTime.format(context),
          'type': transactionType!.name,
          'book_contact_id': '${selectedContact!.id}',
          'book_contact_name': '${selectedContact!.name}',
          'contact_type_id': '${selectedContactType?.id}',
          'contact_type_name': selectedContactType!.name?.capitalizedFirst,
          'amount': amountTextController.text,
          'book_id': '${widget.cashbookModel.id}',
          'book_name': '${widget.cashbookModel.name}',
          'transaction_category_id': '${selectedCategory!.id}',
          'transaction_category_name': '${selectedCategory!.name}',
          'payment_method_id': '${selectedPaymentMethod!.id}',
          'payment_method_name': '${selectedPaymentMethod!.name}',
          'description': remarkTextController.text,
        },
      );
    }
  }
}
