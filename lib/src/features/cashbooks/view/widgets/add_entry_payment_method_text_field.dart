import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddEntryPaymentMethodTextField extends StatelessWidget {
  final VoidCallback onTap;
  final TextEditingController controller;

  const AddEntryPaymentMethodTextField({
    Key? key,
    required this.onTap,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: TextFormField(
        onTap: onTap,
        controller: controller,
        validator: (value){
          if(value.toString().isEmpty){
            return Message.emptyPaymentMethod.tr;
          }
          return null;
        },
        readOnly: true,
        decoration: InputDecoration(
          labelText: '${'Payment Method'.tr} *',
          hintText: 'Select Method'.tr,
          border: const OutlineInputBorder(),
          isDense: true,
          suffixIcon: Icon(
            Icons.arrow_drop_down,
            color: kGrey,
            size: 25,
          ),
        ),
      ),
    );
  }
}
