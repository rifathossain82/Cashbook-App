import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/tag_builder.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future addNewBookBottomSheet({
  required BuildContext context,
  String? bookName,
}) {
  final bookNameTextController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  final businessController = Get.find<BusinessController>();
  final cashbookController = Get.find<CashbookController>();

  if (bookName != null) {
    bookNameTextController.text = bookName;
  }

  final suggestions = [
    'Project Book',
    'Client Record',
  ];

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.6, //size of bottom sheet
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Add New Book',
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  const Divider(
                    height: 0,
                  ),

                  /// book name text field and suggestion list
                  Container(
                    height: context.screenHeight * 0.4,
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextFormField(
                            controller: bookNameTextController,
                            validator: (value) {
                              if (value.toString().isEmpty) {
                                return Message.emptyName;
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              isDense: true,
                              labelText: 'Enter Book Name',
                              border: OutlineInputBorder(),
                            ),
                          ),
                          bookName != null
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(
                                    top: 20,
                                    bottom: 5,
                                  ),
                                  child: Text(
                                    'Suggestions',
                                    style: MyTextStyles.h4.copyWith(
                                      color: kGreyTextColor,
                                    ),
                                  ),
                                ),
                          bookName != null
                              ? Container()
                              : Wrap(
                                  children: suggestions
                                      .map((tag) => TagBuilder(
                                            onTap: () {
                                              bookNameTextController.text = tag;
                                            },
                                            text: tag,
                                            color: kPrimaryLightColor,
                                          ))
                                      .toList(),
                                ),
                        ],
                      ),
                    ),
                  ),

                  /// add new book button
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: KButton(
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {
                          /// add new book
                          await cashbookController.addCashbook(
                            name: bookNameTextController.text,
                            businessID:
                                '${businessController.selectedBusiness.value.id}',
                          );

                          /// close the bottom sheet and refresh cashbook list
                          Navigator.pop(context);
                          cashbookController.getCashbookList(
                            id: '${businessController.selectedBusiness.value.id}',
                          );
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.add,
                            color: kWhite,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            'ADD NEW BOOK',
                            style: MyTextStyles.h4.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kWhite,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
