import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddEntryAmountTextFormField extends StatelessWidget {
  final TextEditingController controller;

  const AddEntryAmountTextFormField({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: TextFormField(
        controller: controller,
        keyboardType: TextInputType.number,
        validator: (value){
          if(value!.isEmpty){
            return Message.emptyAmount.tr;
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: '${'Amount'.tr} *',
          border: const OutlineInputBorder(),
          isDense: true,
        ),
      ),
    );
  }
}
