import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddEntryRemarkTextField extends StatelessWidget {
  final TextEditingController controller;

  const AddEntryRemarkTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: 'Remark'.tr,
          border: const OutlineInputBorder(),
          isDense: true,
          suffixIcon: Icon(
            Icons.mic,
            color: kPrimaryLightColor,
            size: 25,
          ),
        ),
      ),
    );
  }
}
