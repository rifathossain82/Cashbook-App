import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddEntryAttachButton extends StatelessWidget {
  final VoidCallback onTap;

  const AddEntryAttachButton({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KOutlinedButton(
      onPressed: onTap,
      width: context.screenWidth * 0.4,
      height: 50,
      bgColor: Get.find<ThemeController>().isDarkMode ? kPrimaryDarkColor : kWhite,
      borderColor: Get.find<ThemeController>().isDarkMode ? kWhite : kPrimaryLightColor,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.camera_alt,
            color: kPrimaryLightColor,
          ),
          const SizedBox(width: 15),
          Text(
            'Attach Image'.tr,
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.bold,
              color: kPrimaryLightColor,
            ),
          ),
        ],
      ),
    );
  }
}
