import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/tag_builder.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/add_new_book_bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddNewBookCard extends StatelessWidget {
  AddNewBookCard({Key? key}) : super(key: key);

  final bookTypes = [
    'Expenses',
    'New Project',
    'Client Record',
    'Project Book',
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Container(
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: kGrey,
            width: 1,
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              title: Text(
                'Add New Book'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                'Click to quickly add books for'.tr,
                style: MyTextStyles.h5.copyWith(
                  color: kGreyTextColor,
                ),
              ),
              trailing: Container(
                height: 50,
                width: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Get.find<ThemeController>().isDarkMode
                      ? kSecondaryDarkColor
                      : kGreyLight.withOpacity(0.4),
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  AssetPath.booksIcon,
                  height: 30,
                  width: 30,
                ),
              ),
            ),
            Wrap(
              children: bookTypes
                  .map((tag) => TagBuilder(
                        onTap: () {
                          addNewBookBottomSheet(
                              context: context, bookName: tag);
                        },
                        text: tag,
                        color: kPrimaryLightColor,
                      ))
                  .toList(),
            ),
            const SizedBox(height: 20),
          ],
        ),
      );
    });
  }
}
