import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/tag_builder.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future renameCashbookBottomSheet({
  required BuildContext context,
  required String bookId,
  required String bookName,
}) {
  final bookNameTextController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  final businessController = Get.find<BusinessController>();
  final cashbookController = Get.find<CashbookController>();
  bookNameTextController.text = bookName;

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.6, //size of bottom sheet
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Rename Cashbook'.tr,
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  const Divider(
                    height: 0,
                  ),

                  /// book name text field
                  Container(
                    height: context.screenHeight * 0.4,
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextFormField(
                            controller: bookNameTextController,
                            validator: (value) {
                              if (value.toString().isEmpty) {
                                return Message.emptyName.tr;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: 'Book Name'.tr,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  /// save button
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: KButton(
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {

                          BuildContext? cc = formKey.currentContext;

                          /// update cashbook
                          await cashbookController.updateCashbook(
                            id: bookId,
                            name: bookNameTextController.text,
                            businessID:
                                '${businessController.selectedBusiness.value.id}',
                          );

                          /// close the bottom sheet and refresh cashbook list
                          Navigator.pop(cc!);
                          cashbookController.getCashbookList(
                            id: '${businessController.selectedBusiness.value.id}',
                          );
                        }
                      },
                      child: Text(
                        'SAVE'.tr,
                        style: MyTextStyles.h4.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
