import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShowingEntriesTextWidget extends StatelessWidget {
  final int entries;

  const ShowingEntriesTextWidget({
    Key? key,
    required this.entries,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: KDivider(
            height: 0,
            color: kGrey,
          ),
        ),
        Text(
          '$entries ${'Entries Showing'.tr}',
          style: MyTextStyles.h5.copyWith(
            fontWeight: FontWeight.bold,
            color: kGrey,
          ),
        ),
        Expanded(
          child: KDivider(
            height: 0,
            color: kGrey,
          ),
        ),
      ],
    );
  }
}
