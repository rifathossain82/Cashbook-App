import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future deleteCashbookBottomSheet({
  required BuildContext context,
  required CashbookModel cashbookModel,
}) {
  final bookNameTextController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  final businessController = Get.find<BusinessController>();
  final cashbookController = Get.find<CashbookController>();

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.6, //size of bottom sheet
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Delete ${cashbookModel.name}?',
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  const Divider(
                    height: 0,
                  ),

                  /// book name text field and suggestion list
                  Container(
                    height: context.screenHeight * 0.4,
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Please type '${cashbookModel.name}' to confirm",
                            style: MyTextStyles.h3,
                          ),
                          const SizedBox(height: 20),
                          TextFormField(
                            controller: bookNameTextController,
                            validator: (value) {
                              if (value.toString().isEmpty) {
                                return Message.emptyName;
                              } else if (value.toString() !=
                                  cashbookModel.name) {
                                return "Cashbook name doesn't match!";
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              isDense: true,
                              labelText: 'Book Name',
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  /// save button
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: KButton(
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {

                          /// store business id
                          String businessID =
                              '${businessController.selectedBusiness.value.id}';

                          /// fetch transaction list based on cashbook id, then store the transaction list
                          await cashbookController.getBookTransaction(id: '${cashbookModel.id}');
                          var transactions = cashbookController.bookTransaction.value.transactions ?? [];

                          /// if transaction list is empty then delete book in normal way otherwise force to delete book with transaction
                          if(transactions.isEmpty){
                            /// delete book
                            cashbookController.deleteCashbook(
                              id: '${cashbookModel.id}',
                              businessID: businessID,
                            ).then((value) {
                              /// close the bottom sheet and refresh cashbook list
                              Navigator.pop(context);
                              cashbookController.getCashbookList(
                                id: businessID,
                              );
                            });
                          } else{

                            /// show alert dialog with total transaction length
                            bool? result = await customDeleteDialog(
                              context: context,
                              dialogPosition: Alignment.center,
                              title: 'Delete Book?'.tr,
                              contentText: "Are you sure you want to delete this book with ${transactions.length} transactions? This can't be undone.".tr,
                              negativeActionText: 'NO'.tr,
                              positiveActionText: 'YES, DELETE'.tr,
                            );

                            if (result ?? false) {
                              /// force delete book
                              cashbookController.deleteCashbookWithTransactions(
                                id: '${cashbookModel.id}',
                                businessID: businessID,
                              ).then((value) {
                                /// close the bottom sheet and refresh cashbook list
                                Navigator.of(context).pop();
                                cashbookController.getCashbookList(
                                  id: businessID,
                                );
                              });
                            }
                          }

                        }
                      },
                      child: Text(
                        'DELETE',
                        style: MyTextStyles.h4.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
