import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/business_leading_icon.dart';
import 'package:cashbook_app/src/features/add_business/model/business_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final bool isSelected;
  final BusinessModel businessModel;

  const BusinessItemBuilder({
    Key? key,
    required this.onTap,
    required this.isSelected,
    required this.businessModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      contentPadding: EdgeInsets.zero,
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      leading: BusinessLeadingIcon(
        iconColor: kPrimaryLightColor,
      ),
      title: Text(
        businessModel.name ?? "",
        style: MyTextStyles.h4.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        '${'Your Role:'.tr} Owner . ${businessModel.books!.length} Books',
        style: MyTextStyles.h5.copyWith(
          color: kGreyTextColor,
        ),
      ),
      trailing: isSelected
          ? Icon(
              Icons.check_circle_rounded,
              color: kGreen,
            )
          : null,
    );
  }
}
