import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddEntryContactTextField extends StatelessWidget {
  final VoidCallback onTap;
  final TextEditingController controller;

  const AddEntryContactTextField({
    Key? key,
    required this.onTap,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: TextFormField(
        onTap: onTap,
        autofocus: false,
        controller: controller,
        validator: (value){
          if(value.toString().isEmpty){
            return Message.emptyContactName.tr;
          }
          return null;
        },
        readOnly: true,
        decoration: InputDecoration(
          labelText: '${'Contact Name'.tr} *',
          hintText: 'Select Contact'.tr,
          border: const OutlineInputBorder(),
          isDense: true,
          suffixIcon: Icon(
            controller.text.isNotEmpty
                ? Icons.close
                : Icons.arrow_drop_down,
            color: controller.text.isNotEmpty
                ? kGrey
                : kPrimaryLightColor,
            size: 25,
          ),
        ),
      ),
    );
  }
}
