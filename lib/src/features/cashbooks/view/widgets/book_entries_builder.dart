import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/status_builder.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/pages/entry_details_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BookEntriesBuilder extends StatelessWidget {
  final Transactions data;
  final CashbookModel cashbookModel;

  const BookEntriesBuilder({
    Key? key,
    required this.data,
    required this.cashbookModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(
        () => EntryDetailsPage(
          transactions: data,
          cashbookModel: cashbookModel,
        ),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0.0, 3.0),
              spreadRadius: 0,
              blurRadius: 4,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    data.contactName != null
                        ? RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: data.contactName,
                                  style: MyTextStyles.h4.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                TextSpan(
                                  text: ' (${data.contactTypeName ?? ''})',
                                  style: MyTextStyles.h5.copyWith(
                                    color: kGreyTextColor,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        StatusBuilder(
                          status: data.transactionCategoryName!,
                          statusColor: kPrimaryLightColor,
                        ),
                        const SizedBox(width: 8),
                        StatusBuilder(
                          status: '${data.methodName}',
                          statusColor: kBlue,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(width: 8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "${data.amount}",
                      style: MyTextStyles.h3.copyWith(
                        fontWeight: FontWeight.bold,
                        color: data.type == 'cashOut' ? kRed : kGreen,
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      '${'Balance'.tr}: ${data.balance}',
                      style: MyTextStyles.h5.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kGreyTextColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 15),
            data.description != null
                ? Text(
                    '${data.description}',
                    maxLines: 2,
                    style: MyTextStyles.h5,
                  )
                : Container(),
            Row(
              children: [
                Icon(
                  Icons.link,
                  color: kGrey,
                ),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Image'.tr,
                    style: MyTextStyles.h4.copyWith(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ],
            ),
            const KDivider(height: 20),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Enter by You ',
                    style: MyTextStyles.h5.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kGreyTextColor,
                    ),
                  ),
                  TextSpan(
                    text: ' at ${data.time}',
                    style: MyTextStyles.h5.copyWith(
                      color: kGreyTextColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
