import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AddEntryIconTextButton extends StatelessWidget {
  final VoidCallback onTap;
  final IconData leadingIcon;
  final String title;

  const AddEntryIconTextButton({
    Key? key,
    required this.onTap,
    required this.leadingIcon,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            leadingIcon,
            color: kPrimaryLightColor,
          ),
          const SizedBox(width: 8),
          Text(
            title,
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.bold,
              color: kPrimaryLightColor,
            ),
          ),
        ],
      ),
    );
  }
}
