import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/cashbooks/model/book_transaction_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NetBalanceCardWidget extends StatelessWidget {
  final BookTransactionModel bookTransactionModel;
  const NetBalanceCardWidget({Key? key, required this.bookTransactionModel,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0.0, 3.0),
            spreadRadius: 0,
            blurRadius: 4,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Net Balance'.tr,
                  style: MyTextStyles.h3.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${bookTransactionModel.finalBalace}",
                  style: MyTextStyles.h3.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          const KDivider(height: 0),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Total In (+)'.tr,
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${bookTransactionModel.totalCashIn}",
                      style: MyTextStyles.h4.copyWith(
                        color: kGreen,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Total Out (-)'.tr,
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${bookTransactionModel.totalCashOut}",
                      style: MyTextStyles.h4.copyWith(
                        color: kRed,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const KDivider(height: 0),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
            ),
            child: TextButton(
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'VIEW REPORTS'.tr,
                    style: MyTextStyles.h4.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kPrimaryLightColor,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: kPrimaryLightColor,
                    size: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
