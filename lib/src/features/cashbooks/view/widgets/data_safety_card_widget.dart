import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DataSafetyCardWidget extends StatelessWidget {
  const DataSafetyCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
        borderRadius: BorderRadius.circular(4),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0.0, 3.0),
            spreadRadius: 0,
            blurRadius: 4,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.lock,
            color: kGreen,
            size: 20,
          ),
          const SizedBox(width: 8),
          Text(
            'Only you can see these entries'.tr,
            style: MyTextStyles.h5,
          ),
        ],
      ),
    );
  }
}
