import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/delete_cashbook_bottom_sheet_widget.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/rename_cashbook_bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CashbookItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final CashbookModel cashbookModel;

  const CashbookItemBuilder({
    Key? key,
    required this.onTap,
    required this.cashbookModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            contentPadding: const EdgeInsets.only(
              left: 15,
            ),
            leading: Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kGreyLight.withOpacity(0.4),
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.book,
                color: kPrimaryLightColor,
              ),
            ),
            title: Text(
              cashbookModel.name ?? "",
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              'Created ${cashbookModel.createdAt.toString().getDuration} ago',
              style: MyTextStyles.h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  '',
                  style: MyTextStyles.h4.copyWith(
                    color: kGreen,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(width: 8),
                PopupMenuButton<CashbookItemsMenuOptions>(
                  color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
                  onSelected: (value) {
                    if (value == CashbookItemsMenuOptions.details) {
                      onTap();
                    } else if (value == CashbookItemsMenuOptions.rename) {
                      renameCashbookBottomSheet(
                        context: context,
                        bookId: '${cashbookModel.id}',
                        bookName: cashbookModel.name ?? "",
                      );
                    } else {
                      deleteCashbookBottomSheet(
                        context: context,
                        cashbookModel: cashbookModel,
                      );
                    }
                  },
                  icon: Icon(
                    Icons.more_vert_outlined,
                    color: kGrey,
                    size: 30,
                  ),
                  itemBuilder: (BuildContext context) => [
                    _buildPopupMenuItem(
                      value: CashbookItemsMenuOptions.details,
                      icon: Icons.details,
                      iconColor: kGrey,
                      title: 'Details'.tr,
                    ),
                    _buildPopupMenuItem(
                      value: CashbookItemsMenuOptions.rename,
                      icon: Icons.edit,
                      iconColor: kGrey,
                      title: 'Rename'.tr,
                    ),
                    _buildPopupMenuItem(
                      value: CashbookItemsMenuOptions.delete,
                      icon: Icons.delete,
                      iconColor: kRed,
                      title: 'Delete Book'.tr,
                    ),
                  ],
                ),
              ],
            ),
          ),
          const KDivider(),
        ],
      ),
    );
  }

  PopupMenuItem<CashbookItemsMenuOptions> _buildPopupMenuItem({
    required CashbookItemsMenuOptions value,
    required IconData icon,
    required Color iconColor,
    required String title,
  }) {
    return PopupMenuItem<CashbookItemsMenuOptions>(
      value: value,
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        visualDensity: const VisualDensity(
          horizontal: VisualDensity.minimumDensity,
        ),
        leading: Icon(
          icon,
          color: iconColor,
        ),
        title: Text(
          title,
          style: MyTextStyles.h4,
        ),
      ),
    );
  }
}
