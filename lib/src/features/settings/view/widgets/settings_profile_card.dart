import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/image_services.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/profile_strength_slider_widget.dart';
import 'package:cashbook_app/src/features/user_profile/controller/user_profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsProfileCard extends StatelessWidget {
  final BusinessController businessController;

  SettingsProfileCard({
    Key? key,
    required this.businessController,
  }) : super(key: key);

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return businessController.isLoading.value
          ? const KCustomLoader()
          : businessController.businessList.isEmpty
              ? const NoDataFound()
              : Container(
                  padding: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: themeController.isDarkMode
                        ? kSecondaryDarkColor
                        : kWhite,
                  ),
                  child: Column(
                    children: [
                      /// business profile name, photo and edit icon button
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () => openImageSourceSelectorDialog(context),
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: kGrey,
                                  width: 1,
                                ),
                              ),
                              child: Icon(
                                Icons.add_a_photo_outlined,
                                color: kGrey,
                                size: 40,
                              ),
                            ),
                          ),
                          const SizedBox(width: 15),
                          Expanded(
                            flex: 8,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  businessController
                                          .selectedBusiness.value.name ??
                                      "",
                                  style: MyTextStyles.h2.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.warning_rounded,
                                      color: kRedDeep,
                                    ),
                                    const SizedBox(width: 8),
                                    Expanded(
                                      child: Text(
                                        "Incomplete business profile".tr,
                                        style: MyTextStyles.h4.copyWith(
                                          color: kRedDeep,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 5),
                          IconButton(
                            onPressed: () => Navigator.pushNamed(
                              context,
                              RouteGenerator.businessProfile,
                            ),
                            icon: Icon(
                              Icons.mode_edit_outlined,
                              color: kPrimaryLightColor,
                              size: 30,
                            ),
                          ),
                        ],
                      ),

                      /// profile strength slider
                      const ProfileStrengthSlider(
                        percentage: 20,
                      ),
                      const SizedBox(height: 20),

                      /// add missing details button
                      KButton(
                        onPressed: () => Navigator.pushNamed(
                          context,
                          RouteGenerator.businessProfile,
                        ),
                        height: 50,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'ADD MISSING DETAILS'.tr,
                              style: MyTextStyles.h4.copyWith(
                                color: kWhite,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(width: 8),
                            Icon(
                              Icons.arrow_forward_ios_outlined,
                              color: kWhite,
                              size: 18,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
    });
  }

  void openImageSourceSelectorDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Choose an image from".tr,
      dialogPosition: Alignment.bottomCenter,
      actions: [
        /// image from camera
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.cameraImage();
            var imageFile = await ImageServices.getImageFile(img);
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          bgColor: kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Camera'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),

        /// image from gallery
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.galleryImage();
            var imageFile = await ImageServices.getImageFile(img);
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          bgColor: kWhite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.image,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Gallery'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
