import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/our_promise_icon_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OurPromiseWidget extends StatelessWidget {
  const OurPromiseWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
                bottom: 10,
              ),
              child: Text(
                'Our Promise'.tr,
                textAlign: TextAlign.start,
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: Get.find<ThemeController>().isDarkMode
                      ? kWhite.withOpacity(0.7)
                      : kBlackLight.withOpacity(0.7),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OurPromiseIconTextBuilder(
                  iconData: Icons.security,
                  iconColor: kSecondaryLightColor,
                  title: '100% Safe & Secure'.tr,
                ),
                OurPromiseIconTextBuilder(
                  iconData: Icons.cloud_done,
                  iconColor: kPrimaryLightColor,
                  title: 'Auto Data Backup'.tr,
                ),
              ],
            ),
          ],
        ),
      );
    });
  }
}
