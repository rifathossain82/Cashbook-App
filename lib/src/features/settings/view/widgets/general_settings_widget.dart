import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/settings_list_tile_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GeneralSettingsWidget extends StatelessWidget {
  GeneralSettingsWidget({Key? key}) : super(key: key);

  final settingsList = [
    SettingsItemModel(
      routeName: RouteGenerator.appSettings,
      leadingIcon: Icons.app_settings_alt,
      titleText: 'App Settings',
      subtitleText: 'Language, Theme, Security, Backup',
    ),
    SettingsItemModel(
      routeName: RouteGenerator.userProfile,
      leadingIcon: Icons.account_circle_outlined,
      titleText: 'Your Profile',
      subtitleText: 'Name, Mobile Number, Email',
    ),
    SettingsItemModel(
      routeName: RouteGenerator.aboutCashbook,
      leadingIcon: Icons.info_outline_rounded,
      titleText: 'About CashBook',
      subtitleText: 'Privacy policy, T&C, About us',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 20,
          ),
          child: Text(
            'General Settings'.tr,
            textAlign: TextAlign.start,
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.bold,
              color: Get.find<ThemeController>().isDarkMode
                  ? kWhite.withOpacity(0.7)
                  : kBlackLight.withOpacity(0.7),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Get.find<ThemeController>().isDarkMode
                ? kSecondaryDarkColor
                : kWhite,
          ),
          child: ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: settingsList.length,
            itemBuilder: (context, index) => SettingsListTileBuilder(
              itemModel: settingsList[index],
            ),
            separatorBuilder: (context, index) => const Divider(height: 10),
          ),
        ),
      ],
    );
  }
}
