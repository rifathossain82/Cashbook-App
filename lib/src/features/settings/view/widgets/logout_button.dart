import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/circular_button_loader.dart';
import 'package:cashbook_app/src/features/auth/controller/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LogoutButton extends StatelessWidget {
  LogoutButton({Key? key}) : super(key: key);

  final authController = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return GestureDetector(
        onTap: () => logoutMethod(context),
        child: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(top: 20),
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Get.find<ThemeController>().isDarkMode
                ? kSecondaryDarkColor
                : kWhite,
          ),
          child: authController.isLoading.value
              ? CircularButtonLoader(
                  indicatorColor: kRed,
                )
              : Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.logout,
                      size: 25,
                      color: kRedDeep,
                    ),
                    const SizedBox(width: 20),
                    Text(
                      'Logout'.tr,
                      style: MyTextStyles.h3.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kRedDeep,
                      ),
                    ),
                  ],
                ),
        ),
      );
    });
  }

  void logoutMethod(BuildContext context) async {
    bool? result = await kConfirmDialog(
      context: context,
      title: 'Are you sure you want to logout?'.tr,
      negativeActionText: 'Cancel'.tr,
      positiveActionText: 'Logout'.tr,
    );

    if (result != null && result == true) {
      authController.logout();
    }
  }
}
