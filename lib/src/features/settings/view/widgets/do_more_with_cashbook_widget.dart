import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DoMoreWithCashbookWidget extends StatelessWidget {
  const DoMoreWithCashbookWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Do more with CashBook'.tr,
              textAlign: TextAlign.start,
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
                color: Get.find<ThemeController>().isDarkMode
                    ? kWhite.withOpacity(0.7)
                    : kBlackLight.withOpacity(0.7),
              ),
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Expanded(
                  child: _buildItem(
                    onTap: () {},
                    iconData: Icons.monitor_rounded,
                    title: 'CashBook for PC'.tr,
                  ),
                ),
                const SizedBox(width: 15),
                Expanded(
                  child: _buildItem(
                    onTap: () {},
                    iconData: Icons.share,
                    title: 'Share'.tr,
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget _buildItem({
    required VoidCallback onTap,
    required IconData iconData,
    required String title,
  }) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode
            ? kSecondaryDarkColor
            : kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Icon(
            iconData,
            color: kPrimaryLightColor,
            size: 50,
          ),
          const SizedBox(height: 15),
          Text(
            title,
            style: MyTextStyles.h3,
          )
        ],
      ),
    );
  }
}
