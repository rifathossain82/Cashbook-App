import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/profile_strength_slider_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/settings_list_tile_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/get_core.dart';

class TopSettingsWidget extends StatelessWidget {
  TopSettingsWidget({Key? key}) : super(key: key);

  final settingsList = [
    SettingsItemModel(
      routeName: '',
      leadingIcon: Icons.people_alt_outlined,
      titleText: 'Business Team',
      subtitleText: 'Add, remove or change role',
    ),
    SettingsItemModel(
      routeName: RouteGenerator.requestsPage,
      leadingIcon: Icons.check_circle_rounded,
      titleText: 'Requests',
      subtitleText: 'Approve or deny requests',
    ),
    SettingsItemModel(
      routeName: RouteGenerator.businessSettingsPage,
      leadingIcon: Icons.business,
      titleText: 'Business Settings',
      subtitleText: 'Settings specific to this business',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode? kSecondaryDarkColor : kWhite,
      ),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: settingsList.length,
        itemBuilder: (context, index) => SettingsListTileBuilder(
          itemModel: settingsList[index],
        ),
        separatorBuilder: (context, index) => const Divider(height: 10),
      ),
    );
  }
}
