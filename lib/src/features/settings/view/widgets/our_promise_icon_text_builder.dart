import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OurPromiseIconTextBuilder extends StatelessWidget {
  final IconData iconData;
  final Color iconColor;
  final String title;

  const OurPromiseIconTextBuilder({
    Key? key,
    required this.iconData,
    required this.iconColor,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: iconColor.withOpacity(0.2),
          ),
          child: Icon(
            iconData,
            color: iconColor,
            size: 18,
          ),
        ),
        const SizedBox(width: 8),
        Text(
          title,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: Get.find<ThemeController>().isDarkMode
                ? kWhite.withOpacity(0.7)
                : kBlackLight.withOpacity(0.7),
          ),
        ),
      ],
    );
  }
}
