import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class ProfileStrengthSlider extends StatelessWidget {
  final double percentage;

  const ProfileStrengthSlider({
    Key? key,
    required this.percentage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 15),
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: SizedBox(
            height: 10,
            child: LinearProgressIndicator(
              value: percentage / 100,
              backgroundColor: getSliderColor.withOpacity(0.2),
              color: getSliderColor,
            ),
          ),
        ),
        const SizedBox(height: 10),
        SizedBox(
          height: 24,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Profile Strength: ${getProfileStrength.capitalizedFirst}",
                style: MyTextStyles.h4,
              ),
              Text(
                "$percentage%",
                style: MyTextStyles.h4,
              ),
            ],
          ),
        ),
      ],
    );
  }

  String get getProfileStrength {
    if (percentage < 20) {
      return ProfileStrength.week.name;
    } else if (percentage < 40) {
      return ProfileStrength.low.name;
    } else if (percentage < 60) {
      return ProfileStrength.fair.name;
    } else if (percentage < 80) {
      return ProfileStrength.good.name;
    } else {
      return ProfileStrength.excellent.name;
    }
  }

  Color get getSliderColor {
    if (percentage < 20) {
      return kRed;
    } else if (percentage < 40) {
      return kOrange;
    } else if (percentage < 60) {
      return kPrimaryLightColor;
    } else if (percentage < 80) {
      return kBlue;
    } else {
      return kGreen;
    }
  }
}
