import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/do_more_with_cashbook_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/general_settings_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/logout_button.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/our_promise_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/settings_profile_card.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/top_settings_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final businessController = Get.find<BusinessController>();
  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor:
            themeController.isDarkMode ? kPrimaryDarkColor : kGreyLight,
        appBar: AppBar(
          title: Text('Settings'.tr),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SettingsProfileCard(
                businessController: businessController,
              ),
              TopSettingsWidget(),
              const DoMoreWithCashbookWidget(),
              GeneralSettingsWidget(),
              LogoutButton(),
              const OurPromiseWidget(),
              KDivider(
                height: 20,
                color: kGreyMedium,
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  '${'App Version'.tr} 3.3.1',
                  textAlign: TextAlign.start,
                  style: MyTextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: themeController.isDarkMode
                        ? kWhite.withOpacity(0.7)
                        : kBlackLight.withOpacity(0.7),
                  ),
                ),
              ),
              const SizedBox(height: 50),
            ],
          ),
        ),
      );
    });
  }
}
