import 'package:flutter/material.dart';

class SettingsItemModel {
  final IconData leadingIcon;
  final String titleText;
  final String subtitleText;
  final IconData? trailingIcon;
  final String? routeName;
  final VoidCallback? onTap;

  SettingsItemModel({
    required this.leadingIcon,
    required this.titleText,
    required this.subtitleText,
    this.trailingIcon,
    this.routeName,
    this.onTap,
  });
}
