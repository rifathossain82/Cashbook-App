class IntroSliderModel {
  final String assetImgPath;
  final String title;
  final String subTitle;

  IntroSliderModel({
    required this.assetImgPath,
    required this.title,
    required this.subTitle,
  });
}
