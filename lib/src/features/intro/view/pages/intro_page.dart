import 'package:carousel_slider/carousel_controller.dart';
import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/intro/model/intro_slider_model.dart';
import 'package:cashbook_app/src/features/intro/view/widgets/intro_slider_forward_button.dart';
import 'package:cashbook_app/src/features/intro/view/widgets/intro_slider_indicator_widget.dart';
import 'package:cashbook_app/src/features/intro/view/widgets/intro_slider_widget.dart';
import 'package:cashbook_app/src/features/localization/view/language_changer_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  int sliderIndex = 0;
  CarouselController sliderController = CarouselController();

  final sliderDataList = [
    IntroSliderModel(
      assetImgPath: AssetPath.introImage1,
      title: 'Intro 1 Title',
      subTitle: 'Intro 1 Subtitle',
    ),
    IntroSliderModel(
      assetImgPath: AssetPath.introImage2,
      title: 'Intro 2 Title',
      subTitle: 'Intro 2 Subtitle',
    ),
    IntroSliderModel(
      assetImgPath: AssetPath.introImage3,
      title: 'Intro 3 Title',
      subTitle: 'Intro 3 Subtitle',
    ),
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      languageChangerBottomSheet(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Image.asset(AssetPath.cashbookLogo),
        titleSpacing: 0,
        title: Text(
          'AppName'.tr,
          style: MyTextStyles.h3.copyWith(
            fontWeight: FontWeight.bold,
            color: kPrimaryLightColor,
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () => languageChangerBottomSheet(context),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: KOutlinedButton(
                width: 100,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Language'.tr,
                      style: MyTextStyles.h4.copyWith(
                        color: kGrey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(width: 2),
                    Icon(
                      Icons.arrow_drop_down,
                      color: kGrey,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            /// slider
            IntroSliderWidgetBuilder(
              controller: sliderController,
              sliderDataList: sliderDataList,
              sliderIndex: sliderIndex,
              onPageChanged: (index, reason) {
                setState(() {
                  sliderIndex = index;
                });
              },
            ),

            /// indicator
            Container(
              height: 50,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IntroSliderIndicatorWidget(
                    sliderLength: sliderDataList.length,
                    currentSliderIndex: sliderIndex,
                  ),
                  IntroSliderForwardButton(
                    onTap: () {
                      sliderController.nextPage();
                    },
                  ),
                ],
              ),
            ),

            /// spacer to divide slider and bottom widget (button & row text)
            const Spacer(),

            /// get started button
            KButton(
              onPressed: () {
                LocalStorage.saveData(
                  key: LocalStorageKey.into,
                  data: true,
                );
                Get.toNamed(RouteGenerator.login);
              },
              width: context.screenWidth - 30,
              child: Text(
                'GET STARTED'.tr,
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ),

            /// bottom widget (icon and text)
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    AssetPath.protectIcon,
                    height: 22,
                    width: 22,
                    color: kGreen,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    'CashBook is trusted by 30 Lakh+ users'.tr,
                    style: MyTextStyles.h5.copyWith(
                      color: kGreyTextColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
