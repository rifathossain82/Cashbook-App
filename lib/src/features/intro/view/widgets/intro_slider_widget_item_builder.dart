import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/intro/model/intro_slider_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IntroSliderWidgetItemBuilder extends StatelessWidget {
  final IntroSliderModel sliderModel;

  const IntroSliderWidgetItemBuilder({
    Key? key,
    required this.sliderModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset(
              sliderModel.assetImgPath,
              width: context.screenWidth,
            ),
          ),
          const SizedBox(height: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                sliderModel.title.tr,
                maxLines: 1,
                style: MyTextStyles.h2.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 5),
              Text(
                sliderModel.subTitle.tr,
                maxLines: 1,
                style: MyTextStyles.h4,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
