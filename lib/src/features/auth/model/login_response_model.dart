class LoginResponseModel {
  List<Sidebar>? sidebar;
  User? user;
  String? message;
  String? token;
  int? status;

  LoginResponseModel({
    this.sidebar,
    this.user,
    this.message,
    this.token,
    this.status,
  });

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['sidebar'] != null) {
      sidebar = <Sidebar>[];
      json['sidebar'].forEach((v) {
        sidebar!.add(Sidebar.fromJson(v));
      });
    }
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    message = json['message'];
    token = json['token'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (sidebar != null) {
      data['sidebar'] = sidebar!.map((v) => v.toJson()).toList();
    }
    if (user != null) {
      data['user'] = user!.toJson();
    }
    data['message'] = message;
    data['token'] = token;
    data['status'] = status;
    return data;
  }
}

class Sidebar {
  int? id;
  String? name;
  int? businessCategoryId;
  int? businessTypeId;
  int? userId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;

  Sidebar({
    this.id,
    this.name,
    this.businessCategoryId,
    this.businessTypeId,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  Sidebar.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    businessCategoryId = json['business_category_id'];
    businessTypeId = json['business_type_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['business_category_id'] = businessCategoryId;
    data['business_type_id'] = businessTypeId;
    data['user_id'] = userId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  dynamic avatar;
  dynamic roleId;

  User({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.avatar,
    this.roleId,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
    roleId = json['role_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['avatar'] = avatar;
    data['role_id'] = roleId;
    return data;
  }
}
