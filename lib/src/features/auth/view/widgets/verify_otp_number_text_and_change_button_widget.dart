import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class VerifyOTPNumberTextAndChangeButtonWidget extends StatelessWidget {
  final String mobileNumber;

  const VerifyOTPNumberTextAndChangeButtonWidget({
    Key? key,
    required this.mobileNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'number $mobileNumber',
            style: MyTextStyles.h5.copyWith(
              color: kGreyTextColor,
            ),
          ),
          VerticalDivider(
            thickness: 2,
            indent: 3,
            endIndent: 3,
            color: kPrimaryLightColor,
          ),
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Text(
              'Change',
              style: MyTextStyles.h5.copyWith(
                color: kPrimaryLightColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
