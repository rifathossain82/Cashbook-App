import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginWelcomeTextWidget extends StatelessWidget {
  const LoginWelcomeTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Welcome!'.tr,
          style: MyTextStyles.h2.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          'Login to auto backup your data securely'.tr,
          style: MyTextStyles.h4.copyWith(
            color: kGreyTextColor,
          ),
        ),
      ],
    );
  }
}
