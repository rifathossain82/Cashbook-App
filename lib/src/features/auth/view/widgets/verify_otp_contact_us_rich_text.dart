import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class VerifyOTPContactUsRichText extends StatelessWidget {
  const VerifyOTPContactUsRichText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
        style: MyTextStyles.h4,
        children: [
          const TextSpan(
            text: 'Need Help? ',
          ),
          TextSpan(
            text: 'Contact Us',
            recognizer: TapGestureRecognizer()..onTap = () {},
            style: MyTextStyles.h4.copyWith(
              color: kPrimaryLightColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
