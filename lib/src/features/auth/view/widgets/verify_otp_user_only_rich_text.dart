import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class VerifyOTPUserOnlyRichText extends StatelessWidget {
  const VerifyOTPUserOnlyRichText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: MyTextStyles.h5.copyWith(
          color: kGreyTextColor,
        ),
        children: [
          const TextSpan(
            text: 'User only the ',
          ),
          TextSpan(
            text: ' latest OTP ',
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          const TextSpan(
            text: ' sent on ',
          ),
          TextSpan(
            text: ' SMS',
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
