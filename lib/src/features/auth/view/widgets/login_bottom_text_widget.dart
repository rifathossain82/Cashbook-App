import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginBottomTextWidget extends StatelessWidget {
  const LoginBottomTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          textAlign: TextAlign.start,
          text: TextSpan(
            style: MyTextStyles.h5.copyWith(
              color: kGreyTextColor,
            ),
            children: [
              TextSpan(
                text: 'By creating an account, you agree to our '.tr,
              ),
              TextSpan(
                text: 'Terms of Services & Privacy Policy'.tr,
                recognizer: TapGestureRecognizer()..onTap = () {},
                style: MyTextStyles.h5.copyWith(
                  color: kGreyTextColor,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 15),
        RichText(
          textAlign: TextAlign.start,
          text: TextSpan(
            style: MyTextStyles.h5.copyWith(
              color: kGreyTextColor,
            ),
            children: [
              TextSpan(
                text: 'Why do we need your mobile number? '.tr,
              ),
              TextSpan(
                text: 'Know More'.tr,
                recognizer: TapGestureRecognizer()..onTap = () {},
                style: MyTextStyles.h5.copyWith(
                  color: kGreyTextColor,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
