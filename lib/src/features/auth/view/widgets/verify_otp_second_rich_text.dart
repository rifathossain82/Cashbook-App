import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class VerifyOTPSecondRichText extends StatelessWidget {
  final int seconds;

  const VerifyOTPSecondRichText({
    Key? key,
    required this.seconds,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: MyTextStyles.h4.copyWith(
          color: kGreyTextColor,
          fontWeight: FontWeight.w600,
        ),
        children: [
          const TextSpan(
            text: 'Send OTP in ',
          ),
          TextSpan(
            text: '$seconds secs ',
            style: MyTextStyles.h4.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          const TextSpan(
            text: 'via',
          ),
        ],
      ),
    );
  }
}
