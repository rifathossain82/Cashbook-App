import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class VerifyOTPTextField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final ValueChanged onChanged;

  const VerifyOTPTextField({
    Key? key,
    required this.controller,
    required this.focusNode,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
        controller: controller,
        focusNode: focusNode,
        onChanged: onChanged,
        textAlign: TextAlign.center,
        style: const TextStyle(
          letterSpacing: 20,
          fontSize: 25,
        ),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        maxLength: 6,
        decoration: InputDecoration(
          counter: Container(),
          hintText: '******',
          hintStyle: TextStyle(
            letterSpacing: 20,
            color: kGreyTextColor,
          ),
        ),
      ),
    );
  }
}
