import 'dart:async';

import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/verify_otp_contact_us_rich_text.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/verify_otp_number_text_and_change_button_widget.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/verify_otp_second_rich_text.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/verify_otp_text_field.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/verify_otp_user_only_rich_text.dart';
import 'package:flutter/material.dart';

class VerifyOTPPage extends StatefulWidget {
  const VerifyOTPPage({Key? key}) : super(key: key);

  @override
  State<VerifyOTPPage> createState() => _VerifyOTPPageState();
}

class _VerifyOTPPageState extends State<VerifyOTPPage> {
  final otpTextController = TextEditingController();
  final otpTextFocusNode = FocusNode();

  /// received from login page
  String? countryCode;
  String? mobileNumber;

  ///initially I set session time of otp is 60 seconds or 1 minute
  int seconds = 60;

  ///a timer for countdown
  late Timer _timer;

  @override
  void initState() {
    setTimer();
    otpTextFocusNode.requestFocus();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    countryCode = context.getArguments[0];
    mobileNumber = context.getArguments[1];
    super.didChangeDependencies();
  }

  ///countdown of session time
  void setTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (seconds == 0) {
        setState(() {
          timer.cancel();
          kPrint('Time out');
        });
      } else {
        setState(() {
          seconds--;
        });
      }
    });
  }

  @override
  void dispose() {
    otpTextController.dispose();
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
              onPressed: () {},
              child: Text(
                'HELP',
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Text(
              'Verify OTP',
              style: MyTextStyles.h2.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              'We have sent OTP to your mobile ',
              style: MyTextStyles.h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
            VerifyOTPNumberTextAndChangeButtonWidget(
              mobileNumber: '$countryCode-$mobileNumber',
            ),
            SizedBox(height: context.screenHeight * 0.03),
            VerifyOTPTextField(
              controller: otpTextController,
              focusNode: otpTextFocusNode,
              onChanged: (value) {
                if (value.toString().length == 6) {
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    RouteGenerator.dashboard,
                    (route) => false,
                  );
                }
              },
            ),
            SizedBox(height: context.screenHeight * 0.05),
            seconds != 0
                ? VerifyOTPSecondRichText(seconds: seconds)
                : Text(
                    'Resend OTP via',
                    style: MyTextStyles.h4.copyWith(
                      color: kGreyTextColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
            _buildButtons(),
            const VerifyOTPUserOnlyRichText(),
            const Spacer(),
            const VerifyOTPContactUsRichText(),
          ],
        ),
      ),
    );
  }

  Widget _buildButtons() {
    return Padding(
      padding: const EdgeInsets.only(
        top: 15,
        bottom: 20,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: KOutlinedButton(
              onPressed: () {},
              borderColor: seconds != 0 ? kGrey : kPrimaryLightColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.sms_outlined,
                    color: seconds != 0 ? kGrey : kPrimaryLightColor,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    'SMS',
                    style: MyTextStyles.h4.copyWith(
                      fontWeight: FontWeight.bold,
                      color: seconds != 0 ? kGrey : kPrimaryLightColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 15),
          Expanded(
            child: KOutlinedButton(
              onPressed: () {},
              borderColor: seconds != 0 ? kGrey : kGreen,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.whatsapp,
                    color: seconds != 0 ? kGrey : kGreen,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    'WHATSAPP',
                    style: MyTextStyles.h4.copyWith(
                      fontWeight: FontWeight.bold,
                      color: seconds != 0 ? kGrey : kGreen,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
