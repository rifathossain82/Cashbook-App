import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/auth/controller/auth_controller.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/login_bottom_text_widget.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/login_welcome_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  bool passwordVisibility = true;
  final _loginFormKey = GlobalKey<FormState>();

  final authController = Get.find<AuthController>();

  @override
  void dispose() {
    emailTextController.dispose();
    passwordTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            child: Form(
              key: _loginFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: context.screenHeight * 0.05),
                  const LoginWelcomeTextWidget(),
                  SizedBox(height: context.screenHeight * 0.05),
                  _loginFormWidget(),
                  SizedBox(height: context.screenHeight * 0.04),
                  _buildLoginButton(),
                  SizedBox(height: context.screenHeight * 0.05),
                  const LoginBottomTextWidget(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _loginFormWidget() {
    return Column(
      children: [
        TextFormField(
          controller: emailTextController,
          validator: (value) {
            if (value.toString().isEmpty) {
              return Message.emptyEmail.tr;
            } else if (!value.toString().isValidEmail) {
              return Message.invalidEmail.tr;
            }
            return null;
          },
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            isDense: true,
            labelText: 'Email'.tr,
            border: const OutlineInputBorder(),
          ),
        ),
        const SizedBox(height: 20),
        TextFormField(
          controller: passwordTextController,
          validator: (value) {
            if (value.toString().isEmpty) {
              return Message.emptyPassword.tr;
            } else if (value.toString().length < 6) {
              return Message.invalidPassword.tr;
            }
            return null;
          },
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.visiblePassword,
          obscureText: passwordVisibility,
          decoration: InputDecoration(
            isDense: true,
            labelText: 'Password'.tr,
            border: const OutlineInputBorder(),
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  passwordVisibility = !passwordVisibility;
                });
              },
              child: Icon(
                passwordVisibility ? Icons.visibility_off : Icons.visibility,
                color: kBlackLight,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoginButton() => KButton(
        onPressed: () => _loginMethod(),
        child: Obx(
          () {
            return authController.isLoading.value
                ? Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: kWhite,
                    ),
                  )
                : Text(
                    'Login'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: MyTextStyles.h3.copyWith(
                        color: kWhite,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  );
          },
        ),
      );

  void _loginMethod(){
    if (_loginFormKey.currentState!.validate()) {
      authController.login(
        email: emailTextController.text,
        password: passwordTextController.text,
      );
    }
  }
}
