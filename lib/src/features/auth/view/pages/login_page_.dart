import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/localization/view/language_changer_bottom_sheet.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/login_bottom_text_widget.dart';
import 'package:cashbook_app/src/features/auth/view/widgets/login_welcome_text_widget.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final languageTextController = TextEditingController();
  final countryCodeTextController = TextEditingController();
  final mobileNumberTextController = TextEditingController();

  @override
  void initState() {
    languageTextController.text = 'English';
    countryCodeTextController.text = '+880';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: context.screenHeight * 0.05),
                const LoginWelcomeTextWidget(),
                SizedBox(height: context.screenHeight * 0.05),
                _loginFormWidget(),
                SizedBox(height: context.screenHeight * 0.04),
                _buildButtons(),
                SizedBox(height: context.screenHeight * 0.05),
                const LoginBottomTextWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _loginFormWidget() {
    return Column(
      children: [
        TextFormField(
          controller: languageTextController,
          readOnly: true,
          onTap: () => languageChangerBottomSheet(context),
          decoration: InputDecoration(
            isDense: true,
            labelText: 'Select Language',
            labelStyle: MyTextStyles.h4.copyWith(color: kGrey),
            suffixIcon: Icon(
              Icons.arrow_drop_down,
              color: kBlackLight,
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: kGrey),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kGrey),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: TextFormField(
                controller: countryCodeTextController,
                readOnly: true,
                onTap: () {},
                decoration: InputDecoration(
                  isDense: true,
                  suffixIconConstraints: const BoxConstraints(
                    maxHeight: 25,
                    maxWidth: 40,
                    minWidth: 30,
                  ),
                  suffixIcon: Icon(
                    Icons.arrow_drop_down,
                    color: kBlackLight,
                    size: 20,
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: kGrey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: kGrey),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 15),
            Expanded(
              flex: 7,
              child: TextFormField(
                controller: mobileNumberTextController,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  isDense: true,
                  labelText: 'Mobile Number',
                  labelStyle: MyTextStyles.h4.copyWith(color: kGrey),
                  border: const OutlineInputBorder(),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildButtons() {
    return Column(
      children: [
        KButton(
          onPressed: sendOTPOnWhatsappMethod,
          bgColor: kGreen,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.whatsapp,
                color: kWhite,
              ),
              const SizedBox(width: 8),
              Text(
                'SEND OTP ON WHATSAPP',
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 15),
        KOutlinedButton(
          onPressed: sendOTPOnViaSMSMethod,
          borderColor: kPrimaryLightColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.sms_outlined,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 8),
              Text(
                'SEND OTP ON VIA SMS',
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void sendOTPOnWhatsappMethod() {
    Navigator.pushNamed(
      context,
      RouteGenerator.verifyOTPPage,
      arguments: ['+880', mobileNumberTextController.text],
    );
  }

  void sendOTPOnViaSMSMethod() {
    Navigator.pushNamed(
      context,
      RouteGenerator.verifyOTPPage,
      arguments: ['+880', mobileNumberTextController.text],
    );
  }
}