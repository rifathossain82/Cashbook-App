import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/auth/model/login_response_model.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  var isLoading = false.obs;
  Rx<LoginResponseModel> loginResponse = LoginResponseModel().obs;

  Future login({
    required String email,
    required String password,
  }) async {
    try {
      isLoading(true);

      var map = <String, dynamic>{};
      map['email'] = email;
      map['password'] = password;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.login,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(message: 'Logged in Successfully!', bgColor: successColor);
        loginResponse.value = LoginResponseModel.fromJson(responseBody);
        LocalStorage.saveData(
          key: LocalStorageKey.token,
          data: loginResponse.value.token,
        );
        Get.offAllNamed(RouteGenerator.dashboard);
      } else {
        throw 'Logged in Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future logout() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.logout,
        ),
      );

      if (responseBody != null) {
        LocalStorage.removeData(key: LocalStorageKey.token);
        LocalStorage.removeData(key: LocalStorageKey.selectedBusinessId);
        kSnackBar(message: 'Logout Successfully!', bgColor: successColor);
        Get.offAllNamed(RouteGenerator.login);
      } else {
        kSnackBar(message: 'Logout Failed!', bgColor: failedColor);
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
