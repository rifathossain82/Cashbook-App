import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/payment_modes/controller/payment_method_controller.dart';
import 'package:cashbook_app/src/features/payment_modes/model/payment_method_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future updatePaymentModeBottomSheet({
  required BuildContext context,
  required PaymentMethodModel paymentMethodModel,
}) {
  final formKey = GlobalKey<FormState>();
  final paymentMethodController = Get.find<PaymentMethodController>();
  final paymentModeNameTextController = TextEditingController();

  paymentModeNameTextController.text = paymentMethodModel.name ?? '';

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.5, //size of bottom sheet
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Row(
                    children: [
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.close,
                          color: kGrey,
                        ),
                      ),
                      Text(
                        'Update Payment Method'.tr,
                        style: MyTextStyles.h2,
                      ),
                    ],
                  ),
                  const Divider(
                    height: 0,
                  ),

                  /// payment mode text field
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: TextFormField(
                      controller: paymentModeNameTextController,
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return Message.emptyPaymentMethod.tr;
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                        isDense: true,
                        labelText: 'Payment Method',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),

                  /// update button
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: KButton(
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.pop(context);
                          paymentMethodController.updatePaymentMethod(
                            id: '${paymentMethodModel.id}',
                            methodName: paymentModeNameTextController.text,
                          );
                        }
                      },
                      child: Text(
                        'Update'.tr,
                        style: MyTextStyles.h4.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
