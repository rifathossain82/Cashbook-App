import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/no_data_found.dart';
import 'package:cashbook_app/src/core/widgets/popup_menu_item_builder.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:cashbook_app/src/features/payment_modes/controller/payment_method_controller.dart';
import 'package:cashbook_app/src/features/payment_modes/model/payment_method_model.dart';
import 'package:cashbook_app/src/features/payment_modes/view/widgets/add_new_payment_mode_bottom_sheet.dart';
import 'package:cashbook_app/src/features/payment_modes/view/widgets/update_payment_mode_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChoosePaymentModesPage extends StatefulWidget {
  final PaymentMethodModel? paymentMethod;

  const ChoosePaymentModesPage({
    Key? key,
    required this.paymentMethod,
  }) : super(key: key);

  @override
  State<ChoosePaymentModesPage> createState() => _ChoosePaymentModesPageState();
}

class _ChoosePaymentModesPageState extends State<ChoosePaymentModesPage> {
  final paymentMethodController = Get.find<PaymentMethodController>();
  final themeController = Get.find<ThemeController>();
  PaymentMethodModel? selectedPaymentMethod;

  final suggestedPaymentModes = [
    'Bank',
    'Debit Card',
    'Credit Card',
    'Cheque',
  ];

  @override
  void initState() {
    selectedPaymentMethod = widget.paymentMethod;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Method'.tr),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings_outlined,
              color: kPrimaryLightColor,
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => addNewPaymentModeBottomSheet(context),
        backgroundColor: kPrimaryLightColor,
        elevation: 0,
        icon: Icon(
          Icons.add,
          color: kWhite,
        ),
        label: Text(
          'ADD NEW'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
      ),
      body: Obx(() {
        return paymentMethodController.isLoading.value
            ? const KCustomLoader()
            : paymentMethodController.paymentMethodList.isEmpty
                ? const NoDataFound()
                : Padding(
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Payment Modes in this book'.tr,
                            style: MyTextStyles.h5.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kGreyTextColor,
                            ),
                          ),
                          const SizedBox(height: 10),
                          _buildPaymentMethods(),
                          const SizedBox(height: 60),
                          // Text(
                          //   'Suggestions',
                          //   style: MyTextStyles.h5.copyWith(
                          //     fontWeight: FontWeight.bold,
                          //     color: kGreyTextColor,
                          //   ),
                          // ),
                          // const SizedBox(height: 10),
                          // Wrap(
                          //   children: suggestedPaymentModes
                          //       .map((tag) => SuggestedCategoryTagBuilder(
                          //             onTap: () => Navigator.pop(
                          //               context,
                          //               tag,
                          //             ),
                          //             text: tag,
                          //           ))
                          //       .toList(),
                          // ),
                        ],
                      ),
                    ),
                  );
      }),
    );
  }

  Widget _buildPaymentMethods() {
    List<PaymentMethodModel> paymentMethods =
        paymentMethodController.paymentMethodList;
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: paymentMethods.length,
      itemBuilder: (context, index) => RadioListTileWidgetBuilder(
        onChanged: (value) {
          setState(() {
            selectedPaymentMethod = paymentMethods[index];
            Get.back(
              result: selectedPaymentMethod,
            );
          });
        },
        value: paymentMethods[index].id,
        groupValue: selectedPaymentMethod?.id,
        isSelected: selectedPaymentMethod?.id == paymentMethods[index].id,
        title: paymentMethods[index].name ?? '',
        secondary: PopupMenuButton(
          color: Get.find<ThemeController>().isDarkMode
              ? kSecondaryDarkColor
              : kWhite,
          onSelected: (value) {
            if (value == PopupMenuItemOptions.edit) {
              updatePaymentModeBottomSheet(
                context: context,
                paymentMethodModel: paymentMethods[index],
              );
            } else {
              deletePaymentMethods(context, paymentMethods[index].id!);
            }
          },
          icon: Icon(
            Icons.more_vert_outlined,
            color: kGrey,
            size: 25,
          ),
          itemBuilder: (BuildContext context) => [
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.edit,
              icon: Icons.edit_outlined,
              iconColor: kGrey,
              title: 'Edit'.tr,
            ),
            popupMenuItemBuilder(
              value: PopupMenuItemOptions.delete,
              icon: Icons.delete_outline,
              iconColor: kGrey,
              title: 'Delete'.tr,
            ),
          ],
        ),
      ),
      separatorBuilder: (context, index) => const SizedBox(
        height: 8,
      ),
    );
  }


  void deletePaymentMethods(BuildContext context, int methodID) async {
    /// show alert dialog
    bool? result = await customDeleteDialog(
      context: context,
      dialogPosition: Alignment.center,
      title: 'Delete Payment Method?'.tr,
      contentText: "Are you sure you want to delete this payment method? This can't be undone.".tr,
      negativeActionText: 'NO'.tr,
      positiveActionText: 'YES, DELETE'.tr,
    );

    if (result ?? false) {
      final paymentMethodController = Get.find<PaymentMethodController>();
      paymentMethodController.deletePaymentMethod(
        id: '$methodID',
      );
    }
  }
}
