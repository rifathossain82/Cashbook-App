import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/payment_modes/model/payment_method_model.dart';
import 'package:get/get.dart';

class PaymentMethodController extends GetxController {
  var isLoading = false.obs;
  var paymentMethodList = <PaymentMethodModel>[].obs;

  @override
  void onInit() {
    getPaymentMethods();
    super.onInit();
  }

  Future getPaymentMethods() async {
    try {
      isLoading(true);
      paymentMethodList.value = [];

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.paymentMethodList,
        ),
      );

      if (responseBody != null) {
        var data = responseBody['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            paymentMethodList.add(PaymentMethodModel.fromJson(i));
            paymentMethodList.refresh();
          }
        }
      } else {
        throw 'Unable to load payment methods!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> addPaymentMethod({
    required String methodName,
  }) async {
    try {

      var map = <String, dynamic>{};
      map['name'] = methodName;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addPaymentMethod,
          body: map,
        ),
      );

      if (responseBody != null) {
        getPaymentMethods();
      } else {
        throw 'Failed to add payment method!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> updatePaymentMethod({
    required String id,
    required String methodName,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;
      map['name'] = methodName;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updatePaymentMethod,
          body: map,
        ),
      );

      if (responseBody != null) {
        // kSnackBar(
        //   message: "Category update successfully!",
        //   bgColor: successColor,
        // );
        getPaymentMethods();
      } else {
        throw 'Failed to update Payment Method!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }

  Future<void> deletePaymentMethod({
    required String id,
  }) async {
    try {
      var map = <String, dynamic>{};
      map['id'] = id;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.deletePaymentMethod,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Payment Method Deleted Successfully!",
          bgColor: successColor,
        );
        getPaymentMethods();
      } else {
        throw 'Failed to delete Payment Method!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      // isLoading(false);
    }
  }
}
