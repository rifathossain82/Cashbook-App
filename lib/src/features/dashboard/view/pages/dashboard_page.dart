import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/cashbooks/view/pages/cashbook_page.dart';
import 'package:cashbook_app/src/features/dashboard/view/widgets/top_indicator_widget.dart';
import 'package:cashbook_app/src/features/help/view/pages/help_page.dart';
import 'package:cashbook_app/src/features/settings/view/pages/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with SingleTickerProviderStateMixin {
  final themeController = Get.find<ThemeController>();
  late TabController _tabController;
  final pages = [
    const CashbookPage(),
    const HelpPage(),
    const SettingsPage(),
  ];

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: 0,
      length: 3,
      vsync: this,
    );

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        bottomNavigationBar: DefaultTabController(
          length: 3,
          initialIndex: 0,
          child: Material(
            color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
            child: TabBar(
              indicatorSize: TabBarIndicatorSize.label,
              labelColor: kPrimaryLightColor,
              unselectedLabelColor: kGreyTextColor,
              labelStyle: MyTextStyles.h5.copyWith(
                fontWeight: FontWeight.w600,
              ),
              unselectedLabelStyle: MyTextStyles.h5.copyWith(
                fontWeight: FontWeight.w700,
              ),
              indicator: TopIndicator(),
              controller: _tabController,
              tabs: <Widget>[
                Tab(
                  icon: const Icon(
                    Icons.book,
                  ),
                  text: 'Cashbooks'.tr,
                ),
                Tab(
                  icon: const Icon(
                    Icons.help_outline,
                  ),
                  text: 'Help'.tr,
                ),
                Tab(
                  icon: const Icon(
                    Icons.settings_outlined,
                  ),
                  text: 'settings'.tr,
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: pages,
        ),
      );
    });
  }
}
