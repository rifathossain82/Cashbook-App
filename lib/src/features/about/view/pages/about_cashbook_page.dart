import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/widgets/app_settings_list_tile_builder.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/settings_title_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AboutCashbookPage extends StatelessWidget {
  const AboutCashbookPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return Scaffold(
          backgroundColor: Get.find<ThemeController>().isDarkMode ? kPrimaryDarkColor : kGreyLight,
          appBar: AppBar(
            title: const Text('About Cashbook'),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// privacy
              const SettingsTitleTextBuilder(text: 'Privacy'),
              AppSettingsListTieBuilder(
                onTap: () {},
                leadingIcon: Icons.privacy_tip_outlined,
                title: 'Privacy Policy',
                trailing: const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 15,
                ),
              ),
              const KDivider(height: 0),
              AppSettingsListTieBuilder(
                onTap: () {},
                leadingIcon: Icons.onetwothree,
                title: 'Terms & Conditions',
                trailing: const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 15,
                ),
              ),

              /// about
              const SettingsTitleTextBuilder(text: 'About'),
              AppSettingsListTieBuilder(
                onTap: () {},
                leadingIcon: Icons.info_outline,
                title: 'About Us',
                trailing: const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 15,
                ),
              ),
            ],
          ),
        );
      }
    );
  }
}
