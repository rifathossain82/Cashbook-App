class UserInfoModel {
  int? id;
  String? name;
  String? email;
  String? phone;
  dynamic avatar;

  UserInfoModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.avatar,
  });

  UserInfoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['avatar'] = avatar;
    return data;
  }
}
