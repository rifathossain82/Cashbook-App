import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/network/api.dart';
import 'package:cashbook_app/src/core/network/network_utils.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/user_profile/model/user_info_model.dart';
import 'package:get/get.dart';

class UserProfileController extends GetxController {
  var isLoading = false.obs;
  Rx<UserInfoModel> userInfo = UserInfoModel().obs;

  Future getUserInfo() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(
          api: Api.userInfo,
        ),
      );

      if (responseBody != null) {
        userInfo.value = UserInfoModel.fromJson(
          responseBody['user'],
        );
      } else {
        throw 'Failed To Load User!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future<void> updateUserInfo({
    required Map<String, dynamic> body,
  }) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.updateUserInfo,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "User info update successfully!",
          bgColor: successColor,
        );
        getUserInfo();
      } else {
        throw 'Failed to update User Info!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
