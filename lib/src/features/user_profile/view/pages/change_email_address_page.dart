import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeEmailAddressPage extends StatefulWidget {
  final String emailAddress;

  const ChangeEmailAddressPage({
    Key? key,
    required this.emailAddress,
  }) : super(key: key);

  @override
  State<ChangeEmailAddressPage> createState() => _ChangeEmailAddressPageState();
}

class _ChangeEmailAddressPageState extends State<ChangeEmailAddressPage> {

  final emailAddressTextController = TextEditingController();
  final themeController = Get.find<ThemeController>();

  final formKey = GlobalKey<FormState>();
  bool isFormValid = false;

  @override
  void initState() {
    emailAddressTextController.text = widget.emailAddress;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Email Address'.tr),
      ),
      body: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            height: context.screenHeight,
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: emailAddressTextController,
                    validator: (value){
                      if(value.toString().isEmpty){
                        return Message.emptyEmail.tr;
                      } else if(!value.toString().isValidEmail){
                        return Message.invalidEmail.tr;
                      }
                      return null;
                    },
                    onChanged: (value){
                      setState(() {
                        if(value.toString().isNotEmpty){
                          if(value.toString().isValidEmail){
                            isFormValid = true;
                          }
                        } else {
                          isFormValid = false;
                        }
                      });
                    },
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      labelText: 'Email Address'.tr,
                      border: const OutlineInputBorder()
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildVerifyButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildVerifyButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
      child: KElevatedButton(
        onTap: verifyMethod,
        isFormValid: isFormValid,
        text: 'VERIFY'.tr,
      ),
    );
  }

  void verifyMethod() {
    if (formKey.currentState!.validate()) {}
  }
}
