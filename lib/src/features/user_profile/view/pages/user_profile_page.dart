import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_custom_loader.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/features/user_profile/controller/user_profile_controller.dart';
import 'package:cashbook_app/src/features/user_profile/view/pages/change_email_address_page.dart';
import 'package:cashbook_app/src/features/user_profile/view/pages/change_mobile_number_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({Key? key}) : super(key: key);

  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  final userProfileController = Get.find<UserProfileController>();
  final themeController = Get.find<ThemeController>();
  final fullNameTextController = TextEditingController();
  bool isFormValid = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      loadData();
    });
    super.initState();
  }

  void loadData() async {
    await userProfileController.getUserInfo();
    fullNameTextController.text =
        userProfileController.userInfo.value.name ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Profile'.tr),
      ),
      body: Obx(() {
        return userProfileController.isLoading.value
            ? const KCustomLoader()
            : Stack(
                children: [
                  SizedBox(
                    height: context.screenHeight,
                    child: Column(
                      children: [
                        const SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: TextFormField(
                            controller: fullNameTextController,
                            onChanged: (value) {
                              setState(() {
                                if (value.isNotEmpty) {
                                  isFormValid = true;
                                } else {
                                  isFormValid = false;
                                }
                              });
                            },
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              labelText: 'Your Full Name'.tr,
                              isDense: true,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Mobile Number'.tr,
                            style: MyTextStyles.h5.copyWith(
                              color: kGreyTextColor,
                            ),
                          ),
                          subtitle: Text(
                            userProfileController.userInfo.value.phone ??
                                'Not Set'.tr,
                            style: MyTextStyles.h4.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: TextButton(
                            onPressed: () => Get.to(
                              () => ChangeMobileNumberPage(
                                oldMobileNumber: userProfileController
                                        .userInfo.value.phone ??
                                    '',
                              ),
                            ),
                            child: Text(
                              'CHANGE'.tr,
                              style: MyTextStyles.h4.copyWith(
                                fontWeight: FontWeight.bold,
                                color: kPrimaryLightColor,
                              ),
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Email Address'.tr,
                            style: MyTextStyles.h5.copyWith(
                              color: kGreyTextColor,
                            ),
                          ),
                          subtitle: Text(
                            userProfileController.userInfo.value.email ??
                                'Not Set'.tr,
                            style: MyTextStyles.h4.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: TextButton(
                            onPressed: () => Get.to(
                              () => ChangeEmailAddressPage(
                                emailAddress: userProfileController
                                        .userInfo.value.email ??
                                    '',
                              ),
                            ),
                            child: Text(
                              'CHANGE'.tr,
                              style: MyTextStyles.h4.copyWith(
                                fontWeight: FontWeight.bold,
                                color: kPrimaryLightColor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: _buildUpdateButton(),
                  ),
                ],
              );
      }),
    );
  }

  Widget _buildUpdateButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      child: KElevatedButton(
        onTap: userInfoUpdateMethod,
        isFormValid: isFormValid,
        text: 'UPDATE'.tr,
      ),
    );
  }

  void userInfoUpdateMethod() {
    userProfileController.updateUserInfo(
      body: {
        'name': fullNameTextController.text,
      },
    );
  }
}
