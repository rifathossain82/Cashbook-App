import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/features/user_profile/controller/user_profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeMobileNumberPage extends StatefulWidget {
  final String oldMobileNumber;

  const ChangeMobileNumberPage({
    Key? key,
    required this.oldMobileNumber,
  }) : super(key: key);

  @override
  State<ChangeMobileNumberPage> createState() => _ChangeMobileNumberPageState();
}

class _ChangeMobileNumberPageState extends State<ChangeMobileNumberPage> {
  final userProfileController = Get.find<UserProfileController>();
  final themeController = Get.find<ThemeController>();

  final oldMobileNumberController = TextEditingController();
  final newMobileNumberController = TextEditingController();

  // final oldCountryCodeController = TextEditingController();
  // final newCountryCodeController = TextEditingController();

  final formKey = GlobalKey<FormState>();
  bool isFormValid = false;

  @override
  void initState() {
    // oldCountryCodeController.text = '+880';
    // newCountryCodeController.text = '+880';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Mobile Number'.tr),
      ),
      body: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            height: context.screenHeight,
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// old mobile number text form filed
                  const SizedBox(height: 20),
                  Text(
                    'Enter your old mobile number'.tr,
                    style: MyTextStyles.h4,
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: oldMobileNumberController,
                    onChanged: (value) {
                      setState(() {
                        if (oldMobileNumberController.text.isNotEmpty &&
                            newMobileNumberController.text.isNotEmpty) {
                          isFormValid = true;
                        } else {
                          isFormValid = false;
                        }
                      });
                    },
                    validator: (value) {
                      if (value.toString().isEmpty) {
                        return Message.emptyPhone.tr;
                      } else if (value.toString() != widget.oldMobileNumber) {
                        return Message.unregisteredPhoneNumber.tr;
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      isDense: true,
                      labelText: 'Mobile Number'.tr,
                      border: const OutlineInputBorder(),
                    ),
                  ),

                  const SizedBox(height: 40),

                  /// new mobile number text form filed
                  Text(
                    'Enter your new mobile number'.tr,
                    style: MyTextStyles.h4,
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: newMobileNumberController,
                    onChanged: (value) {
                      setState(() {
                        if (oldMobileNumberController.text.isNotEmpty &&
                            newMobileNumberController.text.isNotEmpty) {
                          isFormValid = true;
                        } else {
                          isFormValid = false;
                        }
                      });
                    },
                    validator: (value) {
                      if (value.toString().isEmpty) {
                        return Message.emptyPhone.tr;
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      isDense: true,
                      labelText: 'Mobile Number'.tr,
                      border: const OutlineInputBorder(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildNextButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildNextButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
      child: KElevatedButton(
        onTap: nextMethod,
        isFormValid: isFormValid,
        text: 'NEXT'.tr,
      ),
    );
  }

  void nextMethod() {
    if (formKey.currentState!.validate()) {
      Get.back();
      userProfileController.updateUserInfo(
        body: {
          'phone': newMobileNumberController.text,
        },
      );
    }
  }
}
