import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/localization/view/language_changer_bottom_sheet.dart';
import 'package:cashbook_app/src/core/widgets/settings_title_text_builder.dart';
import 'package:cashbook_app/src/core/widgets/app_settings_list_tile_builder.dart';
import 'package:cashbook_app/src/features/app_settings/view/widgets/app_settings_switch_list_tile_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppSettingsPage extends StatefulWidget {
  const AppSettingsPage({Key? key}) : super(key: key);

  @override
  State<AppSettingsPage> createState() => _AppSettingsPageState();
}

class _AppSettingsPageState extends State<AppSettingsPage> {
  bool appLock = false;
  bool groupBookNotifications = false;
  bool notificationWidget = false;
  bool amountFieldCalculator = false;
  bool darkTheme = false;

  final themeController = Get.put(ThemeController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor:
            themeController.isDarkMode ? kPrimaryDarkColor : kGreyLight,
        appBar: AppBar(
          title: Text('App Settings'.tr),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// data security
              SettingsTitleTextBuilder(text: 'Data Security'.tr),
              AppSettingsListTieBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.dataBackup,
                ),
                leadingIcon: Icons.backup_outlined,
                title: 'Data Backup'.tr,
                trailing: Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 15,
                  color: themeController.isDarkMode ? kWhite : kBlackLight,
                ),
              ),
              const KDivider(height: 0),
              AppSettingsSwitchListTileBuilder(
                value: appLock,
                onChanged: (value) {
                  setState(() {
                    appLock = value;
                  });
                },
                secondaryIcon: Icons.lock_outline,
                title: 'Data Backup'.tr,
              ),

              /// features
              SettingsTitleTextBuilder(text: 'Features'.tr),
              AppSettingsSwitchListTileBuilder(
                value: groupBookNotifications,
                onChanged: (value) {
                  setState(() {
                    groupBookNotifications = value;
                  });
                },
                secondaryIcon: Icons.notifications_none,
                title: 'Group Book Notifications'.tr,
              ),
              const KDivider(height: 0),
              AppSettingsSwitchListTileBuilder(
                value: notificationWidget,
                onChanged: (value) {
                  setState(() {
                    notificationWidget = value;
                  });
                },
                secondaryIcon: Icons.notification_add_outlined,
                title: 'Notification Widget'.tr,
              ),
              const KDivider(height: 0),
              AppSettingsSwitchListTileBuilder(
                value: amountFieldCalculator,
                onChanged: (value) {
                  setState(() {
                    amountFieldCalculator = value;
                  });
                },
                secondaryIcon: Icons.calculate_outlined,
                title: 'Amount Field Calculator'.tr,
              ),

              /// general
              SettingsTitleTextBuilder(text: 'General'.tr),
              AppSettingsListTieBuilder(
                onTap: () => languageChangerBottomSheet(context),
                leadingIcon: Icons.language_outlined,
                title: 'Change Language'.tr,
                trailing: Text(
                  'Language'.tr,
                  style: MyTextStyles.h4.copyWith(
                    color: kGreyTextColor,
                  ),
                ),
              ),
              const KDivider(height: 0),
              AppSettingsSwitchListTileBuilder(
                value: themeController.isDarkMode,
                onChanged: (value) {
                  themeController.toggleTheme();
                },
                secondaryIcon: Icons.dark_mode_outlined,
                title: 'Dark Theme'.tr,
              ),
            ],
          ),
        ),
      );
    });
  }
}
