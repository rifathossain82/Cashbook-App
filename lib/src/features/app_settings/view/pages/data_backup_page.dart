import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DataBackupPage extends StatelessWidget {
  const DataBackupPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Backup'.tr),
      ),
      body: ListTile(
        contentPadding: const EdgeInsets.all(15),
        leading: Icon(
          Icons.backup_outlined,
          color: Get.find<ThemeController>().isDarkMode ? kWhite : kBlackLight,
        ),
        title: Text(
          'Automatic data backup'.tr,
          style: MyTextStyles.h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Your data is automatically synced to cloud as long as you are connected to internet'
                  .tr,
              style: MyTextStyles.h4.copyWith(
                color: kGreyTextColor,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              '${'Last sync time'.tr}:',
              style: MyTextStyles.h4.copyWith(
                color: kGreyTextColor,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              '17 Apr 2023, 10:20 AM',
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const KDivider(height: 20),
            Text(
              'You can also sync your data manually'.tr,
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            KButton(
              onPressed: () {},
              bgColor: kGreen,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.sync,
                    color: kWhite,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    'SYNC NOW'.tr,
                    style: MyTextStyles.h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kWhite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
