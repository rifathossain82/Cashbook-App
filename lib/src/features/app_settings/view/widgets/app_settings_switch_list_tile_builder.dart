import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppSettingsSwitchListTileBuilder extends StatelessWidget {
  final bool value;
  final ValueChanged onChanged;
  final IconData secondaryIcon;
  final String title;

  AppSettingsSwitchListTileBuilder({
    Key? key,
    required this.value,
    required this.onChanged,
    required this.secondaryIcon,
    required this.title,
  }) : super(key: key);

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      tileColor: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
      title: Text(
        title,
        style: MyTextStyles.h3,
      ),
      secondary: Icon(
        secondaryIcon,
        color: kPrimaryLightColor,
      ),
      value: value,
      onChanged: onChanged,
    );
  }
}
