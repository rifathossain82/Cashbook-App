import 'package:cashbook_app/src/features/requests/view/widgets/no_request_found_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReceivedRequestWidget extends StatelessWidget {
  const ReceivedRequestWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NoRequestFoundWidget(
      title: 'No received requests found!'.tr,
      subtitle: 'If your staff members send a request to move a book from their business to this business then those will appear here'.tr,
    );
  }
}
