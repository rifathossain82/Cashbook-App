import 'package:cashbook_app/src/features/requests/view/widgets/no_request_found_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SentRequestWidget extends StatelessWidget {
  const SentRequestWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NoRequestFoundWidget(
      title: 'No sent requests found!'.tr,
      subtitle: 'If you send a requests to move book from this business to another then those will appear here'.tr,
    );
  }
}
