import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class NoRequestFoundWidget extends StatelessWidget {
  final String title;
  final String subtitle;

  const NoRequestFoundWidget({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          Text(
            subtitle,
            textAlign: TextAlign.center,
            style: MyTextStyles.h4,
          ),
        ],
      ),
    );
  }
}
