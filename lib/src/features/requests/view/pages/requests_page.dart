import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/requests/view/widgets/received_request_widget.dart';
import 'package:cashbook_app/src/features/requests/view/widgets/sent_request_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RequestsPage extends StatefulWidget {
  const RequestsPage({Key? key}) : super(key: key);

  @override
  State<RequestsPage> createState() => _RequestsPageState();
}

class _RequestsPageState extends State<RequestsPage>
    with SingleTickerProviderStateMixin {

  final themeController = Get.find<ThemeController>();
  late TabController _tabController;

  final List<String> tabTitles = ['Received', 'Sent'];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      initialIndex: 0,
      length: 2,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeController.isDarkMode? kPrimaryDarkColor : kGreyLight,
      appBar: AppBar(
        title: Text('Requests'.tr),
      ),
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
              ),
              child: TabBar(
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: kPrimaryLightColor,
                labelColor: kPrimaryLightColor,
                unselectedLabelColor: kGreyTextColor,
                labelStyle: MyTextStyles.h5.copyWith(
                  fontWeight: FontWeight.w700,
                ),
                unselectedLabelStyle: MyTextStyles.h5.copyWith(
                  fontWeight: FontWeight.w700,
                ),
                controller: _tabController,
                labelPadding: EdgeInsets.zero,
                tabs: tabTitles.map((title) => Tab(text: title.tr)).toList(),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  ReceivedRequestWidget(),
                  SentRequestWidget(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
