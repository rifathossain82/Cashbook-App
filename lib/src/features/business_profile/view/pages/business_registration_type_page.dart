import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessRegistrationTypePage extends StatefulWidget {
  const BusinessRegistrationTypePage({Key? key}) : super(key: key);

  @override
  State<BusinessRegistrationTypePage> createState() => _BusinessRegistrationTypePageState();
}

class _BusinessRegistrationTypePageState extends State<BusinessRegistrationTypePage> {
  String? selectedRegistrationType;

  final businessRegistrationTypeList = [
    'Individual',
    'Sole Proprietorship/HUF',
    'Partnership',
    'Public/Private Limited',
    'Trust Societies',
    'LLP',
    'Other',
    'Unregistered Business',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Business Registration Type'.tr),
      ),
      body: Column(
        children: [
          /// title and business type list
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'This will help us personalise your app experience',
                      style: MyTextStyles.h4.copyWith(
                        color: kGreyTextColor,
                      ),
                    ),
                  ),

                  /// business registration type list view
                  _buildBusinessRegistrationTypeListView(),
                ],
              ),
            ),
          ),

          /// save button
          KElevatedButton(
            onTap: doneMethod,
            isFormValid: selectedRegistrationType != null,
            text: 'SAVE'.tr,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildBusinessRegistrationTypeListView() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(
        horizontal: 15.0,
      ),
      itemCount: businessRegistrationTypeList.length,
      itemBuilder: (context, index) => RadioListTileWidgetBuilder(
        onChanged: (value) {
          setState(() {
            selectedRegistrationType = businessRegistrationTypeList[index];
          });
        },
        value: businessRegistrationTypeList[index],
        groupValue: selectedRegistrationType,
        isSelected: selectedRegistrationType == businessRegistrationTypeList[index],
        title: businessRegistrationTypeList[index],
      ),
      separatorBuilder: (context, index) => const SizedBox(
        height: 8,
      ),
    );
  }

  void doneMethod() {
    Navigator.pop(context);
  }
}
