import 'package:cashbook_app/src/core/errors/messages.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/business_address_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddBusinessAddressPage extends StatefulWidget {
  const AddBusinessAddressPage({Key? key}) : super(key: key);

  @override
  State<AddBusinessAddressPage> createState() => _AddBusinessAddressPageState();
}

class _AddBusinessAddressPageState extends State<AddBusinessAddressPage> {
  final formKey = GlobalKey<FormState>();
  final buildingNameTextController = TextEditingController();
  final areaTextController = TextEditingController();
  final pinCodeTextController = TextEditingController();
  final stateTextController = TextEditingController();
  final cityTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Business Address'.tr),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    BusinessAddressTextFormFieldBuilder(
                      controller: buildingNameTextController,
                      label: 'Building Name'.tr,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                    ),
                    BusinessAddressTextFormFieldBuilder(
                      controller: areaTextController,
                      label: 'Area/Locality'.tr,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return Message.emptyField.tr;
                        }
                        return null;
                      },
                    ),
                    BusinessAddressTextFormFieldBuilder(
                      controller: pinCodeTextController,
                      label: 'Pin Code'.tr,
                      textInputType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return Message.emptyField.tr;
                        }
                        return null;
                      },
                    ),
                    BusinessAddressTextFormFieldBuilder(
                      controller: stateTextController,
                      label: 'State'.tr,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return Message.emptyField.tr;
                        }
                        return null;
                      },
                    ),
                    BusinessAddressTextFormFieldBuilder(
                      controller: cityTextController,
                      label: 'City'.tr,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                    ),
                    const SizedBox(height: 50),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildSaveButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildSaveButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: KButton(
        onPressed: saveBusinessAddressMethod,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.check,
              color: kWhite,
            ),
            const SizedBox(width: 15),
            Text(
              'SAVE'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void saveBusinessAddressMethod() {
    if (formKey.currentState!.validate()) {}
  }
}
