import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/helpers/helper_methods.dart';
import 'package:cashbook_app/src/core/services/image_services.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/basics_widget.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/business_info_widget.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/communication_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/profile_strength_slider_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessProfilePage extends StatefulWidget {
  const BusinessProfilePage({Key? key}) : super(key: key);

  @override
  State<BusinessProfilePage> createState() => _BusinessProfilePageState();
}

class _BusinessProfilePageState extends State<BusinessProfilePage>
    with SingleTickerProviderStateMixin {

  final themeController = Get.find<ThemeController>();

  late TabController _tabController;
  late ScrollController _scrollController;
  bool isSliverCollapsed = false;

  final List<String> tabTitles = ['Basics', 'Business Info', 'Communication'];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 3, vsync: this);
    _scrollController = ScrollController();

    _scrollController.addListener(() {
      if (_scrollController.offset > 150 &&
          !_scrollController.position.outOfRange) {
        if (!isSliverCollapsed) {
          isSliverCollapsed = true;
          setState(() {});
        }
      }
      if (_scrollController.offset <= 150 &&
          !_scrollController.position.outOfRange) {
        if (isSliverCollapsed) {
          isSliverCollapsed = false;
          setState(() {});
        }
      }
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeController.isDarkMode? kPrimaryDarkColor : kGreyLight,
      appBar: AppBar(
        title: Text('Business Profile'.tr),
      ),
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: [

            /// profile strength
            Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
              ),
              child: Column(
                children: [
                  const ProfileStrengthSlider(
                    percentage: 12.5,
                  ),
                  const SizedBox(height: 15),
                  Container(
                    padding: const EdgeInsets.all(15),
                    width: context.screenWidth,
                    decoration: BoxDecoration(
                      color: kPrimaryLightColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.info,
                          color: kPrimaryLightColor,
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            '8 out of 10 fields are incomplete. Fill these to complete your profile',
                            textAlign: TextAlign.start,
                            style: MyTextStyles.h5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            /// upload business logo section
            GestureDetector(
              onTap: () => openImageSourceSelectorDialog(context),
              child: Container(
                padding: const EdgeInsets.all(15),
                margin: const EdgeInsets.only(bottom: 15),
                decoration: BoxDecoration(
                  color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(
                          color: kGrey,
                          width: 1,
                        ),
                      ),
                      child: Icon(
                        Icons.add_a_photo_outlined,
                        color: kGrey,
                        size: 40,
                      ),
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Upload Business Logo".tr,
                            style: MyTextStyles.h2.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryLightColor,
                            ),
                          ),
                          const SizedBox(height: 3),
                          Text(
                            "File Format: JPEG, PNG".tr,
                            style: MyTextStyles.h4.copyWith(
                              color: kGreyTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            /// tabBar and tabView
            Container(
              decoration: BoxDecoration(
                color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
              ),
              child: TabBar(
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: kPrimaryLightColor,
                labelColor: kPrimaryLightColor,
                unselectedLabelColor: kGreyTextColor,
                labelStyle: MyTextStyles.h5.copyWith(
                  fontWeight: FontWeight.w700,
                ),
                unselectedLabelStyle: MyTextStyles.h5.copyWith(
                  fontWeight: FontWeight.w700,
                ),
                controller: _tabController,
                labelPadding: EdgeInsets.zero,
                tabs: tabTitles.map((title) => Tab(text: title.tr)).toList(),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  BasisWidget(),
                  BusinessInfoWidget(),
                  CommunicationWidget(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void openImageSourceSelectorDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Choose an image from".tr,
      dialogPosition: Alignment.bottomCenter,
      actions: [
        /// image from camera
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.cameraImage();
            var imageFile = await ImageServices.getImageFile(img);

          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Camera'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),

        /// image from gallery
        KOutlinedButton(
          onPressed: () async {
            final img = await ImageServices.galleryImage();
            var imageFile = await ImageServices.getImageFile(img);

          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kPrimaryLightColor,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.image,
                color: kPrimaryLightColor,
              ),
              const SizedBox(width: 10),
              Text(
                'Gallery'.tr,
                style: MyTextStyles.h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
