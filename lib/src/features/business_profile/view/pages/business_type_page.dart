import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/features/add_business/model/business_type_model.dart';
import 'package:cashbook_app/src/core/widgets/business_type_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessTypePage extends StatefulWidget {
  const BusinessTypePage({Key? key}) : super(key: key);

  @override
  State<BusinessTypePage> createState() => _BusinessTypePageState();
}

class _BusinessTypePageState extends State<BusinessTypePage> {
  BusinessTypeModel? selectedBusinessType;

  final businessTypeList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Business Type'.tr),
      ),
      body: Column(
        children: [
          /// title and business type list
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'This will help us personalise your app experience',
                      style: MyTextStyles.h4.copyWith(
                        color: kGreyTextColor,
                      ),
                    ),
                  ),

                  /// business type list view
                  _buildBusinessTypeListView(),
                ],
              ),
            ),
          ),

          /// save button
          KElevatedButton(
            onTap: doneMethod,
            isFormValid: selectedBusinessType != null,
            text: 'SAVE'.tr,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildBusinessTypeListView() {
    return ListView.separated(
      padding: const EdgeInsets.all(15),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: businessTypeList.length,
      itemBuilder: (context, index) => BusinessTypeItemBuilder(
        onTap: () {
          setState(() {
            selectedBusinessType = businessTypeList[index];
          });
        },
        isSelected: selectedBusinessType == businessTypeList[index],
        typeModel: businessTypeList[index],
      ),
      separatorBuilder: (context, index) => const SizedBox(height: 15),
    );
  }

  void doneMethod() {
    Navigator.pop(context);
  }
}
