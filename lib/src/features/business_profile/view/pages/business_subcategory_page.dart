import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessSubcategoryPage extends StatefulWidget {
  const BusinessSubcategoryPage({Key? key}) : super(key: key);

  @override
  State<BusinessSubcategoryPage> createState() =>
      _BusinessSubcategoryPageState();
}

class _BusinessSubcategoryPageState extends State<BusinessSubcategoryPage> {
  /// received from category page
  String? selectedCategory;

  String? selectedSubcategory;
  List<String> subcategoryList = [
    'Dairy',
    'Fruits/Vegetables Shop',
    'Meat & Egg',
    'Super Marts',
  ];

  @override
  void didChangeDependencies() {
    selectedCategory = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Business Subcategory'.tr),
      ),
      body: Column(
        children: [
          /// selected category, change button and subcategory list view
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: kGreyLight,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Category'.tr,
                              style: MyTextStyles.h4,
                            ),
                            const SizedBox(height: 4),
                            Text(
                              selectedCategory ?? '',
                              style: MyTextStyles.h4.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        TextButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text(
                            'Change'.tr,
                            style: MyTextStyles.h4.copyWith(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryLightColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      "Subcategories of ${selectedCategory ?? ""}",
                      textAlign: TextAlign.start,
                      style: MyTextStyles.h4,
                    ),
                  ),

                  /// business subcategory list view
                  _buildBusinessSubcategoryListView(),
                ],
              ),
            ),
          ),

          /// save and next button
          KElevatedButton(
            onTap: () => Navigator.of(context)..pop()..pop(),
            isFormValid: selectedSubcategory != null,
            text: 'SAVE & NEXT'.tr,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildBusinessSubcategoryListView() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(
        horizontal: 15.0,
      ),
      itemCount: subcategoryList.length,
      itemBuilder: (context, index) => RadioListTileWidgetBuilder(
        onChanged: (value) {
          setState(() {
            selectedSubcategory = subcategoryList[index];
          });
        },
        value: subcategoryList[index],
        groupValue: selectedSubcategory,
        isSelected: selectedSubcategory == subcategoryList[index],
        title: subcategoryList[index],
      ),
      separatorBuilder: (context, index) => const SizedBox(
        height: 8,
      ),
    );
  }
}
