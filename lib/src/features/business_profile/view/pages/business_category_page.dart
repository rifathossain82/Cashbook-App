import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_elevated_button.dart';
import 'package:cashbook_app/src/features/add_business/model/business_category_model.dart';
import 'package:cashbook_app/src/core/widgets/business_category_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessCategoryPage extends StatefulWidget {
  const BusinessCategoryPage({Key? key}) : super(key: key);

  @override
  State<BusinessCategoryPage> createState() => _BusinessCategoryPageState();
}

class _BusinessCategoryPageState extends State<BusinessCategoryPage> {
  BusinessCategoryModel? selectedBusinessCategory;

  final businessCategoryList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Business Category'.tr),
      ),
      body: Column(
        children: [
          /// title and category grid list
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'This will help us personalise your app experience',
                      style: MyTextStyles.h4.copyWith(
                        color: kGreyTextColor,
                      ),
                    ),
                  ),

                  /// business category grid view
                  _buildBusinessCategoryGridView(),
                ],
              ),
            ),
          ),

          /// step text and next button
          Column(
            children: [
              Container(
                height: 50,
                width: context.screenWidth,
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                decoration: BoxDecoration(
                  color: Get.find<ThemeController>().isDarkMode
                      ? kSecondaryDarkColor
                      : kGreyLight.withOpacity(0.5),
                ),
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '${'Next'.tr}: ',
                        style: MyTextStyles.h4.copyWith(
                          color: kGreyTextColor,
                        ),
                      ),
                      TextSpan(
                        text: selectedBusinessCategory != null
                            ? ' ${'Choose Subcategory'.tr}'
                            : ' ${'Choose Category'.tr}',
                        style: MyTextStyles.h4,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20),
              KElevatedButton(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.businessSubcategoryPage,
                  arguments: selectedBusinessCategory!.name,
                ),
                isFormValid: selectedBusinessCategory != null,
                text: 'SAVE & NEXT'.tr,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBusinessCategoryGridView() {
    return GridView.builder(
      padding: const EdgeInsets.all(15),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 15,
        mainAxisSpacing: 15,
        childAspectRatio: 2 / 1,
      ),
      itemCount: businessCategoryList.length,
      itemBuilder: (context, index) => BusinessCategoryItemBuilder(
        onTap: () {
          setState(() {
            selectedBusinessCategory = businessCategoryList[index];
          });
        },
        isSelected: selectedBusinessCategory == businessCategoryList[index],
        categoryModel: businessCategoryList[index],
      ),
    );
  }
}
