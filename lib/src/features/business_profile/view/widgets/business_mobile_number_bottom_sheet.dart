import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/mobile_number_text_form_field_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future businessMobileNumberBottomSheet({
  required BuildContext context,
  required String countryCode,
  required String mobileNumber,
}) {
  final countryCodeTextController = TextEditingController();
  final mobileNumberTextController = TextEditingController();

  countryCodeTextController.text = countryCode;
  mobileNumberTextController.text = mobileNumber;

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: const EdgeInsets.only(top: 16),
            height: context.screenHeight * 0.5, //size of bottom sheet
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// heading and close button
                Row(
                  children: [
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.close,
                        color: kGrey,
                      ),
                    ),
                    Text(
                      'Business Mobile Number'.tr,
                      style: MyTextStyles.h2,
                    ),
                  ],
                ),
                const Divider(
                  height: 0,
                ),

                /// mobile number text controller
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: MobileNumberTextFormFieldBuilder(
                    countryCodeController: countryCodeTextController,
                    mobileNumberController: mobileNumberTextController,
                    onTapCountryCode: () {},
                  ),
                ),

                /// save button
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: KButton(
                    onPressed: () {},
                    child: Text(
                      'SAVE'.tr,
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                        color: kWhite,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
    },
  );
}
