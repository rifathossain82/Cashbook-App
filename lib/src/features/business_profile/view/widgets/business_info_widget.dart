import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/widgets/settings_item_builder.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessInfoWidget extends StatelessWidget {
  const BusinessInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final settingsList = [
      SettingsItemModel(
        onTap: () => Navigator.pushNamed(
          context,
          RouteGenerator.businessCategoryPage,
        ),
        leadingIcon: Icons.category_outlined,
        titleText: 'Business Category & Subcategory',
        subtitleText: "Ex. Food, transport etc.",
      ),
      SettingsItemModel(
        onTap: () => Navigator.pushNamed(
          context,
          RouteGenerator.businessTypePage,
        ),
        leadingIcon: Icons.account_tree_outlined,
        titleText: 'Business Type',
        subtitleText: 'Ex. Retailer, wholesaler etc.',
      ),
      SettingsItemModel(
        onTap: () => Navigator.pushNamed(
          context,
          RouteGenerator.businessRegistrationTypePage,
        ),
        leadingIcon: Icons.work_outline,
        titleText: 'Business Registration Type',
        subtitleText: 'Ex. Individual, Private Ltd. etc.',
      ),
    ];

    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: ListView.separated(
        itemCount: settingsList.length,
        itemBuilder: (context, index) => SettingsItemBuilder(
          itemModel: settingsList[index],
        ),
        separatorBuilder: (context, index) => const Divider(height: 10),
      ),
    );
  }
}
