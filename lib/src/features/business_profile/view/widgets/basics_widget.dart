import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/widgets/settings_item_builder.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/add_business_name_bottom_sheet.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/select_staff_size_bottom_sheet.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BasisWidget extends StatefulWidget {
  const BasisWidget({Key? key}) : super(key: key);

  @override
  State<BasisWidget> createState() => _BasisWidgetState();
}

class _BasisWidgetState extends State<BasisWidget> {
  @override
  Widget build(BuildContext context) {
    final settingsList = [
      SettingsItemModel(
        onTap: () => addBusinessNameBottomSheet(context),
        leadingIcon: Icons.business,
        titleText: 'Business Name',
        subtitleText: "Rifat Hossain's Business",
        trailingIcon: Icons.edit_outlined,
      ),
      SettingsItemModel(
        onTap: () => Navigator.pushNamed(
          context,
          RouteGenerator.addBusinessAddress,
        ),
        leadingIcon: Icons.location_on_outlined,
        titleText: 'Business Address',
        subtitleText: 'Add business location',
      ),
      SettingsItemModel(
        onTap: () => selectStaffSizeBottomSheet(context),
        leadingIcon: Icons.people_alt_outlined,
        titleText: 'Staff Size',
        subtitleText: 'Select no. of staff members',
      ),
    ];

    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: ListView.separated(
        itemCount: settingsList.length,
        itemBuilder: (context, index) => SettingsItemBuilder(
          itemModel: settingsList[index],
        ),
        separatorBuilder: (context, index) => const Divider(height: 10),
      ),
    );
  }
}
