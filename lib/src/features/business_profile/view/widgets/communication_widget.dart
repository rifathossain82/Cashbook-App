import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/settings_item_builder.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/add_business_email_bottom_sheet.dart';
import 'package:cashbook_app/src/features/business_profile/view/widgets/business_mobile_number_bottom_sheet.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/profile_strength_slider_widget.dart';
import 'package:cashbook_app/src/features/settings/view/widgets/settings_list_tile_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CommunicationWidget extends StatelessWidget {
  const CommunicationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingsList = [
      SettingsItemModel(
        onTap: () => businessMobileNumberBottomSheet(
          context: context,
          countryCode: '+880',
          mobileNumber: '1885256220',
        ),
        leadingIcon: Icons.local_phone_outlined,
        titleText: 'Business Mobile Number',
        subtitleText: "+880188525620",
        trailingIcon: Icons.edit_outlined,
      ),
      SettingsItemModel(
        onTap: () => addBusinessEmailBottomSheet(context),
        leadingIcon: Icons.mail_outline,
        titleText: 'Business Email',
        subtitleText: 'Add business email',
      ),
    ];

    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: ListView.separated(
        itemCount: settingsList.length,
        itemBuilder: (context, index) => SettingsItemBuilder(
          itemModel: settingsList[index],
        ),
        separatorBuilder: (context, index) => const Divider(height: 10),
      ),
    );
  }
}
