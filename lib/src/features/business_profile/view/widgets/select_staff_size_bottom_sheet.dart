import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/radio_list_tile_widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future selectStaffSizeBottomSheet(BuildContext context) {

  String selectedStaffSize = 'Last Update';
  List<String> staffSizes = [
    '1-3',
    '4-6',
    '7-10',
    '10+',
  ];

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
    ),
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            height: context.screenHeight * 0.62,
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                /// heading and close button
                Row(
                  children: [
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.close,
                        color: kGrey,
                      ),
                    ),
                    Text(
                      'Select Staff Size'.tr,
                      style: MyTextStyles.h2,
                    ),
                  ],
                ),
                KDivider(
                  color: kGrey,
                  height: 10,
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: staffSizes.length,
                    itemBuilder: (context, index) =>
                        RadioListTileWidgetBuilder(
                          onChanged: (value) {
                            setState(() {
                              selectedStaffSize = staffSizes[index];
                            });
                          },
                          value: staffSizes[index],
                          groupValue: selectedStaffSize,
                          isSelected: selectedStaffSize == staffSizes[index],
                          title: staffSizes[index],
                        ),
                    separatorBuilder: (context, index) => const SizedBox(
                      height: 8,
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                KButton(
                  onPressed: () {
                    /// close the bottom sheet
                    Navigator.pop(context);
                  },
                  bgColor: kPrimaryLightColor,
                  child: Text(
                    'APPLY'.tr,
                    style: MyTextStyles.h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kWhite,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
    },
  );
}
