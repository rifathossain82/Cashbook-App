import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class HelpItemBuilder extends StatelessWidget {
  const HelpItemBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        'How to use cashBook App?',
        style: MyTextStyles.h3,
      ),
      trailing: Icon(
        Icons.arrow_forward_ios_outlined,
        color: kGrey,
        size: 20,
      ),
    );
  }
}
