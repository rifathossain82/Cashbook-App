import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/features/auth/model/login_response_model.dart';
import 'package:cashbook_app/src/features/cashbooks/model/cashbook_model.dart';
import 'package:cashbook_app/src/features/cashbooks/view/widgets/cashbook_item_builder.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  final searchTextField = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: TextFormField(
          controller: searchTextField,
          decoration: const InputDecoration(
            hintText: 'Search by book name',
            border: InputBorder.none
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: fakeBookList.length,
        itemBuilder: (context, index) => CashbookItemBuilder(
          onTap: () {},
          cashbookModel: CashbookModel(),
        ),
      ),
    );
  }
}
