import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_outlined_button.dart';
import 'package:cashbook_app/src/core/widgets/user_type_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShareInvitationLinkPage extends StatefulWidget {
  const ShareInvitationLinkPage({Key? key}) : super(key: key);

  @override
  State<ShareInvitationLinkPage> createState() =>
      _ShareInvitationLinkPageState();
}

class _ShareInvitationLinkPageState extends State<ShareInvitationLinkPage> {
  /// received from parent page
  FakeUserData? userData;

  final themeController = Get.find<ThemeController>();

  @override
  void didChangeDependencies() {
    userData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Share Invitation Link'.tr),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                'DONE'.tr,
                style: MyTextStyles.h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryLightColor,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: _buildContent(),
          ),
          Positioned(
            bottom: 0,
            child: _buildShareButtons(),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Share invite link with ${userData!.name} and ask them to signup on CashBook',
              textAlign: TextAlign.center,
              style: MyTextStyles.h4,
            ),
            const SizedBox(height: 15),
            _buildUserCard(),
            const SizedBox(height: 20),
            Text(
              "Rifat Hossain's Business ${'Unique Invite Link'.tr}",
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 15),
            _buildCopyLinkCard(),
            const SizedBox(height: 15),
            _buildHintCard(),
            const SizedBox(height: 70),
          ],
        ),
      ),
    );
  }

  Widget _buildUserCard() {
    return Stack(
      children: [
        Container(
          height: 80,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(
              color: themeController.isDarkMode ? kSecondaryDarkColor : kGrey,
              width: 1,
            ),
          ),
          child: ListTile(
            leading: Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kGreyLight,
              ),
              child: Text(
                userData!.name.substring(0, 1),
                style: MyTextStyles.h2.copyWith(
                  color: kGreen,
                ),
              ),
            ),
            title: Text(
              userData!.name,
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              userData!.phone,
              style: MyTextStyles.h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
            trailing: UserTypeBuilder(
              type: userData!.userType.name.capitalizedFirst,
              color: userData!.userType == UserType.owner
                  ? kGreen
                  : userData!.userType == UserType.partner
                      ? kDeepOrange
                      : kBlue,
            ),
          ),
        ),
        Positioned(
          child: Container(
            height: 80,
            width: 4,
            color: kPrimaryLightColor,
          ),
        ),
      ],
    );
  }

  Widget _buildCopyLinkCard() {
    return ListTile(
      onTap: () {},
      dense: true,
      tileColor: themeController.isDarkMode
          ? kSecondaryDarkColor
          : kItemBlueShadowColor,
      horizontalTitleGap: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      leading: Container(
        padding: const EdgeInsets.all(3),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: kPrimaryLightColor,
        ),
        child: Icon(
          Icons.link,
          color: kWhite,
        ),
      ),
      title: Text(
        'http://mydaybookk.com/users',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: MyTextStyles.h4,
      ),
      trailing: Icon(
        Icons.copy,
        color: kPrimaryLightColor,
      ),
    );
  }

  Widget _buildHintCard() {
    return ListTile(
      onTap: () {},
      dense: true,
      tileColor: kGreen.withOpacity(0.2),
      contentPadding: const EdgeInsets.all(15),
      horizontalTitleGap: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      leading: Icon(
        Icons.security,
        color: kGreen,
      ),
      title: Text(
        'This link is 100% secure. Only ${userData!.name} can access this business by logging in with their mobile number ${userData!.phone}',
        style: MyTextStyles.h4,
      ),
    );
  }

  Widget _buildShareButtons() {
    return Container(
      height: 120,
      width: context.screenWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          KOutlinedButton(
            onPressed: () {},
            borderColor: kPrimaryLightColor,
            bgColor: Colors.transparent,
            child: Text(
              'SHARE ON OTHERS'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kPrimaryLightColor,
              ),
            ),
          ),
          const SizedBox(height: 8),
          KButton(
            onPressed: () {},
            bgColor: kGreen,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.whatsapp,
                  color: kWhite,
                ),
                const SizedBox(width: 15),
                Text(
                  'SHARE ON WHATSAPP'.tr,
                  style: MyTextStyles.h3.copyWith(
                    fontWeight: FontWeight.bold,
                    color: kWhite,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
