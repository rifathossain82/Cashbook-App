import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/mobile_number_text_form_field_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddMemberManuallyPage extends StatefulWidget {
  const AddMemberManuallyPage({Key? key}) : super(key: key);

  @override
  State<AddMemberManuallyPage> createState() => _AddMemberManuallyPageState();
}

class _AddMemberManuallyPageState extends State<AddMemberManuallyPage> {
  final searchTextController = TextEditingController();

  final countryCodeTextController = TextEditingController();
  final mobileNumberTextController = TextEditingController();

  @override
  void initState() {
    countryCodeTextController.text = '+880';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Member Manually'.tr),
      ),
      body: Stack(
        children: [
          Container(
            height: context.screenHeight,
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 25,
            ),
            child: MobileNumberTextFormFieldBuilder(
              countryCodeController: countryCodeTextController,
              mobileNumberController: mobileNumberTextController,
              onTapCountryCode: () {},
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildNextButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildNextButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Get.find<ThemeController>().isDarkMode
            ? kSecondaryDarkColor
            : kWhite,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: KButton(
        onPressed: () => Navigator.pushNamed(
          context,
          RouteGenerator.chooseRoleAndInvite,
          arguments: '8801885256220',
        ),
        child: Text(
          'NEXT'.tr,
          style: MyTextStyles.h3.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
      ),
    );
  }
}
