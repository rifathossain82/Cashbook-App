import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/team_member_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessTeamPage extends StatefulWidget {
  const BusinessTeamPage({Key? key}) : super(key: key);

  @override
  State<BusinessTeamPage> createState() => _BusinessTeamPageState();
}

class _BusinessTeamPageState extends State<BusinessTeamPage> {
  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeController.isDarkMode ? kSecondaryDarkColor : kGreyLight,
      appBar: AppBar(
        title: Text('Business Team'.tr),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            width: context.screenWidth,
            child: const TeamMemberListWidget(),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomWidget(),
          ),
        ],
      ),
    );
  }

  Widget _buildBottomWidget() {
    return Container(
      height: 120,
      width: context.screenWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      ),
      child: Column(
        children: [
          const KDivider(height: 0),
          ListTile(
            dense: true,
            leading: Icon(
              Icons.info,
              color: kSecondaryLightColor,
            ),
            title: Text(
              'View roles & permissions in detail'.tr,
              style: MyTextStyles.h4,
            ),
          ),
          KDivider(
            height: 0,
            color: kGrey,
          ),
          const SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: KButton(
              onPressed: () => Navigator.pushNamed(
                context,
                RouteGenerator.addTeamMember,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.person_add_outlined,
                    color: kWhite,
                  ),
                  const SizedBox(width: 15),
                  Text(
                    'ADD TEAM MEMBER'.tr,
                    style: MyTextStyles.h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kWhite,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
