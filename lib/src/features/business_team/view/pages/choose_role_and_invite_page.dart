import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_button.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/selectable_user_type_builder.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/user_permission_item_builder.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/user_restrictions_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChooseRoleAndInvitePage extends StatefulWidget {
  const ChooseRoleAndInvitePage({Key? key}) : super(key: key);

  @override
  State<ChooseRoleAndInvitePage> createState() =>
      _ChooseRoleAndInvitePageState();
}

class _ChooseRoleAndInvitePageState extends State<ChooseRoleAndInvitePage> {

  final themeController = Get.find<ThemeController>();

  /// received from parent page
  String? user;
  UserType selectedUserType = UserType.staff;

  final userTypes = [
    UserType.partner,
    UserType.staff,
  ];

  final List<String> staffPermissions = [
    'Limited access to selected books',
    'Owner/Partner can assign Admin, Viewer or Operator role to staff in any book',
  ];

  final List<String> staffRestrictions = [
    'No access to books they are not part of',
    'No access to business settings',
    'No option to delete books',
  ];

  final List<String> partnerPermissions = [
    'Full access to all books of this business',
    'Full access to business settings',
    'Add/remove members in business',
  ];

  final List<String> partnerRestrictions = [
    "Can't delete business",
    "Can't remove owner from business",
  ];

  @override
  void didChangeDependencies() {
    user = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose Role & Invite'.tr),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: _buildContent(),
          ),
          Positioned(
            bottom: 0,
            child: _buildInviteButton(),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Text(
              '$user is a new user. Send invite to $user to join this business',
              textAlign: TextAlign.center,
              style: MyTextStyles.h4,
            ),
            const SizedBox(height: 15),
            _buildUserCard(),
            const SizedBox(height: 30),
            _buildUserRoleCard(),
            const SizedBox(height: 10),
            Row(
              children: [
                Icon(
                  Icons.info_outline,
                  color: kGrey,
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Text(
                    'You can change this role later'.tr,
                    style: MyTextStyles.h4.copyWith(
                      color: kGreyTextColor,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 70),
          ],
        ),
      ),
    );
  }

  Widget _buildUserCard() {
    return Stack(
      children: [
        Container(
          height: 80,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(
              color: themeController.isDarkMode ? kSecondaryDarkColor : kGrey,
              width: 1,
            ),
          ),
          child: ListTile(
            leading: Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kGreyLight,
              ),
              child: Text(
                user!.substring(0, 1),
                style: MyTextStyles.h2.copyWith(
                  color: kGreen,
                ),
              ),
            ),
            title: Text(
              user ?? '',
              style: MyTextStyles.h4.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              'Not a Cashbook User. Send an invite'.tr,
              style: MyTextStyles.h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
          ),
        ),
        Positioned(
          child: Container(
            height: 80,
            width: 4,
            color: kRed,
          ),
        ),
      ],
    );
  }

  Widget _buildUserRoleCard() {
    return Container(
      decoration: BoxDecoration(
        color: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: themeController.isDarkMode ? kSecondaryDarkColor : kGrey,
          width: 1,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: Text(
              'Choose Role & Invite'.tr,
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const KDivider(height: 0),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            child: Wrap(
              children: userTypes
                  .map(
                    (type) => SelectableUserTypeBuilder(
                      onTap: () {
                        setState(() {
                          selectedUserType = type;
                        });
                      },
                      text: type.name.capitalizedFirst,
                      color: selectedUserType == type ? kPrimaryLightColor : kGrey,
                    ),
                  )
                  .toList(),
            ),
          ),
          selectedUserType == UserType.partner
              ? _buildPartnerPermissionAndRestrictions()
              : _buildStaffPermissionAndRestrictions(),
        ],
      ),
    );
  }

  Widget _buildPartnerPermissionAndRestrictions() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// partner permissions
          Text(
            'Permissions'.tr,
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: partnerPermissions.length,
            itemBuilder: (context, index) => UserPermissionItemBuilder(
              text: partnerPermissions[index],
            ),
            separatorBuilder: (context, index) => const SizedBox(height: 10),
          ),

          const SizedBox(height: 20),

          /// partner restrictions items
          Text(
            'Restrictions'.tr,
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: partnerRestrictions.length,
            itemBuilder: (context, index) => UserRestrictionItemBuilder(
              text: partnerRestrictions[index],
            ),
            separatorBuilder: (context, index) => const SizedBox(height: 10),
          ),
        ],
      ),
    );
  }

  Widget _buildStaffPermissionAndRestrictions() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// staff permissions
          Text(
            'Permissions'.tr,
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: staffPermissions.length,
            itemBuilder: (context, index) => UserPermissionItemBuilder(
              text: staffPermissions[index],
            ),
            separatorBuilder: (context, index) => const SizedBox(height: 10),
          ),

          const SizedBox(height: 20),

          /// staff restrictions items
          Text(
            'Restrictions'.tr,
            style: MyTextStyles.h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: staffRestrictions.length,
            itemBuilder: (context, index) => UserRestrictionItemBuilder(
              text: staffRestrictions[index],
            ),
            separatorBuilder: (context, index) => const SizedBox(height: 10),
          ),
        ],
      ),
    );
  }

  Widget _buildInviteButton() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: themeController.isDarkMode? kSecondaryDarkColor : kWhite,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: KButton(
        onPressed: () {
          Navigator.pop(context);
          Navigator.popAndPushNamed(
            context,
            RouteGenerator.shareInvitationLink,
            arguments: FakeUserData(
              name: 'Rifat Hossain',
              phone: '+8001885256220',
              userType: UserType.staff,
            ),
          );
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.add,
              color: kWhite,
            ),
            const SizedBox(width: 15),
            Text(
              '${'INVITE AS'.tr} ${selectedUserType.name.toUpperCase()}',
              style: MyTextStyles.h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
