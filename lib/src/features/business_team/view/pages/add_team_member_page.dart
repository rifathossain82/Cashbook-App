import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/settings_title_text_builder.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/contact_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTeamMemberPage extends StatefulWidget {
  const AddTeamMemberPage({Key? key}) : super(key: key);

  @override
  State<AddTeamMemberPage> createState() => _AddTeamMemberPageState();
}

class _AddTeamMemberPageState extends State<AddTeamMemberPage> {
  final searchTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Team Member'.tr),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => Navigator.pushNamed(
          context,
          RouteGenerator.addMemberManually,
        ),
        backgroundColor: kPrimaryLightColor,
        elevation: 0,
        label: Text(
          'ADD MANUALLY'.tr,
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kWhite,
          ),
        ),
        icon: Icon(
          Icons.add,
          color: kWhite,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            controller: searchTextController,
            decoration: InputDecoration(
              hintText: 'Type to search and add'.tr,
              prefixIcon: Icon(
                Icons.search,
                color: kPrimaryLightColor,
              ),
            ),
          ),
          const SettingsTitleTextBuilder(text: 'Choose From Contact (15)'),
          Expanded(
            child: ListView.separated(
              itemCount: 15,
              itemBuilder: (context, index) => ContactItemBuilder(),
              separatorBuilder: (context, index) => const KDivider(height: 0),
            ),
          ),
        ],
      ),
    );
  }
}
