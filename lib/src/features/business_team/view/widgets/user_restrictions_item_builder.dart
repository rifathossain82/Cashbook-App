import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserRestrictionItemBuilder extends StatelessWidget {
  final String text;

  const UserRestrictionItemBuilder({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          CupertinoIcons.clear_circled_solid,
          color: kRed,
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Text(
            text,
            style: MyTextStyles.h4,
          ),
        ),
      ],
    );
  }
}
