import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class UserPermissionItemBuilder extends StatelessWidget {
  final String text;

  const UserPermissionItemBuilder({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.check_circle_rounded,
          color: kGreen,
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Text(
            text,
            style: MyTextStyles.h4,
          ),
        ),
      ],
    );
  }
}
