import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/k_divider.dart';
import 'package:cashbook_app/src/core/widgets/settings_title_text_builder.dart';
import 'package:cashbook_app/src/features/business_team/view/widgets/team_member_item_builder.dart';
import 'package:flutter/material.dart';

class TeamMemberListWidget extends StatelessWidget {
  const TeamMemberListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// owner/partner
          const SettingsTitleTextBuilder(text: 'Owner/Partner'),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: ownerOrPartnerList.length,
            itemBuilder: (context, index) => TeamMemberItemBuilder(
              userData: ownerOrPartnerList[index],
            ),
            separatorBuilder: (context, index) => const KDivider(height: 0),
          ),

          /// staff
          SettingsTitleTextBuilder(text: 'Staff (${staffList.length})'),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: staffList.length,
            itemBuilder: (context, index) => TeamMemberItemBuilder(
              userData: staffList[index],
            ),
            separatorBuilder: (context, index) => const KDivider(height: 0),
          ),

          const SizedBox(height: 150),
        ],
      ),
    );
  }
}