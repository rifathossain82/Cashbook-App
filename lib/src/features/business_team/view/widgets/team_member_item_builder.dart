import 'dart:math';

import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/string_extension.dart';
import 'package:cashbook_app/src/core/fake_data/fake_data.dart';
import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/core/widgets/user_type_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TeamMemberItemBuilder extends StatelessWidget {
  final FakeUserData userData;

  TeamMemberItemBuilder({
    Key? key,
    required this.userData,
  }) : super(key: key);


  final themeController = Get.find<ThemeController>();
  final int index = Random().nextInt(randomColors.length);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {},
      dense: true,
      tileColor: themeController.isDarkMode? kPrimaryDarkColor : kWhite,
      leading: Container(
        height: 40,
        width: 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: themeController.isDarkMode? kSecondaryDarkColor : kGreyLight,
        ),
        child: Text(
          userData.name.substring(0,1),
          style: MyTextStyles.h2.copyWith(
            color: randomColors[index],
          ),
        ),
      ),
      title: Text(
        userData.name,
        style: MyTextStyles.h4.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        userData.phone,
        style: MyTextStyles.h5.copyWith(
          color: kGreyTextColor,
        ),
      ),
      trailing: UserTypeBuilder(
        type: userData.userType.name.capitalizedFirst,
        color: userData.userType == UserType.owner
            ? kGreen
            : userData.userType == UserType.partner
                ? kDeepOrange
                : kBlue,
      ),
    );
  }
}
