import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class NoTeamMemberFound extends StatelessWidget {
  const NoTeamMemberFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Add members & assign roles',
          style: MyTextStyles.h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          'Give access to limited features & books',
          style: MyTextStyles.h4.copyWith(
            color: kGreyTextColor,
          ),
        ),
      ],
    );
  }
}
