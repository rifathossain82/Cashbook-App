import 'dart:math';

import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContactItemBuilder extends StatelessWidget {
  ContactItemBuilder({Key? key}) : super(key: key);

  final themeController = Get.find<ThemeController>();
  final int index = Random().nextInt(randomColors.length);

  @override
  Widget build(BuildContext context) {
    int index = Random().nextInt(randomColors.length);
    return ListTile(
      onTap: () {},
      dense: true,
      tileColor: themeController.isDarkMode? kPrimaryDarkColor : kWhite,
      leading: Container(
        height: 40,
        width: 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: themeController.isDarkMode ? kSecondaryDarkColor : kGreyLight,
        ),
        child: Text(
          'R',
          style: MyTextStyles.h2.copyWith(
            color: randomColors[index],
          ),
        ),
      ),
      title: Text(
        'Rifat Hossain',
        style: MyTextStyles.h4.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        '+8801552323220',
        style: MyTextStyles.h5.copyWith(
          color: kGreyTextColor,
        ),
      ),
      trailing: TextButton(
        onPressed: () {},
        child: Text(
          'INVITE',
          style: MyTextStyles.h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kPrimaryLightColor,
          ),
        ),
      ),
    );
  }
}
