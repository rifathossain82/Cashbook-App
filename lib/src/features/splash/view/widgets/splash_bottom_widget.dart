import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/features/splash/view/widgets/splash_icon_text_builder.dart';
import 'package:flutter/material.dart';

class SplashBottomWidget extends StatelessWidget {
  const SplashBottomWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: kGreyLight,
          width: 1,
        ),
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SplashIconTextBuilder(
              iconData: Icons.security,
              iconColor: kSecondaryLightColor,
              title: '100% Secure',
            ),
            VerticalDivider(
              width: 20,
              color: kGreyLight,
              thickness: 1,
            ),
            SplashIconTextBuilder(
              iconData: Icons.cloud_done,
              iconColor: kPrimaryLightColor,
              title: 'Auto Data Backup',
            ),
          ],
        ),
      ),
    );
  }
}
