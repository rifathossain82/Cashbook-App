import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class SplashIconTextBuilder extends StatelessWidget {
  final IconData iconData;
  final Color iconColor;
  final String title;

  const SplashIconTextBuilder({
    Key? key,
    required this.iconData,
    required this.iconColor,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          iconData,
          color: iconColor,
        ),
        const SizedBox(width: 8),
        Text(
          title,
          style: MyTextStyles.h4,
        ),
      ],
    );
  }
}
