import 'dart:async';
import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/routes/routes.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/splash/view/widgets/splash_bottom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashPage> {
  setSplashDuration() async {
    return Timer(
      const Duration(seconds: 3),
      () => pageNavigation(),
    );
  }

  void pageNavigation() async {
    bool isIntroViewed = LocalStorage.getData(key: LocalStorageKey.into) ?? false;
    var token = LocalStorage.getData(key: LocalStorageKey.token);
    if(isIntroViewed){
      if(token != null){
        Get.offNamed(RouteGenerator.dashboard);
      } else{
        Get.offNamed(RouteGenerator.login);
      }
    } else{
      Get.offNamed(RouteGenerator.intro);
    }
  }

  @override
  void initState() {
    /// set duration and control what next after splash duration
    setSplashDuration();

    /// set primary color as a status bar color only for splash page
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: kPrimaryLightColor,
        statusBarIconBrightness: Brightness.light, // For Android (light icons)
        statusBarBrightness: Brightness.light, // For iOS (dark icons)
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    /// when dispose this page set greyLight color as a default
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: kGreyLight,
        statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
        statusBarBrightness: Brightness.light, // For iOS (dark icons)
      ),
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset(
                AssetPath.cashbookAppLogo,
                height: 220,
                width: 220,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Trust of ',
                      style: MyTextStyles.h2,
                    ),
                    TextSpan(
                      text: '30,00,000+ Businesses',
                      style: MyTextStyles.h2.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: context.screenHeight * 0.25,
              ),
              const SplashBottomWidget(),
              SizedBox(
                height: context.screenHeight * 0.05,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
