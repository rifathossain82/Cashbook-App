enum LocalStorageKey {
  token,
  into,
  selectedBusinessId,
  localeLanguageCode,
  isDarkMode,
}

enum CashbookItemsMenuOptions {
  details,
  rename,
  delete,
}

enum PopupMenuItemOptions {
  edit,
  delete,
}

enum TransactionType {
  cashIn,
  cashOut,
}

enum ProfileStrength {
  week,
  low,
  fair,
  good,
  excellent,
}

enum UserType {
  owner,
  partner,
  staff,
}
