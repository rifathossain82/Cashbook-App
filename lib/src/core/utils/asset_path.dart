class AssetPath{

  /// images
  static const String cashbookAppLogo = 'assets/images/cashbook_app_logo.png';
  static const String cashbookLogo = 'assets/images/cashbook_logo.png';
  static const String profile = 'assets/images/people.png';
  static const String placeholder = 'assets/images/placeholder.png';
  static const String introImage1 = 'assets/images/intro_image_1.jpg';
  static const String introImage2 = 'assets/images/intro_image_2.png';
  static const String introImage3 = 'assets/images/intro_image_3.jpeg';
  static const String noDataFound = 'assets/images/no_data_found.jpg';

  /// icons
  static const String protectIcon = 'assets/icons/protect_icon.png';
  static const String booksIcon = 'assets/icons/bookss.png';
  static const String emptyBoxIcon = 'assets/icons/empty_box.png';


  /// lotties
  static const String confirmationLottie = 'assets/lotties/confirmation_lottie.json';

}