import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'color.dart';
import 'dimensions.dart';

class MyTextStyles {
  static final themeController = Get.find<ThemeController>();

  static TextStyle get h1 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeOverLarge,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }

  static TextStyle get h2 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeExtraLarge,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }

  static TextStyle get h3 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeLarge,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }

  static TextStyle get h4 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeDefault,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }

  static TextStyle get h5 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeSmall,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }

  static TextStyle get h6 {
    return GoogleFonts.poppins(
      fontWeight: FontWeight.w400,
      fontSize: Dimensions.fontSizeExtraSmall,
      color: themeController.isDarkMode ? kWhite : kBlackLight,
    );
  }
}
