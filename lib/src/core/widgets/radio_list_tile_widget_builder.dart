import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class RadioListTileWidgetBuilder extends StatelessWidget {
  final ValueChanged onChanged;
  final dynamic value;
  final dynamic groupValue;
  final bool isSelected;
  final String title;
  final Widget? secondary;

  const RadioListTileWidgetBuilder({
    Key? key,
    required this.onChanged,
    required this.value,
    required this.groupValue,
    required this.isSelected,
    required this.title,
    this.secondary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RadioListTile(
      onChanged: onChanged,
      value: value,
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      dense: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
        side: BorderSide(
          color: kGreyMedium,
          width: isSelected ? 0 : 0.5,
        ),
      ),
      tileColor:
          isSelected ? kPrimaryLightColor.withOpacity(0.2) : Colors.transparent,
      groupValue: groupValue,
      title: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: MyTextStyles.h4,
      ),
      secondary: secondary,
    );
  }
}
