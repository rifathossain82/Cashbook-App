import 'package:flutter/material.dart';

class CircularButtonLoader extends StatelessWidget {
  final Color indicatorColor;

  const CircularButtonLoader({
    Key? key,
    this.indicatorColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 20,
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        color: indicatorColor,
      ),
    );
  }
}
