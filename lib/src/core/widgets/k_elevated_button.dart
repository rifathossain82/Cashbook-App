import 'package:cashbook_app/src/core/extensions/build_context_extension.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class KElevatedButton extends StatelessWidget {
  final VoidCallback onTap;
  final bool isFormValid;
  final String text;

  const KElevatedButton({
    Key? key,
    required this.onTap,
    required this.isFormValid,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isFormValid
          ? onTap
          : null,
      style: ElevatedButton.styleFrom(
        minimumSize: Size(context.screenWidth - 30, 50),
      ),
      child: Text(
        text,
        style: MyTextStyles.h3.copyWith(
          fontWeight: FontWeight.bold,
          color: isFormValid ? kWhite : kGrey,
        ),
      ),
    );
  }
}
