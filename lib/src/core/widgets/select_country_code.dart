// import 'package:barber_shop/src/utils/color.dart';
// import 'package:barber_shop/src/utils/styles.dart';
// import 'package:barber_shop/src/views/base/helper.dart';
// import 'package:barber_shop/src/views/base/k_custom_loader.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// import '../../controllers/common/country_code_controller.dart';
// import '../../models/common/country_code_data.dart';
// import '../../utils/dimensions.dart';
// import 'k_search_field.dart';
//
// /// to select countryCode I used this reusable widget
// /// example: in login page phone number section i used it
//
// Future selectCountryCode(BuildContext context) {
//   final countryCodeController = Get.put(CountryCodeController());
//   countryCodeController.getCountryCodeList();
//   final TextEditingController searchController = TextEditingController();
//
//   searchMethod(String value) {
//     DeBouncer.run(() {
//       if (value.isNotEmpty) {
//         countryCodeController.getCountryCodeList(
//           countryName: searchController.text,
//         );
//       } else {
//         countryCodeController.getCountryCodeList();
//       }
//     });
//   }
//
//   return showModalBottomSheet(
//     context: context,
//     isScrollControlled: true,
//     shape: const RoundedRectangleBorder(
//         borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
//     ),
//     builder: (context) {
//       return StatefulBuilder(builder: (context, setState) {
//         return Padding(
//           padding: MediaQuery.of(context).viewInsets,
//           child: Container(
//             padding: const EdgeInsets.only(top: 16),
//             height: MediaQuery.of(context).size.height * 0.8, //size of bottom sheet
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 //heading and close button
//                 Row(
//                   children: [
//                     IconButton(
//                       onPressed: () {
//                         Get.back();
//                       },
//                       icon: Icon(
//                         Icons.close,
//                         color: kGrey,
//                       ),
//                     ),
//                     Text(
//                       'Select Your Country'.tr,
//                       style: MyTextStyles.h2,
//                     ),
//                   ],
//                 ),
//                 const Divider(
//                   height: 0,
//                 ),
//                 const SizedBox(
//                   height: 16,
//                 ),
//
//                 //search textField
//                 Padding(
//                   padding: EdgeInsets.only(
//                     left: Dimensions.paddingSizeDefault,
//                     right: Dimensions.paddingSizeDefault,
//                     bottom: Dimensions.paddingSizeSmall,
//                   ),
//                   child: KSearchField(
//                     controller: searchController,
//                     hintText: 'Search country'.tr,
//                     onChanged: searchMethod,
//                   ),
//                 ),
//
//                 //here is all country code
//                 Expanded(
//                   child: RefreshIndicator(
//                     child: GetX<CountryCodeController>(builder: (controller) {
//                       if (controller.isLoading.value) {
//                         return const KCustomLoader();
//                       } else if (controller.isLoading.value == false &&
//                           controller.countryCodeData.value.countries!.isEmpty) {
//                         return Stack(
//                           children: [
//                             ListView(),
//                             const Center(
//                               child: Text('No Code available'),
//                             )
//                           ],
//                         );
//                       } else {
//                         return ListView.builder(
//                           itemCount: controller.countryCodeData.value.countries!.length,
//                           itemBuilder: (context, index) {
//                             return CustomCountryCodeTile(
//                               data: controller.countryCodeData.value.countries![index],
//                             );
//                           },
//                         );
//                       }
//                     }),
//                     onRefresh: () async {
//                       searchController.clear();
//                       countryCodeController.getCountryCodeList();
//                     },
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         );
//       });
//     },
//   );
// }
//
// class CustomCountryCodeTile extends StatelessWidget {
//   const CustomCountryCodeTile({Key? key, required this.data}) : super(key: key);
//
//   final Countries data;
//
//   @override
//   Widget build(BuildContext context) {
//     return ListTile(
//       onTap: () {
//         Get.back(result: data);
//       },
//       title: Row(
//         children: [
//           Text(
//             data.niceName.toString(),
//           ),
//           const SizedBox(
//             width: 8,
//           ),
//           Text(
//             '(${data.phoneCode})',
//             style: const TextStyle(color: Colors.grey),
//           ),
//         ],
//       ),
//     );
//   }
// }
