import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/add_business/model/business_category_model.dart';
import 'package:flutter/material.dart';

class BusinessCategoryItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final bool isSelected;
  final BusinessCategoryModel categoryModel;

  const BusinessCategoryItemBuilder({
    Key? key,
    required this.onTap,
    required this.isSelected,
    required this.categoryModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: isSelected
              ? kPrimarySwatchColor.withOpacity(0.2)
              : Colors.transparent,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: isSelected
                ? kPrimaryLightColor
                : kGrey,
            width: 1,
          ),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 8,
              ),
              child: Row(
                children: [
                  Image.asset(
                    AssetPath.cashbookLogo,
                    height: 40,
                    width: 40,
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Text(
                      categoryModel.name ?? "",
                      style: MyTextStyles.h4.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              right: 10,
              top: 10,
              child: isSelected ? Icon(
                Icons.check_circle_rounded,
                color: kPrimaryLightColor,
              ) : Container(),
            ),
          ],
        ),
      ),
    );
  }
}
