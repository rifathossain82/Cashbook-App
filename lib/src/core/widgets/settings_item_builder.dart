import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:cashbook_app/src/features/settings/model/settings_item_model.dart';
import 'package:flutter/material.dart';

class SettingsItemBuilder extends StatelessWidget {
  final SettingsItemModel itemModel;

  const SettingsItemBuilder({
    Key? key,
    required this.itemModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: itemModel.onTap ??
          () {
            if (itemModel.routeName != null || itemModel.routeName != '') {
              Navigator.pushNamed(
                context,
                itemModel.routeName!,
              );
            }
          },
      contentPadding: EdgeInsets.zero,
      horizontalTitleGap: 5,
      leading: Icon(
        itemModel.leadingIcon,
        color: kGrey,
      ),
      title: Text(
        itemModel.titleText,
        style: MyTextStyles.h4,
      ),
      subtitle: Text(
        itemModel.subtitleText,
        style: MyTextStyles.h3.copyWith(
          color: kGreyTextColor,
        ),
      ),
      trailing: Icon(
        itemModel.trailingIcon ?? Icons.add,
        color: kPrimaryLightColor,
        size: 25,
      ),
    );
  }
}
