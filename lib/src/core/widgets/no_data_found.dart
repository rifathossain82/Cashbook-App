import 'package:cashbook_app/src/core/utils/asset_path.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NoDataFound extends StatelessWidget {
  final VoidCallback? searchAgain;
  final String? subTitle;

  const NoDataFound({
    Key? key,
    this.searchAgain,
    this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              AssetPath.emptyBoxIcon,
              width: context.height * 0.25,
              height: context.height * 0.25,
              fit: BoxFit.cover,
            ),
            const SizedBox(height: 5),
            Text(
              'No Data Found!'.tr,
              style: MyTextStyles.h2.copyWith(
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 5),
            Text(
              subTitle ?? '',
              textAlign: TextAlign.center,
              style: MyTextStyles.h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
            // addVerticalSpace(16),
            // KButton(
            //   onPressed: searchAgain,
            //   bgColor: kRedMedium,
            //   width: context.width * 0.4,
            //   borderRadius: 100,
            //   child: Text(
            //     'Search Again',
            //     style: GoogleFonts.roboto(
            //       textStyle: MyTextStyles.h3.copyWith(
            //         color: kWhite,
            //         fontWeight: FontWeight.w500,
            //       ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
