import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class UserTypeBuilder extends StatelessWidget {
  final String type;
  final Color color;

  const UserTypeBuilder({
    Key? key,
    required this.type,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: color.withOpacity(0.2),
      ),
      child: Text(
        type,
        style: MyTextStyles.h5.copyWith(
          color: color,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
