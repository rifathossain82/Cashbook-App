import 'package:flutter/material.dart';

class KCustomLoader extends StatelessWidget {
  const KCustomLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
