import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class MobileNumberTextFormFieldBuilder extends StatelessWidget {
  final TextEditingController countryCodeController;
  final TextEditingController mobileNumberController;
  final FormFieldValidator? validator;
  final ValueChanged? onChanged;
  final VoidCallback onTapCountryCode;

  const MobileNumberTextFormFieldBuilder({
    Key? key,
    required this.countryCodeController,
    required this.mobileNumberController,
    this.validator,
    this.onChanged,
    required this.onTapCountryCode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: TextFormField(
              controller: countryCodeController,
              readOnly: true,
              onTap: onTapCountryCode,
              decoration: InputDecoration(
                isDense: true,
                labelText: 'Code',
                suffixIconConstraints: const BoxConstraints(
                  maxHeight: 25,
                  maxWidth: 40,
                  minWidth: 30,
                ),
                suffixIcon: Icon(
                  Icons.arrow_drop_down,
                  color: kBlackLight,
                  size: 20,
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: kGrey),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: kGrey),
                ),
              ),
            ),
          ),
          const SizedBox(width: 15),
          Expanded(
            flex: 7,
            child: TextFormField(
              controller: mobileNumberController,
              validator: validator,
              onChanged: onChanged,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                isDense: true,
                labelText: 'Mobile Number',
                border: const OutlineInputBorder(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
