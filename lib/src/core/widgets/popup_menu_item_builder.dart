import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

PopupMenuItem popupMenuItemBuilder({
  required dynamic value,
  required IconData icon,
  required Color iconColor,
  required String title,
}) {
  return PopupMenuItem(
    value: value,
    child: ListTile(
      contentPadding: EdgeInsets.zero,
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      leading: Icon(
        icon,
        color: iconColor,
      ),
      title: Text(
        title,
        style: MyTextStyles.h4,
      ),
    ),
  );
}
