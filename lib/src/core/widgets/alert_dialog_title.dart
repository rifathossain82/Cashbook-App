import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AlertDialogTitle extends StatelessWidget {
  final String title;
  const AlertDialogTitle({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        color: kGreyLight.withOpacity(0.6),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 0,
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(
                Icons.close,
                color: kRed,
              ),
            ),
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: MyTextStyles.h3,
          ),
        ],
      ),
    );
  }
}
