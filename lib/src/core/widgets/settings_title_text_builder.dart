import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsTitleTextBuilder extends StatelessWidget {
  final String text;

  const SettingsTitleTextBuilder({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 20,
      ),
      child: Text(
        text,
        textAlign: TextAlign.start,
        style: MyTextStyles.h4.copyWith(
          fontWeight: FontWeight.bold,
          color: Get.find<ThemeController>().isDarkMode
              ? kWhite.withOpacity(0.7)
              : kBlackLight.withOpacity(0.7),
        ),
      ),
    );
  }
}
