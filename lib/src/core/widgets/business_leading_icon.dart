import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class BusinessLeadingIcon extends StatelessWidget {
  final Color? iconColor;

  const BusinessLeadingIcon({
    Key? key,
    this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: kGrey,
          width: 1,
        ),
      ),
      child: Icon(
        Icons.business,
        color: iconColor ?? kGrey,
      ),
    );
  }
}
