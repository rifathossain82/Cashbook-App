import 'package:cashbook_app/src/core/theme/controller/theme_controller.dart';
import 'package:cashbook_app/src/core/utils/color.dart';
import 'package:cashbook_app/src/core/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppSettingsListTieBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final IconData leadingIcon;
  final String title;
  final Widget trailing;

  AppSettingsListTieBuilder({
    Key? key,
    required this.onTap,
    required this.leadingIcon,
    required this.title,
    required this.trailing,
  }) : super(key: key);

  final themeController = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return ListTile(
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      tileColor: themeController.isDarkMode ? kSecondaryDarkColor : kWhite,
      onTap: onTap,
      leading: Icon(
        leadingIcon,
        color: kPrimaryLightColor,
      ),
      title: Text(
        title,
        style: MyTextStyles.h3,
      ),
      trailing: trailing,
    );
  }
}
