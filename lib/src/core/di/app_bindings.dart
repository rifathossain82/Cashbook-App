import 'package:cashbook_app/src/features/add_business/controller/business_controller.dart';
import 'package:cashbook_app/src/features/auth/controller/auth_controller.dart';
import 'package:cashbook_app/src/features/cashbooks/controller/cashbook_controller.dart';
import 'package:cashbook_app/src/features/category/controller/transaction_category_controller.dart';
import 'package:cashbook_app/src/features/contact/controller/contact_controller.dart';
import 'package:cashbook_app/src/features/payment_modes/controller/payment_method_controller.dart';
import 'package:cashbook_app/src/features/user_profile/controller/user_profile_controller.dart';
import 'package:get/get.dart';

class AppBindings extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => AuthController(), fenix: true);
    Get.lazyPut(() => BusinessController(), fenix: true);
    Get.lazyPut(() => CashbookController(), fenix: true);
    Get.lazyPut(() => ContactController(), fenix: true);
    Get.lazyPut(() => TransactionCategoryController(), fenix: true);
    Get.lazyPut(() => PaymentMethodController(), fenix: true);
    Get.lazyPut(() => UserProfileController(), fenix: true);
  }
  
}