import 'package:cashbook_app/src/features/about/view/pages/about_cashbook_page.dart';
import 'package:cashbook_app/src/features/add_business/view/pages/add_business_name_page.dart';
import 'package:cashbook_app/src/features/add_business/view/pages/select_business_category_page.dart';
import 'package:cashbook_app/src/features/add_business/view/pages/select_business_type_page.dart';
import 'package:cashbook_app/src/features/app_settings/view/pages/app_settings_page.dart';
import 'package:cashbook_app/src/features/app_settings/view/pages/data_backup_page.dart';
import 'package:cashbook_app/src/features/auth/view/pages/login_page.dart';
import 'package:cashbook_app/src/features/auth/view/pages/verify_otp_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/add_business_address_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/business_category_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/business_profile_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/business_registration_type_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/business_subcategory_page.dart';
import 'package:cashbook_app/src/features/business_profile/view/pages/business_type_page.dart';
import 'package:cashbook_app/src/features/business_settings/view/pages/business_settings_page.dart';
import 'package:cashbook_app/src/features/business_settings/view/pages/change_owner_page.dart';
import 'package:cashbook_app/src/features/business_settings/view/pages/delete_business_confirm_page.dart';
import 'package:cashbook_app/src/features/business_settings/view/pages/delete_business_page.dart';
import 'package:cashbook_app/src/features/business_team/view/pages/add_member_manually_page.dart';
import 'package:cashbook_app/src/features/business_team/view/pages/add_team_member_page.dart';
import 'package:cashbook_app/src/features/business_team/view/pages/business_team_page.dart';
import 'package:cashbook_app/src/features/business_team/view/pages/choose_role_and_invite_page.dart';
import 'package:cashbook_app/src/features/business_team/view/pages/share_invitation_link_page.dart';
import 'package:cashbook_app/src/features/cashbooks/view/pages/add_entry_page.dart';
import 'package:cashbook_app/src/features/contact/view/pages/choose_contact_page.dart';
import 'package:cashbook_app/src/features/dashboard/view/pages/dashboard_page.dart';
import 'package:cashbook_app/src/features/help/view/pages/help_page.dart';
import 'package:cashbook_app/src/features/intro/view/pages/intro_page.dart';
import 'package:cashbook_app/src/features/payment_modes/view/pages/choose_payment_modes.dart';
import 'package:cashbook_app/src/features/requests/view/pages/requests_page.dart';
import 'package:cashbook_app/src/features/search/view/pages/search_page.dart';
import 'package:cashbook_app/src/features/splash/view/pages/splash_page.dart';
import 'package:cashbook_app/src/features/user_profile/view/pages/user_profile_page.dart';
import 'package:get/get.dart';

class RouteGenerator {
  static const String splash = '/';
  static const String intro = '/intro';
  static const String login = '/login';
  static const String verifyOTPPage = '/verifyOTPPage';
  static const String dashboard = '/dashboard';
  static const String addEntry = '/addEntry';
  static const String chooseContact = '/chooseContact';
  static const String choosePaymentModes = '/choosePaymentModes';
  static const String addBusinessName = '/addBusinessName';
  static const String selectBusinessCategory = '/selectBusinessCategory';
  static const String selectBusinessType = '/selectBusinessType';
  static const String searchPage = '/searchPage';
  static const String help = '/help';
  static const String businessProfile = '/businessProfile';
  static const String addBusinessAddress = '/addBusinessAddress';
  static const String businessCategoryPage = '/businessCategoryPage';
  static const String businessSubcategoryPage = '/businessSubcategoryPage';
  static const String businessTypePage = '/businessTypePage';
  static const String businessRegistrationTypePage = '/businessRegistrationTypePage';
  static const String requestsPage = '/requestsPage';
  static const String businessSettingsPage = '/businessSettingsPage';
  static const String changeOwner = '/changeOwner';
  static const String deleteBusiness = '/deleteBusiness';
  static const String appSettings = '/appSettings';
  static const String dataBackup = '/dataBackup';
  static const String userProfile = '/userProfile';
  static const String aboutCashbook = '/aboutCashbook';
  static const String deleteBusinessConfirm = '/deleteBusinessConfirm';
  static const String businessTeam = '/businessTeam';
  static const String addTeamMember = '/addTeamMember';
  static const String addMemberManually = '/addMemberManually';
  static const String chooseRoleAndInvite = '/chooseRoleAndInvite';
  static const String shareInvitationLink = '/shareInvitationLink';

  static final routes=[
    GetPage(name: RouteGenerator.splash, page: ()=> const SplashPage(),),
    GetPage(name: RouteGenerator.intro, page: ()=> const IntroPage(),),
    GetPage(name: RouteGenerator.login, page: ()=> const LoginPage(),),
    GetPage(name: RouteGenerator.verifyOTPPage, page: ()=> const VerifyOTPPage(),),
    GetPage(name: RouteGenerator.dashboard, page: ()=> const DashboardPage(),),
    GetPage(name: RouteGenerator.addEntry, page: ()=> const AddEntryPage(),),
    GetPage(name: RouteGenerator.chooseContact, page: ()=> const ChooseContactPage(),),
    // GetPage(name: RouteGenerator.choosePaymentModes, page: ()=> const ChoosePaymentModesPage(),),
    GetPage(name: RouteGenerator.addBusinessName, page: ()=> const AddBusinessNamePage(),),
    GetPage(name: RouteGenerator.selectBusinessCategory, page: ()=> const SelectBusinessCategoryPage(),),
    GetPage(name: RouteGenerator.selectBusinessType, page: ()=> const SelectBusinessTypePage(),),
    GetPage(name: RouteGenerator.searchPage, page: ()=> const SearchPage(),),
    GetPage(name: RouteGenerator.help, page: ()=> const HelpPage(),),
    GetPage(name: RouteGenerator.businessProfile, page: ()=> const BusinessProfilePage(),),
    GetPage(name: RouteGenerator.addBusinessAddress, page: ()=> const AddBusinessAddressPage(),),
    GetPage(name: RouteGenerator.businessCategoryPage, page: ()=> const BusinessCategoryPage(),),
    GetPage(name: RouteGenerator.businessSubcategoryPage, page: ()=> const BusinessSubcategoryPage(),),
    GetPage(name: RouteGenerator.businessTypePage, page: ()=> const BusinessTypePage(),),
    GetPage(name: RouteGenerator.businessRegistrationTypePage, page: ()=> const BusinessRegistrationTypePage(),),
    GetPage(name: RouteGenerator.requestsPage, page: ()=> const RequestsPage(),),
    GetPage(name: RouteGenerator.businessSettingsPage, page: ()=> const BusinessSettingsPage(),),
    GetPage(name: RouteGenerator.changeOwner, page: ()=> ChangeOwnerPage(),),
    GetPage(name: RouteGenerator.deleteBusiness, page: ()=> DeleteBusinessPage(),),
    GetPage(name: RouteGenerator.appSettings, page: ()=> const AppSettingsPage(),),
    GetPage(name: RouteGenerator.dataBackup, page: ()=> const DataBackupPage(),),
    GetPage(name: RouteGenerator.userProfile, page: ()=> const UserProfilePage(),),
    GetPage(name: RouteGenerator.aboutCashbook, page: ()=> const AboutCashbookPage(),),
    GetPage(name: RouteGenerator.businessTeam, page: ()=> const BusinessTeamPage(),),
    GetPage(name: RouteGenerator.addTeamMember, page: ()=> const AddTeamMemberPage(),),
    GetPage(name: RouteGenerator.addMemberManually, page: ()=> const AddMemberManuallyPage(),),
    GetPage(name: RouteGenerator.chooseRoleAndInvite, page: ()=> const ChooseRoleAndInvitePage(),),
    GetPage(name: RouteGenerator.shareInvitationLink, page: ()=> const ShareInvitationLinkPage(),),
  ];
}
