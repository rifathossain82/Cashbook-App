import 'package:cashbook_app/src/core/enums/app_enum.dart';
import 'package:cashbook_app/src/core/services/local_storage.dart';
import 'package:get/get.dart';

class ThemeController extends GetxController {
  final _isDarkMode = false.obs;

  bool get isDarkMode {
    _isDarkMode.value = LocalStorage.getData(key: LocalStorageKey.isDarkMode) ?? false;
    return _isDarkMode.value;
  }

  void toggleTheme() {
    _isDarkMode.value = !_isDarkMode.value;

    /// set in locale storage
    LocalStorage.saveData(
      key: LocalStorageKey.isDarkMode,
      data: _isDarkMode.value,
    );
  }
}
