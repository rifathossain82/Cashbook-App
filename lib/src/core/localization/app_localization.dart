import 'dart:ui';

import 'package:cashbook_app/src/core/localization/language/bangla.dart';
import 'package:cashbook_app/src/core/localization/language/english.dart';
import 'package:cashbook_app/src/features/localization/model/locale_model.dart';
import 'package:get/get.dart';

class AppLocalization extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        "bn_BN": banglaLanguage,
        "en_US": englishLanguage,
      };

  static final locals = [
    LocaleData(
      locale: const Locale('bn', 'BN'),
      languageName: 'Bangla',
      countryName: 'Bangladesh',
      languageCode: 'BN',
      localeLanguageName: 'বাংলা',
    ),
    LocaleData(
      locale: const Locale('en', 'US'),
      languageName: 'English',
      countryName: 'United States',
      languageCode: 'EN',
      localeLanguageName: 'English',
    ),
  ];
}
