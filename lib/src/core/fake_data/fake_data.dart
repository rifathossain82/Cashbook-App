import 'package:cashbook_app/src/core/enums/app_enum.dart';

class BookModel {
  final String name;
  final String creationTime;
  final String netBalance;

  BookModel({
    required this.name,
    required this.creationTime,
    required this.netBalance,
  });
}

final fakeBookList = [
  BookModel(
    name: 'Market',
    creationTime: 'Created 13 mins age',
    netBalance: '0',
  ),
  BookModel(
    name: 'Home',
    creationTime: 'Created 30 mins age',
    netBalance: '0',
  ),
  BookModel(
    name: 'Office',
    creationTime: 'Created on Apr 02 2023',
    netBalance: '0',
  ),
  BookModel(
    name: 'Mine',
    creationTime: 'Created on Apr 04 2023',
    netBalance: '300',
  ),
];

class FakeBusinessModel {
  final String name;
  final String category;
  final String type;
  final int totalBook;

  FakeBusinessModel({
    required this.name,
    required this.category,
    required this.type,
    required this.totalBook,
  });
}

final fakeBusinessList = [
  FakeBusinessModel(
    name: 'RH Shop',
    category: 'Education',
    type: 'Service Provider',
    totalBook: 5,
  ),
  FakeBusinessModel(
    name: 'Samim',
    category: 'Construction',
    type: 'Real State',
    totalBook: 12,
  ),
  FakeBusinessModel(
    name: 'Osmani Group',
    category: 'Food',
    type: 'Manufacture',
    totalBook: 2,
  ),
  FakeBusinessModel(
    name: 'Shahin Trade Mill',
    category: 'Electronics',
    type: 'Distributor',
    totalBook: 8,
  ),
  FakeBusinessModel(
    name: 'Jalal Fashion',
    category: 'Fashion',
    type: 'Other',
    totalBook: 2,
  ),
];

class FakeUserData {
  final String name;
  final String phone;
  final UserType userType;

  FakeUserData({
    required this.name,
    required this.phone,
    required this.userType,
  });
}

final ownerOrPartnerList = [
  FakeUserData(
    name: 'You',
    phone: '+8801885256220',
    userType: UserType.owner,
  ),
  FakeUserData(
    name: 'Jilani',
    phone: '+8801885256221',
    userType: UserType.partner,
  ),

];

final staffList = [
  FakeUserData(
    name: 'Arif',
    phone: '+8801885256220',
    userType: UserType.staff,
  ),
  FakeUserData(
    name: 'Kamrul',
    phone: '+8801885256221',
    userType: UserType.staff,
  ),
  FakeUserData(
    name: 'Jobayer',
    phone: '+8801885256221',
    userType: UserType.staff,
  ),
  FakeUserData(
    name: 'Rasel',
    phone: '+8801885256221',
    userType: UserType.staff,
  ),
  FakeUserData(
    name: 'Kawchar',
    phone: '+8801885256221',
    userType: UserType.staff,
  ),

];
